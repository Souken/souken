﻿Shader "Custom/weed" {
	Properties {
        _BendScale("Bend Scale", Range(0.0, 1.0))	= 0.2
		_Color("Diffuse Material Color", Color)		= (1, 1, 1, 1)
        _MainTex("Main Texture", 2D) = "white" {}
    }
    SubShader {
		Tags {"Queue" = "Transparent"} 
 
		Pass {	
			Cull Back	// first render the back faces
			ZWrite Off	// don't write to depth buffer 
						// in order not to occlude other objects
			Blend SrcAlpha OneMinusSrcAlpha 
						// blend based on the fragment's alpha value
 
			CGPROGRAM
 
			#pragma vertex vert  
			#pragma fragment frag 
			
			#define PI 3.14159

			uniform sampler2D _MainTex;
			uniform float4	_Color;
			uniform float	_BendScale;
			uniform float	_Cutoff;
 
			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};
 
			vertexOutput vert(vertexInput input) 
			{
				vertexOutput output;

				float bend = sin(PI * _Time.x * 1000 / 45 + input.vertex.y);
				float x = cos(input.texcoord.y * (PI/2)) - 1.0;
				float y = cos(input.texcoord.y * (PI/2)) - 1.0;
				input.vertex.y += _BendScale * bend * (x + y);
 
				output.tex = input.texcoord;
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);

				return output;
			}
 
			float4 frag(vertexOutput input) : COLOR
			{
				return tex2D(_MainTex, input.tex.xy) * _Color;
			}
 
			ENDCG
		}
	}
}
