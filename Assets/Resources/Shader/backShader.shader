﻿Shader "Custom/backShader" {
	Properties{
		_MainTex ( "Base", 2D ) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 100
	
		Pass {
			CULL	 Front
			Lighting Off
			SetTexture [_MainTex] { combine texture } 
		}
	} 
}
