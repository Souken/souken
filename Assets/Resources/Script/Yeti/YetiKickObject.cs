﻿using UnityEngine;
using System.Collections;

// 蹴りの攻撃判定
public class YetiKickObject : MonoBehaviour {

	[SerializeField]
	private float damagePercent = 3.0f;
	[SerializeField] private int damage = 50;
	[SerializeField]
	private float knockedForce = 3.0f;

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// 当たり判定
	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			PlayerStatus player = collider.gameObject.GetComponent<PlayerStatus>();
			player.calcDamage(damage);
			player.calcKnocked(transform.position, knockedForce);
			Destroy(gameObject);
		}
	}

}
