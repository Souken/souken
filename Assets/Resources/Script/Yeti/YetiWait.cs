﻿using UnityEngine;
using System.Collections;

// 待機状態
// この時にダメージを受けたらしばらく怯ませる
public class YetiWait : YetiAIBase {

	[SerializeField, Tooltip("何秒間待機させるか")] private float waitTime = 3.0f;
	[Tooltip("プレイヤーを追尾する時の回転力、高いほど追尾性能が上がる")]
	[SerializeField, Range(0.0f, 0.5f)]
	private float ROTATE_POWER = 0.01f;

	private Transform targetPos;

	public float nowTime = 0.0f;

	enum RotateState { Wait, Right, Left };
	RotateState rotateState = RotateState.Wait;

	// Use this for initialization
	void Start () {
		aiUpdate = calcWait;
		nextAIType = EnemyAIType.AIYeti.Tackle;	// 今は仮

		targetPos = Yeti.player.transform;
		if (targetPos == null) { Debug.LogError("Playerが見つかりません"); }

		// 裏モードの場合待機時間少なめ
		if (SaveData.Instance.isBackMode() == true || GetComponent<Yeti>().isEnemyBackMode() == true)
		{
			waitTime = 2.0f;
		}
	}
	
	void calcWait()
	{
		// プレイヤーの座標と自分の座標からプレイヤーへの方向ベクトル算出(ただしyは考えない)
		Vector3 myPos = gameObject.transform.position;
		Vector3 tarPos = targetPos.position;
		myPos.y = tarPos.y = 0.0f;
		Vector3 targetDirect = (tarPos - myPos).normalized;

		// 4元数でやってみる
		Quaternion oldRotate = transform.rotation;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);

		// モーションチェンジ
		changeMotion(oldRotate);

		nowTime += Time.deltaTime;
		if (nowTime >= waitTime) 
		{ 
			exitFlag = true;
			// 乱数で決める
			switch(Random.Range(0, 4))
			{
				case 0:
					nextAIType = EnemyAIType.AIYeti.Tackle;	return;
				case 1:
					nextAIType = EnemyAIType.AIYeti.Quake; return;
				case 2:
					nextAIType = EnemyAIType.AIYeti.Breath; return;
				case 3:
					nextAIType = EnemyAIType.AIYeti.Run; return;
			}
		}
	}

	// モーションの切り替え(回転方向によって旋回モーションを変える)
	void changeMotion(Quaternion oldRotate)
	{
		// 加算されていたら左、減算されていたら右
		if (oldRotate.y > transform.rotation.y)
		{
			if (rotateState == RotateState.Right) return;
			rotateState = RotateState.Right;
			Yeti.animator.Play("turnRight");
		}
		else if (oldRotate.y < transform.rotation.y)
		{
			if (rotateState == RotateState.Left) return;
			rotateState = RotateState.Left;
			Yeti.animator.Play("turnLeft");
		}
		else
		{
			if (rotateState == RotateState.Wait) return;
			rotateState = RotateState.Wait;
			Yeti.animator.Play("wait");
		}
	}

	public override void setInit()
	{
		base.setInit();
		Yeti.animator.Play("wait");
	}

	public override bool isExit()
	{
		if(base.isExit() == false)	return false;
		nowTime = 0.0f;

		return true;
	}

}
