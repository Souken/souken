﻿using UnityEngine;
using System.Collections;

public class YetiDamage : YetiAIBase {

	// Use this for initialization
	void Start () {
		aiUpdate = calcMotion;
		nextAIType = EnemyAIType.AIYeti.Quake;	// 何となく地割れ
	}
	
	void calcMotion()
	{
		// モーションが終わったら何かしらの攻撃処理へ
		if (Yeti.state.normalizedTime >= 1.0f) { exitFlag = true; }
	}

	public override void setInit()
	{
		base.setInit();
		Yeti.animator.Play("damage3");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

}
