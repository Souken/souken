﻿using UnityEngine;
using System.Collections;

// 地割れのオブジェクト
public class YetiQuake : MonoBehaviour {

	[SerializeField]
	private float damagePercent = 1.4f;
	[SerializeField] public float speed = 0.01f;
	[SerializeField] private int damage = 10;
	[SerializeField, Tooltip("持続時間(秒)")]
	private float duration = 2.0f;
	[SerializeField, Range(0.0f, 1.0f),
	Tooltip("持続時間から何割経ったらパーティクルを止めるか")]
	private float stopParticleRate = 0.8f;

	[SerializeField]
	private ParticleSystem rock;
	[SerializeField]
	private ParticleSystem snow;

	private float nowTime = 0.0f;
	private bool isShotParticle = true;	// パーティクル放出の制御

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	// Use this for initialization
	void Start () {
		//rock = transform.FindChild("");
	}
	
	// Update is called once per frame
	void Update () {
		nowTime += Time.deltaTime;
		if (nowTime >= duration)
		{
			Destroy(gameObject); return;
		}

		// 一定時間経ったらエフェクト止める
		if(isShotParticle == true && nowTime >= duration * stopParticleRate)
		{
			rock.emissionRate = 0.0f;
			snow.emissionRate = 0.0f;
			isShotParticle = false;
		}
		transform.Translate(0.0f, 0.0f, speed);
	}

	// 当たり判定
	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			PlayerStatus player = collider.gameObject.GetComponent<PlayerStatus>();
			player.calcDamage(damage);
			player.calcShrink();
			Destroy(gameObject);
		}
	}

}
