﻿using UnityEngine;
using System.Collections;

public class YetiTackle : YetiAIBase {

	[SerializeField, Tooltip("判定用オブジェクト")]
	private GameObject yetiTackleObject;
	[SerializeField, Tooltip("タックルのダメージ")]
	private int	damage = 0;
	[Tooltip("プレイヤーを追尾する時の回転力、高いほど追尾性能が上がる")]
	[SerializeField, Range(0.0f, 1.0f)] private float ROTATE_POWER = 0.01f;
	[SerializeField] private float	speed = 0.01f;

	private Transform	targetPos;
	[SerializeField] private float nowTime = 0.0f;	// デバッグ用でシリアライズ中
	[SerializeField] private AudioClip tackleSE;
	[SerializeField] private AudioClip tackleVoice;

	// 時間になったか
	bool isTimeout(float limitTime)
	{
		nowTime += Time.deltaTime;
		if (nowTime >= limitTime) { nowTime = 0.0f; return true; }
		return false;
	}

	void Start()
	{
		aiUpdate = calcCharge;
		nextAIType = EnemyAIType.AIYeti.Wait;

		yetiTackleObject.GetComponent<YetiTackleObject>().setAttack(GetComponent<Yeti>().getAttackPoint());

		targetPos = Yeti.player.transform;
		if (targetPos == null) { Debug.LogError("Playerが見つかりません"); }
	}

	void calcCharge()
	{
		// チャージモーション処理とか
		//if (isTimeout(chargeTime) == true) { aiUpdate = calcTackle; }

		// モーションが終わったら待機処理に移行
		if (Yeti.state.normalizedTime >= 1.0f) { exitFlag = true; }
	}

	void calcTackle()
	{
		// プレイヤーの座標と自分の座標からプレイヤーへの方向ベクトル算出(ただしyは考えない)
		Vector3 myPos = gameObject.transform.position;
		Vector3 tarPos = targetPos.position;
		myPos.y = tarPos.y = 0.0f;
		Vector3 targetDirect = (tarPos - myPos).normalized;

		// 4元数でやってみる
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);
		gameObject.transform.Translate(0.0f, 0.0f, speed);
	}

	public void startRolling()
	{
		aiUpdate = calcTackle;
		audio.Play();
		// 判定オブジェクト生成
		GameObject tmp = Instantiate(yetiTackleObject, transform.position + new Vector3(0.0f, 0.75f, 0.0f), Quaternion.identity) as GameObject;
		tmp.transform.parent = transform;
	}

	public void endRolling()
	{
		aiUpdate = calcCharge;
		audio.Stop();
		// 攻撃オブジェクトの子を探して削除
		foreach (Transform child in transform)
		{
			if (0 <= child.name.IndexOf("Tackle")) { Destroy(child.gameObject); }
			//if (child.tag == "attack") { child.GetComponent<BladeScript>().deleteMyObject(); }
		}
	}

	public override void setInit()
	{
		base.setInit();
		Yeti.animator.Play("tackle");
		audio.clip = tackleSE;
		audio.PlayOneShot(tackleVoice);
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		nowTime = 0.0f;
		aiUpdate = calcCharge;
		audio.Stop();
		audio.clip = null;

		return true;
	}
}
