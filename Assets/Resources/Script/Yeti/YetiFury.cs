﻿using UnityEngine;
using System.Collections;

// 怒り時の動作
public class YetiFury : YetiAIBase {

	[SerializeField, Tooltip("赤みを帯びさせるので")]
	private GameObject polySurface;
	[SerializeField, Range(0.0f, 1.0f), Tooltip("緑と青の値をどこまで減らすか")]
	private float maxSubNum = 0.4f;
	[SerializeField, Range(0.0f, 0.1f), Tooltip("毎フレーム減らす値")]
	private float subNum = 0.01f;
	[SerializeField]
	private AudioClip furyVoice;
	[SerializeField, Tooltip("(悔しい)という意味")]
	private AudioClip mortifyingVoice;
	[SerializeField, Tooltip("蒸気")]
	private GameObject smoke;

	private float nowBGColorNum = 0.0f;
	private bool changeColorFlag = false;

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIYeti.Quake;
		nowBGColorNum = polySurface.renderer.material.color.b;
	}
	
	void calcUpdate()
	{
		// モーションが終わったら待機処理に移行
		if (Yeti.state.normalizedTime >= 1.0f) {
			GameObject tmp = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;
			tmp.transform.Translate(0.0f, 1.3f, 0.0f);
			tmp.transform.Rotate(new Vector3(1.0f, 0.0f, 0.0f), -90.0f);
			tmp.transform.parent = transform;
			exitFlag = true; 
		}

		// 指定値まで減らす
		if (changeColorFlag == true && nowBGColorNum != maxSubNum)
		{
			nowBGColorNum -= subNum;	// 適当に減らす
			if (nowBGColorNum <= maxSubNum) { nowBGColorNum = maxSubNum; }
			polySurface.renderer.material.color = new Color(1.0f, nowBGColorNum, nowBGColorNum, 1.0f);
		}
	}

	public override void setInit()
	{
		base.setInit();
		Yeti.animator.Play("fury");
		GetComponent<Yeti>().setInvincible();
		audio.PlayOneShot(mortifyingVoice);
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		
		return true;
	}

	public void startChangeColor() 
	{ 
		changeColorFlag = true;
		audio.PlayOneShot(furyVoice);
	}

}
