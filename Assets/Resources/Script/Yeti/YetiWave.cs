﻿using UnityEngine;
using System.Collections;

// 雪男が発生させる衝撃波
// 基本的に判定は球で取っているが
// スケールの高さ以下でないと処理を行わない
public class YetiWave : MonoBehaviour {

	[SerializeField]
	private float damagePercent = 2.0f;
	[SerializeField]
	private int damage = 20;
	[SerializeField, Tooltip("持続時間(秒)")]
	private float duration = 1.0f;
	[SerializeField, Tooltip("プレイヤーを飛ばす力")]
	private float knokeForce = 1.0f;

	private float nowTime = 0.0f;
	private float decisionHeight;
	private bool hitFlag = false;

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	// Use this for initialization
	void Start () {
		decisionHeight = transform.localScale.y;
	}
	
	// Update is called once per frame
	void Update () {
		nowTime += Time.deltaTime;
		if (nowTime >= duration) Destroy(gameObject);
	}

	bool isHitWave(Vector3 playerPos)
	{
		if (playerPos.y >= transform.position.y && playerPos.y <= transform.position.y + decisionHeight)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (hitFlag == true) return;
		print("衝撃波判定");

		if (other.gameObject.tag == "Player" && isHitWave(other.gameObject.transform.position) == true)
		{
			print("waveHit");

			PlayerStatus player = other.gameObject.GetComponent<PlayerStatus>();
			if (player.isMoveStatus() == false) { return; }
			
			player.calcDamage(damage);
			player.calcKnocked(transform.position, knokeForce);
			hitFlag = true;

			print("正常に飛ばした");
		}
	}

}