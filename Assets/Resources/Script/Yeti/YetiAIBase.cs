﻿using UnityEngine;
using System.Collections;

using EnemyAIType;


// 雪男のAI基底クラス
// ただ単に今のAI処理が終わったら次に渡す処理を置いておくだけの簡単なクラス
public class YetiAIBase : AIBase {

	public AIYeti nextAIType { get; protected set; }

}
