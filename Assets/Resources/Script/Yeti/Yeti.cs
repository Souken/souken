﻿using UnityEngine;
using System.Collections;

using EnemyAIType;

[RequireComponent(typeof(YetiWait))]
[RequireComponent(typeof(YetiTackle))]
[RequireComponent(typeof(YetiJump))]
[RequireComponent(typeof(YetiKick))]
[RequireComponent(typeof(YetiBreath))]
[RequireComponent(typeof(YetiRun))]
[RequireComponent(typeof(YetiDamage))]
[RequireComponent(typeof(YetiFury))]

// 雪男のメインクラス
// ここでAIの変更などを行う
public class Yeti : EnemyBase {

	private delegate bool AI();
	private AI aiUpdate;

	public static Animator			animator	{ get; private set; }
	public static AnimatorStateInfo state		{ get; private set; }
	public static AnimatorStateInfo oldState	{ get; private set; }

	public static PlayerStatus player { get; private set; }	// AIスクリプトからでもすぐに使えるようにstatic化(但し外部から上書きされないようにprivate set)
	public static bool furyFlag { get; private set; }

	[SerializeField]
	private AudioClip[] damageSE;
	[SerializeField]
	private AudioClip deadDownSE;
	[SerializeField]
	private AudioClip deadSE;

	[SerializeField, Tooltip("現在のAIの状態")]
	private AIYeti aiStatus = AIYeti.Wait;

	private YetiAIBase[] ai = new YetiAIBase[(int)AIYeti.NUM];

	// Use this for initialization
	protected override void Awake()
	{
		base.Awake();

		animator = GetComponent<Animator>();
		state = animator.GetCurrentAnimatorStateInfo(0);

		player = GameObject.Find("Player").GetComponent<PlayerStatus>();
		furyFlag = false;

		ai[(int)AIYeti.Wait]	= GetComponent<YetiWait>();
		ai[(int)AIYeti.Tackle]	= GetComponent<YetiTackle>();
		ai[(int)AIYeti.Quake]	= GetComponent<YetiJump>();
		ai[(int)AIYeti.Kick]	= GetComponent<YetiKick>();
		ai[(int)AIYeti.Breath]	= GetComponent<YetiBreath>();
		ai[(int)AIYeti.Run]		= GetComponent<YetiRun>();
		ai[(int)AIYeti.Damage]	= GetComponent<YetiDamage>();
		ai[(int)AIYeti.Fury]	= GetComponent<YetiFury>();

		if (SaveData.Instance.isBackMode() == true || isEnemyBackMode() == true) aiStatus = AIYeti.Run;

		ai[(int)aiStatus].setInit();
		aiUpdate = ai[(int)aiStatus].update;
	}
	
	// Update is called once per frame
	void Update () {
		// フェードインしきっていないなら動かない
		if (FadeManager.Instance.isMaxFadeIn() == false) { return; }

		rigidbody.WakeUp();

		oldState = state;
		state = animator.GetCurrentAnimatorStateInfo(0);

		if (nowState == State.Dead)
		{
			if (GameManager.Instance.isPreClear() == false && GameManager.Instance.isClear() == false &&
				state.IsName("dead") == true && state.normalizedTime >= 1.0f)
			{
				GameManager.Instance.gamePreClear();	// ゲームクリア
			}
			else if (state.IsName("dead") == false)
			{
				calcDead();
			}
			return;
		}

		// trueが帰ってきたらそのフェーズは終了
		// elseの場合はまだ途中
		if (aiUpdate() == true) 
		{
			// HPが半分切っていたら怒りモードへ(裏モードの場合、3/4切ったらすぐに怒る)
			if (furyFlag == false && hitPoint <= (SaveData.Instance.isBackMode() == true ||
				GetComponent<Yeti>().isEnemyBackMode() == true ? MAX_HITPOINT / 2 : (MAX_HITPOINT / 4) * 3))
			{
				furyFlag = true;
			
				aiStatus = AIYeti.Fury;
				calcChangeAI();
				return;
			}

			aiStatus = ai[(int)aiStatus].nextAIType;
			calcChangeAI(); 
		}
	}

	// AI変更
	void calcChangeAI()
	{
		// 死亡してるか生きている以外の場合は正常に戻す
		if (nowState != State.Alive && nowState != State.Dead) { nowState = State.Alive; }

		// 現在のAIの中で決めてあるものを次のAIに
		ai[(int)aiStatus].setInit();
		aiUpdate = ai[(int)aiStatus].update;

		print(aiStatus);
	}

	public override void calcDamage(int damage)
	{
		base.calcDamage(damage);
		print("yuki");

		// 死んだらモーション強制変更
		if (nowState == State.Dead) 
		{
			audio.PlayOneShot(deadDownSE);
			audio.PlayOneShot(deadSE);
			animator.Play("dead"); 
			return; 
		}

		// 待機状態ならダメージモーションへ(怒りモードなら移行なし)
		if (furyFlag == false && aiStatus == AIYeti.Wait)
		{
			aiStatus = AIYeti.Damage;
			
			int hoge = Random.Range(0, damageSE.Length);
			print(hoge);
			audio.PlayOneShot(damageSE[hoge]);
			calcChangeAI();
		}
	}

	// 死亡処理
	protected override void calcDead()
	{
		base.calcDead();
		if (GameManager.Instance.isClear() == true) { return; }
		//GameManager.Instance.gameClear();	// ゲームクリア
		print("ｵｳﾌ(死亡)");
	}

}
