﻿using UnityEngine;
using System.Collections;


// ジャンプした後の地割れ
// 地割れの発生はAnimationEvent側で行う
public class YetiJump : YetiAIBase {

	[SerializeField] private GameObject quake;
	[SerializeField] private GameObject wave;
	[SerializeField, Tooltip("通常状態の地割れの数")]
	private int normalQuakeNum = 3;
	[SerializeField, Tooltip("怒り状態の地割れの数")]
	private int furyQuakeNum = 5;

	[SerializeField, Tooltip("地割れの角度")]
	private float quakeRotate = 30.0f;

	[SerializeField] private float initJumpPower = 0.1f;
	[SerializeField, Tooltip("上昇時の減衰量")]
	private float mulJumpPower = 0.006f;

	[SerializeField, Tooltip("地割れの瞬間のボイス")]
	private AudioClip shockVoice;
	[SerializeField, Tooltip("ジャンプ時のボイス")]
	private AudioClip jumpVoice;

	private float jumpPower = 0.0f;
	private float nowJumpPower = 0.0f;
	
	enum JumpType { JT_Ground, JT_JUMP, JT_DOWN }
	private JumpType jumpType = JumpType.JT_Ground;

	// 地割れ生成時に使うラムダ式
	private delegate void Del(float f);

	// Use this for initialization
	void Start () {
		aiUpdate = calcWait;
		nextAIType = EnemyAIType.AIYeti.Wait;

		quake.GetComponent<YetiQuake>().setAttack(GetComponent<Yeti>().getAttackPoint());
		wave.GetComponent<YetiWave>().setAttack(GetComponent<Yeti>().getAttackPoint());

		if (SaveData.Instance.isBackMode() == true || GetComponent<Yeti>().isEnemyBackMode() == true)
		{
			quake.GetComponent<YetiQuake>().speed = 0.15f;
		}

		jumpPower = initJumpPower;
	}
	
	void calcWait()
	{
		// モーションが終わったら待機処理に移行
		if (Yeti.state.normalizedTime >= 1.0f) { exitFlag = true; }

		// ジャンプ処理
		if (jumpType == JumpType.JT_Ground)
		{
			if (transform.position.y != 0.0f) { transform.Translate(0.0f, -transform.position.y, 0.0f); }
			return;
		}

		// ジャンプ中
		if (jumpType == JumpType.JT_JUMP)
		{
			// 上がりきったら待機、徐々に上昇率を減らす
			if (jumpPower == 0.0f)return;
			jumpPower -= mulJumpPower;
			if (jumpPower <= 0.0f)
			{
				jumpPower = 0.0f;
				return;
			}
		}
		// 落下中
		else
		{
			jumpPower -= 0.024f;
			// もし地面についたら
			if (transform.position.y < 0.0f)
			{
				jumpPower = 0.0f;
				Vector3 pos = transform.position;
				pos.y = 0.0f;
				transform.position.Set(pos.x, pos.y, pos.z);
				jumpType = JumpType.JT_Ground;
				return;
			}
		}
		// 移動
		transform.Translate(0.0f, jumpPower, 0.0f);
	}

	public override void setInit()
	{
		base.setInit();
		jumpPower		= initJumpPower;
		nowJumpPower	= 0.0f;
		jumpType		= JumpType.JT_Ground;

		Yeti.animator.Play("jump");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

	// 地割れの生成
	public void createQuake()
	{
		print("地割れ発生");
		audio.PlayOneShot(shockVoice);

		// 生成
		Vector3 pos = transform.position;
		pos.y = 0.0f;

		// 衝撃波も出す
		GameObject hoge = Instantiate(wave, pos, Quaternion.identity) as GameObject;
		hoge.transform.parent = GameObject.Find("Game Element").transform;

		// 方向指定して地割れを生成するラムダ式
		Del del = f =>
		{
			Quaternion qvec = transform.rotation * Quaternion.AngleAxis(f, new Vector3(0, 1, 0));
			GameObject tmp = Instantiate(quake, pos, qvec) as GameObject;
			tmp.transform.parent = GameObject.Find("Game Element").transform;
		};

		// 正の最大値を出してそこから一定間隔ごと引いて生成
		int quakeNum = Yeti.furyFlag == true ? furyQuakeNum : normalQuakeNum;
		float num = (float)(quakeNum - (quakeNum-1)/2) * quakeRotate;
		for (int i = 1; i <= quakeNum; ++i)
		{
			del(num - (float)i * quakeRotate);
		}

	}

	// ジャンプ
	public void jump()
	{
		print("jump!!!!!!");
		audio.PlayOneShot(jumpVoice);
		jumpType = JumpType.JT_JUMP;
		nowJumpPower += jumpPower;
	}

	// 落下開始
	public void jumpDown()
	{
		jumpType = JumpType.JT_DOWN;
	}

}
