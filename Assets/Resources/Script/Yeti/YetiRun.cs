﻿using UnityEngine;
using System.Collections;

// プレイヤーを追いかける用
public class YetiRun : YetiAIBase {

	[Tooltip("プレイヤーを追尾する時の回転力、高いほど追尾性能が上がる")]
	[SerializeField, Range(0.0f, 1.0f)]
	private float ROTATE_POWER = 0.05f;
	[SerializeField]
	private float normalSpeed = 0.01f;
	[SerializeField]
	private float furySpeed	= 0.01f;
	[SerializeField]
	private float runningTime = 3.0f;
	[SerializeField, Tooltip("攻撃に移る射程距離")]
	private float range = 1.0f;

	[SerializeField]
	private Transform playerTrans;

	private float nowTime = 0.0f;

	// Use this for initialization
	void Start () {
		aiUpdate = calcRun;
	}
	
	void calcRun()
	{
		// プレイヤーの座標と自分の座標からプレイヤーへの方向ベクトル算出(ただしyは考えない)
		Vector3 myPos = gameObject.transform.position;
		Vector3 tarPos = playerTrans.position;
		myPos.y = tarPos.y = 0.0f;
		Vector3 targetDirect = (tarPos - myPos).normalized;

		// 4元数でやってみる
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);
		gameObject.transform.Translate(0.0f, 0.0f, Yeti.furyFlag == true ? furySpeed : normalSpeed);

		// プレイヤーに一定距離近づいたらキック、一定時間走ったら地割れ
		nowTime += Time.deltaTime;
		float distance = Vector3.Distance(transform.position, playerTrans.position);
		if (distance <= range)
		{
			nextAIType = EnemyAIType.AIYeti.Kick;
			exitFlag = true;
		}
		else if(nowTime >= runningTime)
		{
			nextAIType = EnemyAIType.AIYeti.Quake;
			exitFlag = true;
		}
	}

	public override void setInit()
	{
		base.setInit();
		Yeti.animator.Play(Yeti.furyFlag == true ? "run2" : "run1");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

}
