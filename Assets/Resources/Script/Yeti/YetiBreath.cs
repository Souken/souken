﻿using UnityEngine;
using System.Collections;

// ブレス時のAI
public class YetiBreath : YetiAIBase {

	[SerializeField]
	private GameObject breathObject;
	[SerializeField, Tooltip("")]
	private float createIntervalTime = 0.1f;
	[SerializeField]
	private AudioClip breathVoice;
	[SerializeField]
	private GameObject mouse;

	private float nowTime = 0.0f;
	private bool breathFlag = false;

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIYeti.Quake;
		breathObject.GetComponent<YetiBreathObject>().setAttack(GetComponent<Yeti>().getAttackPoint());
		breathObject.GetComponent<YetiBreathObject>().setMode(GetComponent<Yeti>().isEnemyBackMode());
	}
	
	void calcUpdate()
	{
		if (Yeti.state.normalizedTime >= 1.0f) { exitFlag = true; }

		if(breathFlag == true)
		{
			nowTime += Time.deltaTime;
			if (nowTime >= createIntervalTime)
			{
				nowTime = 0.0f;
				Quaternion directQ = mouse.transform.rotation;
				GameObject tmp = Instantiate(breathObject, mouse.transform.position, directQ) as GameObject;
				tmp.transform.parent = GameObject.Find("Game Element").transform;
			}
		}

	}

	public override void setInit()
	{
		base.setInit();
		Yeti.animator.Play("breath");
		audio.PlayOneShot(breathVoice);
		nowTime = 0.0f;
		breathObject.GetComponent<YetiBreathObject>().isFury = Yeti.furyFlag;
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;

		return true;
	}

	public void startBreath()
	{
		breathFlag = true;
	}

	public void exitBreath()
	{
		breathFlag = false;
	}

}
