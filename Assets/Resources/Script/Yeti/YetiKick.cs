﻿using UnityEngine;
using System.Collections;

// 近距離の蹴り(ふっとばし有)
public class YetiKick : YetiAIBase {

	[SerializeField] private GameObject kick;
	[SerializeField] private AudioClip kickSE;
	[SerializeField] private AudioClip kickVoice;
	[SerializeField, Tooltip("攻撃判定の生成位置, ボスからの相対距離")]
	private Vector3 createPos;

	// Use this for initialization
	void Start () {
		aiUpdate = calcWait;
		nextAIType = EnemyAIType.AIYeti.Quake;
		kick.GetComponent<YetiKickObject>().setAttack(GetComponent<Yeti>().getAttackPoint());
	}

	// 待機
	void calcWait()
	{
		if (Yeti.state.normalizedTime >= 1.0f) { exitFlag = true; }
	}

	public override void setInit()
	{
		base.setInit();
		Yeti.animator.Play("kick");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

	public void createKickObject()
	{
		print("攻撃判定生成");
		GameObject tmp = Instantiate(kick, transform.position + createPos, transform.rotation) as GameObject;
		tmp.transform.parent = transform;
		audio.PlayOneShot(kickSE);
		audio.PlayOneShot(kickVoice);
	}

	public void deleteKickObject()
	{
		// 攻撃オブジェクトの子を探して削除
		foreach (Transform child in transform)
		{
			if (child.name == "YetiKickObject(Clone)")
			{
				Destroy(child.gameObject); return;
			}
		}
	}
}
