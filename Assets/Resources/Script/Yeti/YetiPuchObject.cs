﻿using UnityEngine;
using System.Collections;

// パンチ時の当たり判定オブジェクト
public class YetiPuchObject : MonoBehaviour {

	private int		damage = 0;
	private bool	oneHitFlag = false;

	void Awake()
	{
		
	}

	// 初期設定(判定の大きさとダメージ量)
	public void initSet(Vector3 size, int arg_damage)
	{
		transform.localScale = size;
		damage = arg_damage;
	}
	
	void OnTriggerEnter(Collider other)
	{
		// まだ判定が一度も行われずプレイヤー以外は直ぐ抜ける
		if (oneHitFlag == true && other.gameObject.tag != "Player") { return; }
		Yeti.player.calcDamage(damage);
		Yeti.player.calcKnocked(transform.position, 0.1f);
		oneHitFlag = true;
		
	}
}
