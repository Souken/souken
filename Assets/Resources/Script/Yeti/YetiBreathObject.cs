﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// ブレスの判定オブジェクト
// 判定が行われたらそのオブジェクトの情報を取得して
// 一定時間経つまで判定を行わない
public class YetiBreathObject : MonoBehaviour {

	[SerializeField]
	private float damagePercent = 1.5f;
	[SerializeField]
	private int damage = 20;
	[SerializeField, Range(0.0f, 0.2f)]
	private float speed = 0.1f;
	[SerializeField, Tooltip("持続時間(秒)")]
	private float duration = 2.0f;
	[SerializeField, Tooltip("判定再開までの間隔")]
	private float judgmentResumeTime = 1.0f;

	private float durationTime = 0.0f;
	private float nowTime = 0.0f;
	private bool playerHitFlag = false;

	private GameObject player;
	[SerializeField]
	private bool isBackMode = false;
	[SerializeField]
	private float ROTATE_POWER = 0.01f;
	[SerializeField]
	public bool isFury = false;

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	public void setMode(bool mode) { isBackMode = mode; }

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
		// 生存時間
		durationTime += Time.deltaTime;
		if (durationTime >= duration) { Destroy(gameObject); }

		// 判定再開処理
		if (playerHitFlag == true)
		{
			nowTime += Time.deltaTime;
			if (nowTime >= judgmentResumeTime)
			{
				nowTime = 0.0f;
				playerHitFlag = false;
			}
		}

		// 裏モードの場合追尾させる
		if(SaveData.Instance.isBackMode() == true || isBackMode == true || isFury == true)
		{
			// 自分の座標と自分の座標から相手への方向ベクトル算出
			Vector3 myPos = transform.position;
			Vector3 tarPos = player.transform.position;
			Vector3 targetDirect = (tarPos - myPos).normalized;
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);

		}		
		// 移動
		transform.Translate(0.0f, 0.0f, speed);
	}

	// 攻撃判定
	void OnTriggerStay(Collider other)
	{
		// 当たっているかプレイヤー以外なら抜ける
		if (playerHitFlag == true || other.gameObject.tag != "Player") { return; }

		PlayerStatus player = other.gameObject.GetComponent<PlayerStatus>();
		player.calcDamage(damage);
		player.calcShrink();
		playerHitFlag = true;
	}

}
