﻿using UnityEngine;
using System.Collections;

public class GaugeProcessing : MonoBehaviour {

    [SerializeField] private Texture2D      frontImage;
    [SerializeField] private Texture2D      gaugeImage;

    [SerializeField] private string         statusName = "HPorSP";         // ゲージに使うステータス名(と言ってもHPかSP)
    [SerializeField] private float          barPercent = 0.0f;
    [SerializeField] private PlayerStatus   status;

    private Vector2 barSize;

	// リサイズ用
	// 1280*720を基準とする
	private float baseWidth = 1280.0f;
	private float baseHeight = 720.0f;

	// 縦横補正用(倍率指定)
	private float corTall = 1.0f;
	private float corWide = 1.0f;

	private float imageW;
	private float imageH;
	private float imageX;
	private float imageY;

	private Rect drawRectSize;

	// Use this for initialization
	void Start () {
		// 初期画像サイズ
		imageX = guiTexture.pixelInset.x;
		imageY = guiTexture.pixelInset.y;
		imageW = guiTexture.pixelInset.width;
		imageH = guiTexture.pixelInset.height;

		resizeImage();
	}

    void OnGUI()
    {
        // 改善版
        GUI.BeginGroup(new Rect(drawRectSize.x, drawRectSize.y, drawRectSize.width * barPercent, drawRectSize.height));
        GUI.DrawTexture(new Rect(0.0f, 0.0f, drawRectSize.width, drawRectSize.height), frontImage);
        GUI.EndGroup();
        GUI.DrawTexture(drawRectSize, gaugeImage);
    }
	
	// Update is called once per frame
	void Update () {
        if(statusName == "HP"){
            barPercent = (float)status.getHitPoint() / (float)status.getMaxHitPoint();
        }
        else if(statusName == "SP")
        {
            barPercent = status.getSkillPoint() / status.getMaxSkillPoint();
        }
        else
        {
            Debug.LogError("ステータス名が異常です");
        }
    }

	// リサイズ
	void resizeImage()
	{
		// 現在のウィンドウサイズとアスペクト比を取得
		float scWidth = Screen.width;
		float scHeight = Screen.height;
		float winAspect = scWidth / scHeight;

		// 基準サイズとの比率を計算
		float wRatio = 100.0f / (baseWidth / scWidth);
		float hRatio = 100.0f / (baseHeight / scHeight);

		// 縦横時の判別
		float ratio;
		if (scWidth < scHeight) { ratio = wRatio * corTall; }
		else { ratio = hRatio * corWide; }

		// リサイズサイズと表示位置
		int reimageeSizeW = (int)(imageW * (ratio / 100.0f));
		int reimageeSizeH = (int)(imageH * (ratio / 100.0f));
		int reimageeSizeX = (int)(imageX * (ratio / 100.0f));
		int reimageeSizeY = (int)(imageY * (ratio / 100.0f));

		drawRectSize = new Rect(reimageeSizeX, reimageeSizeY, reimageeSizeW, reimageeSizeH);
		guiTexture.pixelInset = drawRectSize;

		// GUI.DrawTexture用に修正
		drawRectSize = new Rect(reimageeSizeX, Screen.height - reimageeSizeH - reimageeSizeY, reimageeSizeW, reimageeSizeH);
	}

}
