﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// 敵の基底クラス
// 壊せるオブジェクトも敵とみなす
public class EnemyBase : CharacterBase {

    private static bool         initListFlag    = true;				// 初期化確認
    protected static List<bool> enemyIDList		= new List<bool>(); // 敵全体のIDを格納するリスト
    protected int id;												// プレイヤーの攻撃モーションによる多重判定防止用

	[SerializeField, Tooltip("デバッグ:裏モード状態にする")]
	protected bool isDebugBackMode = false;
	[SerializeField, Tooltip("裏モード時の攻撃力")]
	private int backModeAttack		= 50;
	[SerializeField, Tooltip("裏モード時の体力")]
	private int backModeHitpoint	= 750;

	public bool isEnemyBackMode() { return isDebugBackMode; }

    // リストの初期化
    public static void InitList()
    {
        // 要素が空なら抜ける
        if (initListFlag == true) { return; }
        initListFlag = true;
    }

    // リストの解放
    public static void ExitList()
    {
        // 要素が空なら抜ける
        if (initListFlag == false) { return; }
        enemyIDList.RemoveRange(0, enemyIDList.Count);
        initListFlag = false;
    }

    // idの取得
    public int getID() { return id; }

    // 基底Awakeで番号割り振り
    protected override void Awake()
    {
        // キャラクターベースでステータス初期化
        base.Awake();

        // 使われていない番号を探して割り当てる
        for (int i = 0; i < enemyIDList.Count; ++i)
        {
            if (enemyIDList[i] == false) 
            { 
                id = i;
                enemyIDList[i] = true;
                return;
            }
        }
        // ここまで来たら要素が空いていないので新しく末尾に追加
        enemyIDList.Add(true);
        id = enemyIDList.Count - 1;

		// 裏モードなら攻撃力&体力増加
		if (SaveData.Instance.isBackMode() == true || isDebugBackMode == true)
		{
			attackPoint = backModeAttack;
			MAX_HITPOINT = backModeHitpoint;
			hitPoint = MAX_HITPOINT;
		}
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update() {

	}

}
