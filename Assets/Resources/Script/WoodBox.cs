﻿using UnityEngine;
using System.Collections;

public class WoodBox : EnemyBase {

    
    protected override void Awake()
    {
        base.Awake();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void calcDamage(int damage)
    {
        gameObject.transform.Translate(0.0f, 0.0f, -0.1f);  // 適当に動かしてみる
        base.calcDamage(damage);
    }

}
