﻿using UnityEngine;
using System.Collections;

// 木を地面と一緒にループ移動させるスクリプト
// 草も可能
public class RollTree : MonoBehaviour {

	// スピード
	private static readonly Vector3 SPEED_VEC	= new Vector3(0.005f, 0.0f, -0.005f);
	// ループ位置
	private static readonly float ROOP_POS_X	= 20.0f; 
	// ワープ量
	private static readonly Vector3 WARP_VEC	= new Vector3(-40.0f, 0.0f, 40.0f);

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// 地面のスクロールと同じ
		gameObject.transform.Translate(SPEED_VEC);
		// Xが一定値に達したら
		if (gameObject.transform.position.x >= ROOP_POS_X)
		{
			gameObject.transform.Translate(WARP_VEC);
		}
	}
}
