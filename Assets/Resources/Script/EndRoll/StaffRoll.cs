﻿using UnityEngine;
using System.Collections;

// スタッフロールに使用する
public class StaffRoll : MonoBehaviour {

	[SerializeField, Tooltip("ファイル名")]
	private string	fileName = "staffRoll";
	[SerializeField, Tooltip("スタッフロールの画像枚数")]
	private int		texNum = 7;
	[SerializeField, Tooltip("1枚辺りの待機時間")]
	private float	waitTime = 3.0f;
	[SerializeField, Tooltip("移動に要するフレーム数")]
	private int		moveFrame = 60;
	[SerializeField]
	private AudioSource bgm;

	private enum NowMode { NM_Wait, NM_StartMove, NM_EndMove, NM_End };
	[SerializeField, Tooltip("状態確認用")]
	private NowMode nowMode = NowMode.NM_StartMove;

	// 関数の中身を更新して処理
	// trueが返ってきたら現在の処理は完了とみなし、中身を替える
	private delegate bool UpdateMode();
	private UpdateMode updateMode;

	private Texture2D[] tex;		// 待機表示画像
	private Texture2D[] moveTex;	// 移動表示画像(要は残像)
	private float		nowTime		= 0.0f;
	private int			nowTexNum	= 0;

	private Color	texColor	= new Color(0.5f, 0.5f, 0.5f, 0.0f);
	private Vector2	startMovePos;
	private Rect	nowRect;
	private float	frameAlpha;		// 1フレーム辺りの透明度の加算・減算度
	private float	frameMovePower;	// 1フレーム辺りの移動量

	[SerializeField, Tooltip("BGMの長さとの兼ね合いでどうしても少し延長したい時に使用")]
	private float	extensionSeconds = 2.0f;
	[SerializeField, Tooltip("移動確認用")]
	private int		nowMoveFrame = 0;
	[SerializeField, Tooltip("終了確認用")]
	private bool	isEnd = false;

	void Awake()
	{
		FadeManager.Instance.startFadeIn(0.05f);
		// 画像読み込み
		tex = new Texture2D[texNum];
		moveTex = new Texture2D[texNum];
		for(int i = 0; i < texNum; ++i)
		{
			tex[i]		= Resources.Load("UI/EndRoll/" + fileName + i.ToString()) as Texture2D;
			moveTex[i]	= Resources.Load("UI/EndRoll/" + fileName + "Wind" + i.ToString()) as Texture2D;
		}
	}

	// Use this for initialization
	void Start () {
		// 最初は移動なので移動用テクスチャ設定
		guiTexture.texture = moveTex[nowTexNum];

		startMovePos			= new Vector2(guiTexture.pixelInset.width, 0.0f);
		nowRect					= new Rect(startMovePos.x, startMovePos.y, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
		guiTexture.pixelInset	= nowRect;
		guiTexture.color		= texColor;

		frameAlpha		= 0.5f / (float)moveFrame;
		frameMovePower	= startMovePos.x / (float)moveFrame;

		updateMode = timeAdjustment;
	}
	
	// Update is called once per frame
	void Update () {
		if (isEnd == true) return;

		// もう一回見ていたら飛ばせるように
		if(SaveData.Instance.getIsReleaseBackMode() == true && Input.GetButtonDown("Attack") == true)
		{
			FadeManager.Instance.loadLevel("StageSelect", 0.01f);
			isEnd = true;
		}

		if (updateMode() == false) return;

		// 処理を切り替える
		switch (nowMode)
		{
			case NowMode.NM_StartMove:
				updateMode = wait;
				nowMode = NowMode.NM_Wait;
				break;
			case NowMode.NM_Wait: 
				updateMode = endMove;
				nowMode = NowMode.NM_EndMove;
				break;
			case NowMode.NM_EndMove:
				updateMode = startMove;
				nowMode = NowMode.NM_StartMove;
				break;
			default:
				updateMode = endScene;
				nowMode = NowMode.NM_End;
				return;
		}
		// 画像の差し替え
		guiTexture.texture = nowMode == NowMode.NM_Wait ? tex[nowTexNum] : moveTex[nowTexNum];
	}

	// 時間調整用
	bool timeAdjustment()
	{
		nowTime += Time.deltaTime;
		if (nowTime < extensionSeconds) return false;
		nowTime = 0.0f;
		nowMode = NowMode.NM_EndMove;
		return true;
	}

	// 指定時間待機
	bool wait()
	{
		nowTime += Time.deltaTime;
		if(nowTime < waitTime)	return false;
		nowTime = 0.0f;
		// 「完」までいったらフェードアウトしてタイトルへ
		if (nowTexNum == texNum - 1) { nowMode = NowMode.NM_End; }
		return true;
	}

	// 右から中央に移動しつつ徐々に実体化
	bool startMove()
	{
		++nowMoveFrame;
		if (nowMoveFrame == moveFrame)
		{
			nowRect.Set(0.0f, 0.0f, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
			texColor.a = 0.5f;
			nowMoveFrame = 0;
		}
		else
		{
			nowRect.Set(nowRect.position.x - frameMovePower, 0.0f, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
			texColor.a += frameAlpha;
		}

		guiTexture.pixelInset = nowRect;
		guiTexture.color = texColor;
		return nowMoveFrame == 0 ? true : false;
	}

	// 中央から右に移動しつつ徐々に透明化
	bool endMove()
	{
		++nowMoveFrame;
		if (nowMoveFrame == moveFrame)
		{
			nowRect.Set(startMovePos.x, startMovePos.y, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
			texColor.a		= 0.0f;
			nowMoveFrame	= 0;
			// 今はループ
			nowTexNum		= nowTexNum == texNum - 1 ? 0 : nowTexNum + 1;
		}
		else
		{
			nowRect.Set(nowRect.position.x - frameMovePower, 0.0f, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
			texColor.a -= frameAlpha;
		}

		guiTexture.pixelInset	= nowRect;
		guiTexture.color		= texColor;
		return nowMoveFrame == 0 ? true : false;
	}

	// 終わり
	bool endScene()
	{
		// 徐々に下げる
		bgm.volume -= 0.001f;
		if (isEnd == true) return false;

		// もし裏モードが解放されていなかったら解放する
		if (SaveData.Instance.getIsReleaseBackMode() == false)
		{
			SaveData.Instance.isReleaseBackMode = true;
			SaveData.Instance.isNowMode = true;
		}

		FadeManager.Instance.loadLevel("StageSelect", 0.001f);
		isEnd = true;
		return false;
	}

}
