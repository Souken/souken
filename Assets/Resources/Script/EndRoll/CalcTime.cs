﻿using UnityEngine;
using System.Collections;

// 時間計測用
public class CalcTime : MonoBehaviour {

	float time = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		guiText.text = time.ToString();

		// デバッグ用
		if (Input.anyKeyDown == true) Application.Quit();
	}
}
