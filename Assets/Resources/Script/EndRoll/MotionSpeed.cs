﻿using UnityEngine;
using System.Collections;

public class MotionSpeed : MonoBehaviour {

	[SerializeField, Range(0.0f, 2.0f)]
	private float motionSpeed = 1.0f;

	private Animator animator;

	void Awake()
	{
		animator = GetComponent<Animator>();
		animator.speed = motionSpeed;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
