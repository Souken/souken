﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour
{
    [SerializeField] GameObject pauseSet; 

    public enum GAME_STATE
    {
        ACTIVE,
        PAUSE
    };
    [SerializeField] GAME_STATE state;

    public bool GameIsActive() { return (state == GAME_STATE.ACTIVE); }
    public void ExitGame() {
        Time.timeScale = 1.0f;
    }



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetButtonDown("Start") )
        {
            SwitchPauseMode();
            SwitchDynamicObject();
        }
        */
    }


    public void SwitchPauseMode()
    {
        state = (state == GAME_STATE.ACTIVE) ? GAME_STATE.PAUSE : GAME_STATE.ACTIVE;
        SwitchDynamicObject();
    }

    void SwitchDynamicObject()
    {
        Behaviour[] bhvs = this.GetComponentsInChildren<Behaviour>();

        //もしゲームがアクティブ状態になっていたら
        if (state == GAME_STATE.ACTIVE)
        {
            //ゲーム内の時間を動かす
            Time.timeScale = 1.0f;

            //オブジェクトの動きにかかわるコンポネントを全て有効に
            for (int i = 0; i < bhvs.Length; ++i)
            {
                bhvs[i].enabled = true;
            }

            //ポーズ中に表示される項目を無効に
            Behaviour[] psBhvs = pauseSet.GetComponentsInChildren<Behaviour>();
            for (int i = 0; i < psBhvs.Length; ++i)
            {
                //PauseのScriptをOFFにしないようにする
                Pause pause = psBhvs[i].GetComponent<Pause>();
                if (pause != null)
                {
                    continue;
                }

                psBhvs[i].enabled = false;
            }
        }

        //もしゲームがポーズ状態になっていたら
        if (state == GAME_STATE.PAUSE)
        {
            //ゲーム内の時間をストップ
            Time.timeScale = 0.0f;

            //オブジェクトの動きにかかわるコンポネントを全て有効に
            for (int i = 0; i < bhvs.Length; ++i)
            {
                //自分をOFFにしないようにする
                GameState gs_check = bhvs[i].GetComponent<GameState>();
                if (gs_check != null)
                {
                    continue;
                }

                bhvs[i].enabled = false;
            }

            //ポーズ中に表示される項目を有効に
            Behaviour[] psBhvs = pauseSet.GetComponentsInChildren<Behaviour>();
            for (int i = 0; i < psBhvs.Length; ++i)
            {
                psBhvs[i].enabled = true;
            }
        }
    }
}
