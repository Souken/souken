﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

// 暗号化と復号化を提供する静的クラス
public static class Cryption{
	// 暗号化に用いる文字列
	static string PASS_KEY = "37564";

	// 引数の文字列を暗号化して返す
	public static string encryption(string arg_str)
	{
		byte[] key = Encoding.UTF8.GetBytes(PASS_KEY);
		byte[] data = Encoding.UTF8.GetBytes(arg_str);
		
		int j = 0; string str = "";
		for (int i = 0; i < data.Length; ++i)
		{
			j = j < key.Length ? j + 1 : 1;				// keyがdataより要素数が少ない時のため
			data[i] = (byte)(data[i] ^ key[j - 1]);		// XORで暗号化
			str += string.Format("{0:000}", data[i]);	// 3ケタにして追加していく
		}
		return str;
	}

	// 引数の文字列を複合化
	public static string decryption(string arg_str)
	{
		byte[] key = Encoding.UTF8.GetBytes(PASS_KEY);
		byte[] data = new byte[arg_str.Length / 3];		// 3ケタ区切りしたので文字数の1/3のbyte配列用意
		// 文字数の1/3回繰り返す
		for (int i = 0; i < arg_str.Length / 3; ++i)
		{
			data[i] = byte.Parse(arg_str.Substring(i * 3, 3));	// 3桁ずつbyte変換して格納
		}

		int j = 0;
		for (int i = 0; i < data.Length; ++i)
		{
			j = j < key.Length ? j + 1 : 1;				// keyがdataより要素数が少ない時のため
			data[i] = (byte)(data[i] ^ key[j - 1]);		// XORで暗号化
		}
		return Encoding.UTF8.GetString(data);
	}

}
