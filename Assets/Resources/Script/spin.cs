﻿using UnityEngine;
using System.Collections;

public class spin : MonoBehaviour
{

    [SerializeField]
    private float speed = 3.0f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 360.0f * speed * Time.deltaTime, 0);
    }
}
