﻿using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour {

    public GameObject camera = null;

	// Use this for initialization
	void Start () {
        camera = GameObject.Find("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 vec = gameObject.transform.position;
        vec -= camera.transform.position;
/*
        cPos.x = -cPos.x;
        cPos.y = -cPos.y;
        cPos.z = -cPos.z;
*/
        vec += gameObject.transform.position;
        
        gameObject.transform.LookAt(vec);
	}
}
