﻿using UnityEngine;
using System.Collections;

// 草原に生えている草などを2Dテクスチャで立体的に見せる時に使用
public class BillBoard : MonoBehaviour {

	[SerializeField, Tooltip("真上・真下から見た時にもビルボードさせるか")]
	private bool yAxisBillBoard = false;
	[SerializeField]
	private Camera camera;

	// Use this for initialization
	void Start () {
		// 設定されていなかったら
		if (camera == null)
		{
			Debug.LogWarning("設定されていなかったのでメインカメラを設定しました");
			camera = Camera.main;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (yAxisBillBoard == true) 
		{
			transform.LookAt(camera.transform.position);
		}
		else
		{
			// 向く方向のY座標を自分の高さに設定
			Vector3 target = camera.transform.position;
			target.y = transform.position.y;
			transform.LookAt(target);
		}
	}
}
