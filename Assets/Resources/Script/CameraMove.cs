﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour
{
    public GameObject pObj = null;
    public GameObject eObj = null;

    //向いている方向のキャッシュ
    public Vector3 dir;
    //目標点
    Vector3 ftPos;

    //追従の遅さ
    public float delay = 1.0f;
    public float delayY = 1.0f;

    //カメラを自機から離す量
    public float defDist = 2.5f;
    float dist = 0.0f;
    //カメラが自機からどれくらい上を向くか
    public float upper = 1.5f;

    //今向いている向きの Acos, Asin の値
    float angleXZ, angleY;
    //左右を向く遅さ
    float aDelay = 30.0f;

    //注目モード
    bool lockonMode = false;

    // Use this for initialization
    void Start()
    {
        ftPos = gameObject.transform.position;
        angleXZ = 4.71f;

        dir.z = 1.0f;
        dist = defDist;

        SphereCollider sphColl = gameObject.GetComponent<SphereCollider>();
        BoxCollider boxColl = pObj.GetComponent<BoxCollider>();
        if (!sphColl || !boxColl)
        {
            Debug.LogError("player OR camera has'nt attached [collider]");
            return;
        }
        /*
        //自機のColliderがカメラのColliderより大きいと問題なので矯正
        float boxCollRad = (boxColl.center - boxColl.bounds.min).magnitude;
        if ((sphColl.radius) > boxCollRad)
        {
            Debug.LogWarning("player's [collider] is smaller than camera's that : it will clamp scale to player's");
            sphColl.radius = boxCollRad;
        }
         */
    }

    // Update is called once per frame
    void Update()
    {
		/*
        if (Input.GetButtonDown("Lock"))
        {
            lockonMode = !(lockonMode);
        }
		*/
        if (!lockonMode)
        {
            ToPlayer(pObj);
        }
        else
        {
            if (eObj)
            {
                Lockon(pObj, eObj);
            }
        }

        gameObject.rigidbody.MovePosition(ftPos);
    }

    //CameraType3-----------------------------------------------------

    void SameDirectionOfPlayer(GameObject player)
    {
        dir = player.transform.forward;

        angleXZ -= Input.GetAxisRaw("R_Horizontal") / aDelay;
        angleY += Mathf.Asin(dir.y) * aDelay;

        dir.x = Mathf.Cos(angleXZ);
        dir.z = Mathf.Sin(angleXZ);
    }

    void ToPlayer(GameObject player)
    {
        SameDirectionOfPlayer(player);
        dir.Normalize();
        dist = defDist;

        //自機の位置から少し離した位置に移動先を決定
        ftPos = new Vector3(player.transform.position.x - dir.x * defDist
                            , player.transform.position.y - dir.y * defDist + 1.0f
                            , player.transform.position.z - dir.z * defDist);

        //Rayの当り判定チェック
        //pObj.collider.enabled = false;
        //自機と衝突物とのむきに対して
        Ray ray = new Ray(pObj.transform.position, -dir);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, dist, 8 | 9))
        {
            //print("hitcamera");

            pObj.collider.enabled = true;
            print(hit.distance);

            if (hit.distance < 0.0001f)
            {
                return;
            }

            SphereCollider spColl = gameObject.GetComponent<SphereCollider>();

            //衝突点と自機のベクトルを得る
            Vector3 hitPoint = ray.GetPoint(hit.distance - spColl.radius * 2.0f);
            hitPoint.y += ftPos.y;
            gameObject.transform.position = hitPoint;
            ftPos.x = hitPoint.x;
            ftPos.z = hitPoint.z;

            Debug.DrawLine(pObj.transform.position, hitPoint);
        }

        pObj.collider.enabled = true;
        //少し上にカメラをずらす 
        Vector3 tPos = player.transform.position;
        tPos.y += upper;
        gameObject.transform.LookAt(tPos);

        delay = 5.0f;


        ftPos[0] += (gameObject.transform.position[0] - ftPos[0]) / delay;
        ftPos[1] += (gameObject.transform.position[1] - ftPos[1]) / delayY;
        ftPos[2] += (gameObject.transform.position[2] - ftPos[2]) / delay;

    }

    void Lockon(GameObject player, GameObject enemy)
    {
        SameDirectionOfPlayer(player);
        dir.Normalize();

        //敵の向きを元に、自機基準の向きを自機と敵基準に
        Vector3 vec = (enemy.transform.position - player.transform.position);
        dist = vec.magnitude;
        dir = vec.normalized;// *dist / 50 + dir * (50 - dist) / 50;

        //再決定された距離と向きを元に移動先を決定

        ftPos = new Vector3(player.transform.position.x - (dir.x * (dist / 5 + 1.0f)) //Mathf.Max( (dir.x * defDist), (dir.x * dist / 5) )
                             , player.transform.position.y + (Mathf.Abs(dir.y) * 2.0f + dist) / 10.0f
                             , player.transform.position.z - (dir.z * (dist / 5 + 1.0f)));//Mathf.Max( (dir.z * defDist), (dir.z * dist / 5) ));

        //Rayの当り判定チェック
        //自機と衝突物とのむきに対して
        Ray ray = new Ray(pObj.transform.position + dir, -dir);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, defDist, 8 | 9 | 11))
        {

            //衝突点と自機のベクトルを得る
            Vector3 hitPoint = ray.GetPoint(hit.distance - gameObject.transform.localScale.x * 2.0f);

            hitPoint.y = ftPos.y;
            gameObject.transform.position = hitPoint;
            ftPos = hitPoint;

            Debug.DrawLine(pObj.transform.position, hitPoint);
        }

        //少し上にカメラをずらす 
        Vector3 tPos = enemy.transform.position;
        tPos.y += dist / 10;
        gameObject.transform.LookAt(tPos);

        delay = 5.0f;


        ftPos[0] += (gameObject.transform.position[0] - ftPos[0]) / delay;
        ftPos[1] += (gameObject.transform.position[1] - ftPos[1]) / delayY;
        ftPos[2] += (gameObject.transform.position[2] - ftPos[2]) / delay;

    }
}
