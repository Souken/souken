﻿using UnityEngine;
using System.Collections;

public class Omake : MonoBehaviour {

	// リサイズ用
	// 1280*720を基準とする
	private float baseWidth = 1280.0f;
	private float baseHeight = 720.0f;

	// 縦横補正用(倍率指定)
	private float corTall = 1.0f;
	private float corWide = 1.0f;

	private int fontSize;

	// ピクセルオフセット
	private float pixX;
	private float pixY;

	bool ko = false;

	// Use this for initialization
	void Start () {
		FadeManager.Instance.startFadeIn(0.05f);

		guiText.text = "体験版はここまでです\n遊んでいただきありがとうございました\n \n完成版をお楽しみに!\n攻撃ボタンを押すとタイトルへ戻ります";

		// 初期設定取得
		fontSize = guiText.fontSize;
		pixX = guiText.pixelOffset.x;
		pixY = guiText.pixelOffset.y;

		// 最初にリサイズしてしまう
		resizeFont();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (FadeManager.Instance.isMaxFadeIn() && Input.GetButtonDown("Attack") == true) { FadeManager.Instance.loadLevel("Title", 0.01f); }
	}

	// リサイズ関数
	void resizeFont()
	{
		// 現在のウィンドウサイズとアスペクト比を取得
		float scWidth = Screen.width;
		float scHeight = Screen.height;
		float winAspect = scWidth / scHeight;

		// 基準サイズとの比率を計算
		float wRatio = 100.0f / (baseWidth / scWidth);
		float hRatio = 100.0f / (baseHeight / scHeight);

		// 縦横時の判別
		float ratio;
		if (scWidth < scHeight) { ratio = wRatio * corTall; }
		else { ratio = hRatio * corWide; }

		// リサイズするフォントサイズ
		int reFontSize = (int)((float)fontSize * (ratio / 100.0f));
		// リサイズフォント位置
		int rePixOffsetX = (int)((float)pixX * (ratio / 100.0f));
		int rePixOffsetY = (int)((float)pixY * (ratio / 100.0f));

		// 変更
		guiText.fontSize = reFontSize;
		guiText.pixelOffset.Set(rePixOffsetX, rePixOffsetY);
	}
}
