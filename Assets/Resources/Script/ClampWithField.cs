﻿using UnityEngine;
using System.Collections;

public class ClampWithField : MonoBehaviour {

    //フィールドにスクリプトを持たせる場合
    [SerializeField] GameObject[] objs;    


    [SerializeField] CapsuleCollider cpsl;

    [SerializeField] float clampEps;


	// Use this for initialization
	void Start () {

	    //指定されてなければ
        if( cpsl == null )
        {
           //アタッチされた球オブジェクトの半径から、クランプする領域を決定
            cpsl = GetComponent<CapsuleCollider>();
        }

        //クランプの値が大きすぎないか検査
        if (2.0f < clampEps)
            clampEps = 2.0f;

        clampEps += 1.0f;
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < objs.Length; ++i )
        {
            ClampField(objs[i]);
        }

        
        /*
        //y軸を考慮しない計算用のベクトルを用意

        Vector3 objPt = obj.transform.position + obj.rigidbody.velocity;
        Vector3 fldPt = transform.position;

        objPt.y = 0;
        fldPt.y = 0;

        //対象とフィールドのベクトルを用意
        Vector3 vec = (fldPt - objPt);
        //print(vec.magnitude - cpsl.radius);

        //フィールドの範囲から出てないかチェック
        if (vec.magnitude <= cpsl.radius )
        {
            //出てなければ終了
            return;
        }

        //ここから出てた場合

        //どれくらいはみ出しているかを出して        
        float clampPrm = vec.magnitude - (cpsl.radius);
        vec *= ((clampPrm) / vec.magnitude );

      

        //修正値(係数)を適用して戻す
        Vector3 integratedPos = (obj.transform.position + vec);
        integratedPos.y = obj.transform.position.y;
        obj.rigidbody.MovePosition(integratedPos);
        //obj.rigidbody.velocity = Vector3.zero;
	    */
	}



    //-------------------------------------------------------
    //
    //使用用途的に、これが毎フレーム呼ばれるはず
    //
    //衝突時の処理を行うわけではなく、
    //Unityの物理シミュレーション直後のプロセスはこれだけなので
    //ここで座標を無理やり動かしている

    void ClampField(GameObject obj)
    {
        //y軸を考慮しない計算用のベクトルを用意

        Vector3 objPt = obj.transform.position + obj.rigidbody.velocity;
        Vector3 fldPt = transform.position;

        objPt.y = 0;
        fldPt.y = 0;

        //対象とフィールドのベクトルを用意
        Vector3 vec = (fldPt - objPt);
        //print(vec.magnitude - cpsl.radius);

        //フィールドの範囲から出てないかチェック
        if (vec.magnitude <= cpsl.radius)
        {
            //出てなければ終了
            return;
        }

        //ここから出てた場合

        //どれくらいはみ出しているかを出して        
        float clampPrm = vec.magnitude - (cpsl.radius);
        vec *= ((clampPrm) / vec.magnitude);



        //修正値(係数)を適用して戻す
        Vector3 integratedPos = (obj.transform.position + vec);
        integratedPos.y = obj.transform.position.y;
		obj.transform.position = integratedPos;
		//obj.rigidbody.MovePosition(integratedPos);
        //obj.rigidbody.velocity = Vector3.zero;
	        
    }

    /*
    void OnTriggerStay(Collider coll)
    {
        //y軸を考慮しない計算用のベクトルを用意

        Vector3 objPt = obj.transform.position + obj.rigidbody.velocity;
        Vector3 fldPt = transform.position;

        objPt.y = 0;
        fldPt.y = 0;

        //対象とフィールドのベクトルを用意
        Vector3 vec = (fldPt - objPt);
        //print(vec.magnitude - cpsl.radius);

        //フィールドの範囲から出てないかチェック
        if (vec.magnitude < cpsl.radius)
        {
            //出てなければ終了
            return;
        }

    //ここから出てた場合
        
        //どれくらいはみ出しているかを出して        
        float clampPrm = vec.magnitude - (cpsl.radius);
        vec *= (clampPrm / vec.magnitude);
        
        print(clampPrm);
        Debug.DrawLine(cpsl.collider.ClosestPointOnBounds(obj.transform.position), obj.transform.position);


        //修正値(係数)を適用して戻す
        //clmpVec *= (clampEps);

        Vector3 integratedPos = (obj.transform.position + vec);
        obj.transform.position = integratedPos;


    }
    */
    
}
