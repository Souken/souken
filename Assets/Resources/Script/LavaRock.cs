﻿using UnityEngine;
using System.Collections;

// 最終ステージの周りではねてるもの
public class LavaRock : MonoBehaviour {

	[SerializeField]
	private float distance = 0.0f;
	float nowTime = 0.0f;

	float addTime = 0.0f;
	Vector3 initPos;

	// Use this for initialization
	void Start () {
		distance = Random.RandomRange(0.1f, 1.0f);
		initPos = transform.position;

		addTime = Random.RandomRange(0.016f, 0.032f);
	}
	
	// Update is called once per frame
	void Update () {
		nowTime += addTime;
		transform.Translate(0.0f, distance * Mathf.Cos(90.0f * nowTime * Mathf.Deg2Rad), 0.0f);

		// 2秒経ったらまた最初から
		if (nowTime >= 2.0f)
		{
			transform.position = initPos;
			nowTime = 0.0f;
		}

	}
}
