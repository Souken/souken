﻿using UnityEngine;
using System.Collections;

public class TpInCam : MonoBehaviour
{

    //接触直後のカメラから自身との最近接点の距離
    Vector3 cameraToClosest;
    Material cashMat;

    // Use this for initialization
    void Start()
    {
        cashMat = gameObject.renderer.material;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        //物体にカメラが当たったら
        if (col.gameObject.layer == 8)
        {
            //接触直後のカメラから自身との最近接点の距離を得る
            cameraToClosest = collider.ClosestPointOnBounds(col.gameObject.transform.position);
            cameraToClosest -= col.gameObject.transform.position;

            //透過に対応するために、Materialを差し替える
            Material swMat = cashMat;
            swMat.shader = Shader.Find("Transparent/Diffuse");
            gameObject.renderer.material = swMat;

            //print("(´･ω･｀)");
        }
    }

    void OnTriggerStay(Collider col)
    {
        //物体にカメラが当たったら
        if (col.gameObject.layer == 8)
        {
            //カメラから自身との最近接点の距離を得る
            Vector3 pos = collider.ClosestPointOnBounds(col.gameObject.transform.position);
            pos -= col.gameObject.transform.position;

            //ratio = カメラと最近接点の距離 / 接触時のカメラから自身との最近接点の距離
            float ratio = (pos.magnitude) / cameraToClosest.magnitude;

            Color color = gameObject.renderer.material.color;
            color.a = ratio;
            gameObject.renderer.material.color = color;
        }
    }
    void OnTriggerExit(Collider col)
    {
        //物体にカメラが当たったら
        if (col.gameObject.layer == 8)
        {
            Color color = gameObject.renderer.material.color;
            color.a = 1.0f;
            gameObject.renderer.material.color = color;

            //衝突中に変更しておいたマテリアルを元に戻す
            Material swMat = gameObject.renderer.material;
            swMat.shader = Shader.Find("Diffuse");
            gameObject.renderer.material = cashMat;
        }
    }
}
