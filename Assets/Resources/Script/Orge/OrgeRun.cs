﻿using UnityEngine;
using System.Collections;

public class OrgeRun : OrgeAIBase {

	[Tooltip("プレイヤーを追尾する時の回転力、高いほど追尾性能が上がる")]
	[SerializeField, Range(0.0f, 1.0f)]
	private float ROTATE_POWER = 0.01f;
	[SerializeField]
	private float normalSpeed = 0.01f;
	[SerializeField]
	private float furySpeed = 0.01f;
	[SerializeField]
	private float runningTime = 3.0f;
	[SerializeField, Tooltip("攻撃に移る射程距離")]
	private float range = 1.0f;

	[SerializeField]
	private Transform playerTrans;

	private float nowTime = 0.0f;

	// Use this for initialization
	void Start () {
		aiUpdate = calcRun;
	}
	
	void calcRun()
	{
		// プレイヤーの座標と自分の座標からプレイヤーへの方向ベクトル算出(ただしyは考えない)
		Vector3 myPos = gameObject.transform.position;
		Vector3 tarPos = playerTrans.position;
		myPos.y = tarPos.y = 0.0f;
		Vector3 targetDirect = (tarPos - myPos).normalized;

		// 4元数でやってみる
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);
		gameObject.transform.Translate(0.0f, 0.0f, Yeti.furyFlag == true ? furySpeed : normalSpeed);

		// プレイヤーに一定距離近づいたらなぎ払い、一定時間走ったらジャンプ
		nowTime += Time.deltaTime;
		float distance = Vector3.Distance(transform.position, playerTrans.position);
		if (distance <= range)
		{
			exitFlag = true;
			nextAIType = EnemyAIType.AIOrge.AttackRoll;
		}
		else if(nowTime >= runningTime)
		{
			exitFlag = true;
			nextAIType = Random.Range(0, 3) == 0 ? EnemyAIType.AIOrge.Jump : EnemyAIType.AIOrge.AttackUnder;
		}
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.Play("run");
		nowTime = 0.0f;
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

}
