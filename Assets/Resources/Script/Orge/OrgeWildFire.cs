﻿using UnityEngine;
using System.Collections;

public class OrgeWildFire : MonoBehaviour {

	[Tooltip("プレイヤーを追尾する時の回転力、高いほど追尾性能が上がる")]
	[SerializeField, Range(0.0f, 0.5f)]
	private float ROTATE_POWER = 0.05f;
	[SerializeField]
	private float damagePercent;
	[SerializeField]
	private int damage;
	private GameObject target;
	private int nowMyFrame = 0;
	[SerializeField]
	private float mySpeed = 0.2f;
	[SerializeField]
	private int DELETE_FRAME = 180;

	private int nowFrame = 0;

	void Awake()
	{
		damagePercent = 0.8f;
	}

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	// Use this for initialization
	void Start()
	{
		Color[] colors = new Color[5];
		for (int i = 4; i >= 0; --i)
		{
			float num = i;
			num = num / 10.0f + 0.1f;
			colors[i] = new Color(1.0f, num, 0.0f, 0.5f - (num - 0.1f));
		}

		ParticleAnimator particleAnimator = GetComponent<ParticleAnimator>();
		Color[] modifiedColors = particleAnimator.colorAnimation;
		particleAnimator.colorAnimation = colors;

		target = GameObject.Find("Player");
		transform.LookAt(target.transform);
	}

	// Update is called once per frame
	void Update()
	{
		if (++nowFrame >= DELETE_FRAME) { Destroy(gameObject); }

		// プレイヤーの座標と自分の座標からプレイヤーへの方向ベクトル算出
		Vector3 myPos = transform.position;
		Vector3 tarPos = target.transform.position;
		Vector3 targetDirect = (tarPos - myPos).normalized;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);

		gameObject.transform.Translate(new Vector3(0.0f, 0.0f, mySpeed));
	}

	// 当たり判定(取り敢えず何かに当たったら消す、プレイヤーの場合ダメージを)
	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			PlayerStatus player = collider.gameObject.GetComponent<PlayerStatus>();
			player.calcDamage(damage);
			player.calcShrink();
			Instantiate((GameObject)Resources.Load("Prefab/fire"), collider.gameObject.transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}

}
