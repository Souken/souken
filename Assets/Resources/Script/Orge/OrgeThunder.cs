﻿using UnityEngine;
using System.Collections;

public class OrgeThunder : OrgeAIBase {

	[SerializeField]
	private Transform	playerTrans;
	[SerializeField]
	private GameObject	thunderPointObject;
	[SerializeField]
	private GameObject	shockWaveObject;
	[SerializeField]
	private AudioClip	thunderSE;
	[SerializeField]
	private float endThunderTime = 5.0f;
	[SerializeField, Tooltip("プレイヤー中心座標からの雷発生範囲の半径")]
	private float createThunderCircleRadius = 8.0f;
	[SerializeField, Tooltip("一回の生成個数")]
	private int createNum = 3;

	private float	nowTime = 0.0f;
	private Vector3 playerCenterPos;

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIOrge.Wait;
		thunderPointObject.GetComponent<OrgeThunderPoint>().setAttack(GetComponent<Orge>().getAttackPoint());

		playerCenterPos = playerTrans.position;

		// 裏モードの場合1.5倍に
		if (SaveData.Instance.isBackMode() == true || GetComponent<Orge>().isEnemyBackMode() == true)
		{
			createNum = (int)((float)createNum * 1.5f);
		}

	}
	
	// Update is called once per frame
	void calcUpdate () {
		if (Orge.state.IsName("exitThunderMotion") == true && Orge.state.normalizedTime >= 1.0f)
		{
			exitFlag = true; return;
		}

		// 落雷へ移行
		if (nowTime != endThunderTime && Orge.state.IsName("thunderLoop") == true)
		{
			aiUpdate = calcThunderbolt; return;
		}
	}

	// 落雷計算
	void calcThunderbolt()
	{
		playerCenterPos = playerTrans.position;

		nowTime += Time.deltaTime;
		if (nowTime >= endThunderTime)
		{
			nowTime = endThunderTime;
			Orge.animator.SetTrigger("exitThunderTrigger");
			aiUpdate = calcUpdate;
			return;
		}
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.SetTrigger("thunderAttackTrigger");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		nowTime = 0.0f;

		return true;
	}

	public void createThunderPoint()
	{
		//print(playerCenterPos);
		for(int i = 0; i < createNum; ++i)
		{
			Vector3 pos = new Vector3(
			Random.Range(playerCenterPos.x - createThunderCircleRadius, playerCenterPos.x + createThunderCircleRadius), 0.01f,
			Random.Range(playerCenterPos.z - createThunderCircleRadius, playerCenterPos.z + createThunderCircleRadius)
			);
			GameObject tmp = Instantiate(thunderPointObject, pos, Quaternion.identity) as GameObject;
			tmp.transform.parent = GameObject.Find("Game Element").transform;
		}

		// 裏モードの場合1つおまけで、必ず現在位置に落とす
		if (SaveData.Instance.isBackMode() == true || GetComponent<Orge>().isEnemyBackMode() == true)
		{
			GameObject tmp = Instantiate(thunderPointObject, playerCenterPos, Quaternion.identity) as GameObject;
			tmp.transform.parent = GameObject.Find("Game Element").transform;
		}

	}
	
}
