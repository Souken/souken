﻿using UnityEngine;
using System.Collections;

public class OrgeAttackUnder : OrgeAIBase {

	[SerializeField]
	private GameObject	attackObject;
	[SerializeField]
	private GameObject	waveObject;
	[SerializeField]
	private AudioClip	chargeSE;
	[SerializeField]
	private AudioClip	attackSE;
	[Tooltip("プレイヤーを追尾する時の回転力、高いほど追尾性能が上がる")]
	[SerializeField, Range(0.0f, 0.5f)]
	private float ROTATE_POWER = 0.01f;
	[SerializeField]
	private int createWaveNum = 7;
	[SerializeField]
	private float rotate = 15.0f;

	[SerializeField]
	private Transform targetPos;
	
	// 衝撃波生成時に使うラムダ式
	private delegate void Del(float f);

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIOrge.Thunder;

		attackObject.GetComponent<YetiWave>().setAttack(GetComponent<Orge>().getAttackPoint());
		waveObject.GetComponent<YetiQuake>().setAttack(GetComponent<Orge>().getAttackPoint());

		// 裏モードの場合2倍に
		if (SaveData.Instance.isBackMode() == true || GetComponent<Orge>().isEnemyBackMode() == true)
		{
			createWaveNum *= 2;
		}

		if (targetPos == null) { Debug.LogError("Playerが見つかりません"); }
	}
	
	// Update is called once per frame
	void calcUpdate () {
		if (Orge.state.normalizedTime >= 1.0f) { exitFlag = true; return; }

		// プレイヤーの座標と自分の座標からプレイヤーへの方向ベクトル算出(ただしyは考えない)
		Vector3 myPos = gameObject.transform.position;
		Vector3 tarPos = targetPos.position;
		myPos.y = tarPos.y = 0.0f;
		Vector3 targetDirect = (tarPos - myPos).normalized;

		// 4元数でやってみる
		Quaternion oldRotate = transform.rotation;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);
	
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.Play("attackUnder");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

	// 判定類全部生成
	public void createAttackUnderDamages()
	{
		// 生成
		Vector3 pos = transform.position;
		pos.y = 0.0f;

		// 衝撃波も出す
		GameObject hoge = Instantiate(attackObject, pos, Quaternion.identity) as GameObject;
		hoge.transform.parent = GameObject.Find("Game Element").transform;

		// 方向指定して地割れを生成するラムダ式
		Del del = f =>
		{
			Quaternion qvec = transform.rotation * Quaternion.AngleAxis(f, new Vector3(0, 1, 0));
			GameObject tmp = Instantiate(waveObject, pos, qvec) as GameObject;
			tmp.transform.parent = GameObject.Find("Game Element").transform;
		};

		// 正の最大値を出してそこから一定間隔ごと引いて生成
		int quakeNum = createWaveNum;
		float num = (float)(quakeNum - (quakeNum - 1) / 2) * rotate;
		for (int i = 1; i <= quakeNum; ++i)
		{
			del(num - (float)i * rotate);
		}

	}

}
