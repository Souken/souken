﻿using UnityEngine;
using System.Collections;

// ジャンプしつつ振り下ろし
public class OrgeJump : OrgeAIBase {

	[SerializeField] private GameObject attackObject;
	[SerializeField] private Vector3	createPos;
	[SerializeField] private float		initJumpPower	= 0.1f;
	[SerializeField] private float		subJumpPower	= 0.006f;
	[SerializeField]
	private float movePower = 0.1f;

	[SerializeField, Tooltip("裏モード用")]
	private GameObject wildFire;

	private float jumpPower = 0.0f;
	enum JumpType { JT_Ground, JT_JUMP, JT_DOWN }
	private JumpType jumpType = JumpType.JT_Ground;

	GameObject target;

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIOrge.Wait;

		target = GameObject.Find("Player");

		attackObject.GetComponent<OrgeHeightDamage>().setAttack(GetComponent<Orge>().getAttackPoint());
		wildFire.GetComponent<OrgeWildFire>().setAttack(GetComponent<Orge>().getAttackPoint());
	}
	
	// Update is called once per frame
	void calcUpdate () {
		
		if (Orge.state.IsName("wait") == true)
		{
			exitFlag = true;
		}

		// プレイヤーの方向を向く
		transform.LookAt(target.transform);
		Vector3 direct = transform.forward;
		direct.y = 0.0f;
		transform.forward = direct;

		// ジャンプ処理
		if (jumpType == JumpType.JT_Ground)
		{
			if (transform.position.y != 0.0f) { transform.Translate(0.0f, -transform.position.y, 0.0f); }
			return;
		}

		// ジャンプ中
		if (jumpType == JumpType.JT_JUMP)
		{
			// 上がりきったら待機、徐々に上昇率を減らす
			if (jumpPower == 0.0f) return;
			jumpPower -= subJumpPower;
			if (jumpPower <= 0.0f)
			{
				jumpPower = 0.0f;
				return;
			}
		}
		// 落下中
		else
		{
			jumpPower -= subJumpPower;
			// もし地面についたら
			if (transform.position.y < 0.0f)
			{
				jumpPower = 0.0f;
				Vector3 pos = transform.position;
				pos.y = 0.0f;
				transform.position.Set(pos.x, pos.y, pos.z);
				jumpType = JumpType.JT_Ground;

				// 鬼火の生成
				if (SaveData.Instance.isBackMode() == true || GetComponent<Orge>().isEnemyBackMode() == true)
				{
					GameObject tmp = Instantiate(wildFire, transform.position, Quaternion.identity) as GameObject;
					tmp.transform.parent = GameObject.Find("Game Element").transform;
				}
				
				return;
			}
		}
		// 移動
		transform.Translate(0.0f, jumpPower, movePower);
	
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.SetTrigger("jumpAttackTrigger");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

	// ジャンプ
	public void jump()
	{
		print("jump!!!!!!");
		jumpType = JumpType.JT_JUMP;
		jumpPower += initJumpPower;
	}

	// 落下開始
	public void jumpDown()
	{
		jumpType = JumpType.JT_DOWN;
	}

	// 攻撃
	public void createHeightAttack()
	{
		Vector3 pos = transform.position;
		pos.y = 0.0f;
		GameObject tmp = Instantiate(attackObject, pos, Quaternion.identity) as GameObject;
		tmp.transform.forward = transform.forward;
		tmp.transform.Translate(createPos);
	}

}
