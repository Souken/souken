﻿using UnityEngine;
using System.Collections;

public class OrgeWidthDamage : MonoBehaviour {

	[SerializeField]
	private int damage = 20;
	[SerializeField, Tooltip("プレイヤーを飛ばす力")]
	private float knokeForce = 1.0f;

	private bool hitFlag = false;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnTriggerEnter(Collider other)
	{
		if (hitFlag == true) return;

		if (other.gameObject.tag == "Player")
		{
			PlayerStatus player = other.gameObject.GetComponent<PlayerStatus>();
			if (player.isMoveStatus() == false) { return; }

			player.calcDamage(damage);
			player.calcKnocked(transform.position, knokeForce);
			hitFlag = true;

			print("正常に飛ばした");
		}
	}
}
