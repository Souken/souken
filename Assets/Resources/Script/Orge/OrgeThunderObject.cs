﻿using UnityEngine;
using System.Collections;

public class OrgeThunderObject : MonoBehaviour {

	[SerializeField]
	private float damagePercent = 0.8f;
	[SerializeField]
	private int damage = 20;
	[SerializeField]
	private float limitTime = 1.0f;
	
	private float nowTime = 0.0f;
	private bool hitFlag = false;
	public bool isDestroy { get; private set; }

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	// Use this for initialization
	void Awake()
	{
		transform.Translate(0.0f, transform.localScale.y / 2.0f, 0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		nowTime += Time.deltaTime;
		if (nowTime >= limitTime) 
		{ 
			isDestroy = true;
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (hitFlag == true) return;

		if (other.gameObject.tag == "Player")
		{
			PlayerStatus player = other.gameObject.GetComponent<PlayerStatus>();
			if (player.isMoveStatus() == false) { return; }

			player.calcDamage(damage);
			player.calcShrink();
			hitFlag = true;
		}
	}
}
