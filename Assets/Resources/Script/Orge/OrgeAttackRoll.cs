﻿using UnityEngine;
using System.Collections;

public class OrgeAttackRoll : OrgeAIBase {

	[SerializeField]
	private GameObject attackObject;
	[SerializeField]
	private AudioClip rollSE;

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIOrge.Thunder;
	}
	
	// Update is called once per frame
	void calcUpdate () {
		if (Orge.state.normalizedTime >= 1.0f) { exitFlag = true; return; }
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.Play("attackRoll");
		audio.PlayOneShot(rollSE);
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

	public void startAttackRoll()
	{
		GameObject tmp = Instantiate(attackObject, transform.position, Quaternion.identity) as GameObject;
		tmp.transform.Translate(0.0f, transform.localScale.y / 2.0f, 0.0f);
		tmp.transform.parent = transform;
	}

	public void endAttackRoll()
	{
		//Destroy(transform.FindChild("OrgeRollDamage(Clone)").gameObject);
	}
}
