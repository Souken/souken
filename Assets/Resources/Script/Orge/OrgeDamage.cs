﻿using UnityEngine;
using System.Collections;

public class OrgeDamage : OrgeAIBase {

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIOrge.Thunder;
	}
	
	// Update is called once per frame
	void calcUpdate () {
		if (Orge.state.normalizedTime >= 1.0f)
		{
			exitFlag = true;
		}
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.SetBool("isDamage", true);
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

	public void endDamage() { Orge.animator.SetBool("isDamage", false); }
}
