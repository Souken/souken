﻿using UnityEngine;
using System.Collections;

public class OrgeHeightDamage : MonoBehaviour {

	[SerializeField]
	private float damagePercent = 2.0f;
	[SerializeField]
	private int damage = 20;
	[SerializeField, Tooltip("持続時間(秒)")]
	private float duration = 1.0f;
	[SerializeField, Tooltip("プレイヤーを飛ばす力")]
	private float knokeForce = 1.0f;

	private float nowTime = 0.0f;
	private float decisionHeight;
	private bool hitFlag = false;

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update()
	{
		nowTime += Time.deltaTime;
		if (nowTime >= duration) Destroy(gameObject);
	}

	void OnTriggerEnter(Collider other)
	{
		if (hitFlag == true) return;
		print("衝撃波判定");

		if (other.gameObject.tag == "Player")
		{
			print("waveHit");

			PlayerStatus player = other.gameObject.GetComponent<PlayerStatus>();
			if (player.isMoveStatus() == false) { return; }

			player.calcDamage(damage);
			player.calcKnocked(transform.position, knokeForce);
			hitFlag = true;

			print("正常に飛ばした");
		}
	}
}
