﻿using UnityEngine;
using System.Collections;

public class OrgeWait : OrgeAIBase
{
	[SerializeField]
	private float waitTime = 2.0f;

	private float nowTime = 0.0f;

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIOrge.Run;
	}
	
	// Update is called once per frame
	void calcUpdate () {
		nowTime += Time.deltaTime;
		if (nowTime >= waitTime) { exitFlag = true; }
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.Play("wait");
		nowTime = 0.0f;
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

}
