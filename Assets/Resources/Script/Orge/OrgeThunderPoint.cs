﻿using UnityEngine;
using System.Collections;

public class OrgeThunderPoint : MonoBehaviour {

	[SerializeField]
	private GameObject thunder;
	[SerializeField, Tooltip("雷を打つまでの時間")]
	private float chargeTime = 1.0f;

	private float nowTime = 0.0f;
	private bool isShotThunder = false;

	private OrgeThunderObject shotTunder;

	public void setAttack(int attackPoint)
	{
		thunder.GetComponent<OrgeThunderObject>().setAttack(attackPoint);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isShotThunder == false)
		{
			nowTime += Time.deltaTime;
			if (nowTime >= chargeTime) 
			{ 
				GameObject tmp = Instantiate(thunder, transform.position, Quaternion.identity) as GameObject;
				//tmp.transform.parent = transform;
				tmp.transform.parent = GameObject.Find("Game Element").transform;
				shotTunder = tmp.GetComponent<OrgeThunderObject>();
				isShotThunder = true;
				Destroy(gameObject);
			}
		}
		else
		{
			if (shotTunder.isDestroy == true)
			{
				Destroy(gameObject);
			}
		}

	}
}
