﻿using UnityEngine;
using System.Collections;

using EnemyAIType;

[RequireComponent(typeof(OrgeRun))]

public class Orge : EnemyBase {

	private delegate bool AI();
	private AI aiUpdate;

	public static Animator animator { get; private set; }
	public static AnimatorStateInfo state { get; private set; }
	public static AnimatorStateInfo oldState { get; private set; }

	public static PlayerStatus player { get; private set; }	// AIスクリプトからでもすぐに使えるようにstatic化(但し外部から上書きされないようにprivate set)

	[SerializeField, Tooltip("現在のAIの状態")]
	private AIOrge aiStatus = AIOrge.Wait;

	private OrgeAIBase[] ai = new OrgeAIBase[(int)AIOrge.NUM];

	// Use this for initialization
	protected override void Awake()
	{
		base.Awake();

		animator = GetComponent<Animator>();
		state = animator.GetCurrentAnimatorStateInfo(0);

		player = GameObject.Find("Player").GetComponent<PlayerStatus>();

		ai[(int)AIOrge.Wait]		= GetComponent<OrgeWait>();
		ai[(int)AIOrge.Run]			= GetComponent<OrgeRun>();
		ai[(int)AIOrge.Jump]		= GetComponent<OrgeJump>();
		ai[(int)AIOrge.AttackWidth]	= GetComponent<OrgeAttackWitdh>();
		ai[(int)AIOrge.AttackUnder] = GetComponent<OrgeAttackUnder>();
		ai[(int)AIOrge.AttackRoll]	= GetComponent<OrgeAttackRoll>();
		ai[(int)AIOrge.Thunder]		= GetComponent<OrgeThunder>();
		ai[(int)AIOrge.Damage]		= GetComponent<OrgeDamage>();
		
		ai[(int)aiStatus].setInit();
		aiUpdate = ai[(int)aiStatus].update;
	}
	
	// Update is called once per frame
	void Update () {
		// フェードインしきっていないなら動かない
		if (FadeManager.Instance.isMaxFadeIn() == false) { return; }

		oldState = state;
		state = animator.GetCurrentAnimatorStateInfo(0);

		if (nowState == State.Dead)
		{
			if (GameManager.Instance.isPreClear() == false && GameManager.Instance.isClear() == false &&
				state.IsName("dead") == true && state.normalizedTime >= 1.0f)
			{
				GameManager.Instance.gamePreClear();	// ゲームクリア
			}
			else if (state.IsName("dead") == false)
			{
				calcDead();
			}
			return;
		}

		// trueが帰ってきたらそのフェーズは終了
		// elseの場合はまだ途中
		if (aiUpdate() == true)
		{
			aiStatus = ai[(int)aiStatus].nextAIType;
			calcChangeAI(); 
		}
	}

	// AI変更
	void calcChangeAI()
	{
		// 死亡してるか生きている以外の場合は正常に戻す
		if (nowState != State.Alive && nowState != State.Dead) { nowState = State.Alive; }

		// 現在のAIの中で決めてあるものを次のAIに
		ai[(int)aiStatus].setInit();
		aiUpdate = ai[(int)aiStatus].update;

		print(aiStatus);
	}

	public override void calcDamage(int damage)
	{
		base.calcDamage(damage);
		print("鬼ダメージ");
		// 死んだらモーション強制変更
		if (nowState == State.Dead) { animator.Play("dead"); return; }

		// 待機以外は怯みなし
		if (aiStatus == AIOrge.Wait)
		{
			aiStatus = AIOrge.Damage;
			calcChangeAI();
		}
	}

	// 死亡処理
	protected override void calcDead()
	{
		base.calcDead();
		if (GameManager.Instance.isClear() == true) { return; }
		//GameManager.Instance.gameClear();	// ゲームクリア
		//animator.Play("dead");
		print("ｵｳﾌ(死亡)");
	}

}
