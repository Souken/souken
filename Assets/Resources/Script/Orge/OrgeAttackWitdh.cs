﻿using UnityEngine;
using System.Collections;

public class OrgeAttackWitdh : OrgeAIBase {

	[SerializeField]
	private GameObject attackObject;
	[SerializeField]
	private Vector3 createPos = new Vector3(0.0f, 0.5f, 3.0f);

	// Use this for initialization
	void Start () {
		aiUpdate = calcUpdate;
		nextAIType = EnemyAIType.AIOrge.Thunder;
	}
	
	// Update is called once per frame
	void calcUpdate () {
		if (Orge.state.normalizedTime >= 1.0f) { exitFlag = true; }
	}

	public override void setInit()
	{
		base.setInit();
		Orge.animator.Play("attackWidth");
	}

	public override bool isExit()
	{
		if (base.isExit() == false) return false;
		return true;
	}

	// 攻撃
	public void createWidthAttack()
	{
		Vector3 pos = transform.position;
		pos.y = 0.0f;
		GameObject tmp = Instantiate(attackObject, pos, Quaternion.identity) as GameObject;
		tmp.transform.forward = transform.forward;
		tmp.transform.Translate(createPos);
		tmp.transform.parent = transform;
	}

	// 攻撃オブジェクト削除
	public void deleteWidthAttack()
	{
		Destroy(transform.FindChild("OrgeWidthDamage(Clone)").gameObject);
	}
}
