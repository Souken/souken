﻿using UnityEngine;
using System.Collections;

// 猫の手の判定用スクリプト
public class CatHand : MonoBehaviour {

	[SerializeField] private float	damagePercent = 5.0f;
	[SerializeField] private int	damage = 50;	// ダメージ
	public	bool attackFlag = false;			// 攻撃判定開始
	private bool hitFlag	= false;			// 攻撃判定があった時にON(多重判定防止用)

	public void setAttack(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (hitFlag == true && attackFlag == false) { hitFlag = false; }
	}

	// 判定
	void OnTriggerEnter(Collider other)
	{
		if (attackFlag == false || hitFlag == true || other.gameObject.tag != "Player") { return; }
		print("猫手ヒット");

		// ダメージ与えた後に吹っ飛ばす
		PlayerStatus player = other.gameObject.GetComponent<PlayerStatus>();
		player.calcDamage(damage);
		player.calcKnocked(gameObject.transform.position, 10.0f);
		hitFlag = true;
	}

}
