﻿using UnityEngine;
using System.Collections;

public class TitleProto : MonoBehaviour {

	// Use this for initialization
	void Start () {
        guiText.text = "攻撃で開始 : ジャンプorESCで終了";
	}

	void OnGUI()
	{
	}

	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Attack") || Input.GetKeyDown(KeyCode.P))
        {
            FadeManager.Instance.loadLevel("Stage1", 0.5f);
        }
        else if(Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

	}
}
