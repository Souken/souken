﻿using UnityEngine;
using System.Collections;

public class CreateEffect : MonoBehaviour
{
    public Vector3 GetPos() { return this.transform.position; }
    public Vector3 GetBottom()
    {
        // 自分の底辺
        Vector3 bottom = transform.position;
        bottom.y = transform.position.y - transform.localScale.y / 2;
        return bottom;
    }

    public void CreateJumpEffect()
    {
        // ジャンプエフェクトの生成
        GameObject jump = (GameObject)Resources.Load("Prefab/jumpEffect");
        jump.GetComponent<JumpEffect>().SetTarget(this.transform);
        Instantiate(jump, transform.position, transform.rotation);
    }

    public void CreateTutiEffect()
    {
        // 土エフェクトの生成
		GameObject tuti = (GameObject)Resources.Load("Prefab/tuti");
        Instantiate(tuti, GetBottom(), transform.rotation);
    }

    public void CreateMagicEffect()
    {
		GameObject magic = (GameObject)Resources.Load("Prefab/MagicCircle");
        Instantiate(magic, gameObject.transform.position, transform.rotation);
    }
}
