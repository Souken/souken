﻿using UnityEngine;
using System.Collections;

// 敵味方共通の抽象クラス
// 基本的にステータスと共通の処理を置いておく
public class CharacterBase : MonoBehaviour{

    // ステータス関連
    protected enum State
    {
        Alive, Invincible, Damage, Dead
    };
	[SerializeField]
    protected State nowState = State.Alive; // 現在の状態
	
    [SerializeField] protected int  MAX_HITPOINT = 500;
	[SerializeField] protected int	hitPoint;
	[SerializeField] protected int	attackPoint;
    protected bool                  deadFlag;

    public int getMaxHitPoint() { return MAX_HITPOINT; }
    public int getHitPoint()    { return hitPoint; }
	public int getAttackPoint() { return attackPoint; }

    // 生きているかどうか
    public bool isAlive() { return nowState != State.Dead ? true : false; }
	// 無敵か
	public void setInvincible() { nowState = State.Invincible; }
	public bool isInvincible() { return nowState == State.Invincible || nowState == State.Dead ? true : false; }

    protected virtual void Awake()
    {
		if (hitPoint == 0) hitPoint = MAX_HITPOINT;
    }

	// ダメージ処理(共通処理はこれ、この他に処理をする場合はoverrideさせて処理を書く)
    public virtual void calcDamage(int damage)
    {
        // 一応念のため
        if (isAlive() == false) { return; }
        hitPoint -= damage;
        if (hitPoint <= 0)
        {
            hitPoint = 0;
            nowState = State.Dead;
        }
    }

	// 死亡処理
	protected virtual void calcDead()
	{
		// キャラに応じた処理を派生先で書く
	}


}
