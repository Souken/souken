﻿using UnityEngine;
using System.Collections;

public class HitObject : MonoBehaviour {

    [SerializeField]
    private PlayerStatus status;

    private AudioClip damageBurnSE; // 炎ダメージ
    
	// Use this for initialization
	void Start () {
        damageBurnSE = (AudioClip)Resources.Load("se/damageBurn3");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {
        // hit.gameObjectで衝突したオブジェクト情報が得られる
        // ダメージを受けるオブジェクトの場合
        if (other.gameObject.tag == "DamageObject")
        {
            status.calcDamage(100);  // 仮ダメージにする
            Debug.LogError(status.getHitPoint().ToString());
            audio.PlayOneShot(damageBurnSE);
            Destroy(other.gameObject);
        }
    }

}
