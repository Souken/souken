﻿using UnityEngine;

// シングルトンクラス生成に使用する基底クラス
// シングルトンクラスを生成する場合には必ずこのクラスを継承して、以下のように書くこと
// public class クラス名 : SingletonMonoBehaviour<クラス名>
public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {

    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));  // typeof…型情報を返す
                if (instance == null) { Debug.LogError(typeof(T) + "is nothing"); }
            }
            return instance;
        }
    }
}
