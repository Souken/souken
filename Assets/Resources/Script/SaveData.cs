﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

// 起動時にデータを読み込み
// セーブなどの管理はここのスクリプトで
public class SaveData : SingletonMonoBehaviour<SaveData> {

	[SerializeField, Tooltip("裏モードが解放されているか")]
	public bool		isReleaseBackMode = false;
	public bool		isNowMode = false;
	[SerializeField]
	private int		attackBoostItemNum = 0;
	[SerializeField]
	private int		hitpointBoostItemNum = 0;
	public int		money = 50;

	public List<string> shiroSkill { get; private set; }
	public List<string> kuroSkill { get; private set; }

	[SerializeField, Tooltip("この値*闘神の勾玉の個数分が強化値として攻撃力に加算される")]
	private int baseAttackBoostNum = 10;
	[SerializeField, Tooltip("この値*護神の勾玉の個数分が強化値として最大体力に加算される")]
	private int baseHitpointBoostNum = 10;

	// 攻撃力の強化値を渡す
	public int getAddAttack() { return attackBoostItemNum * baseAttackBoostNum; }
	// 体力の強化値を渡す
	public int getAddHitpoint() { return hitpointBoostItemNum * baseHitpointBoostNum; }
	// 裏モードが解放されているか
	public bool getIsReleaseBackMode() { return isReleaseBackMode; }

	// 裏モードかどうか
	public bool isBackMode() { return isNowMode; }

	// スキル追加(0…シロ, 1…クロ, 2…2匹)
	public void addSkill(int num, string skillName)
	{
		if (num == 0 || num == 2) shiroSkill.Add(skillName);
		if (num == 1 || num == 2) kuroSkill.Add(skillName);
	}

	// 勾玉追加(true…攻撃, false…体力)
	public void addItem(bool isAttackItem)
	{
		if (isAttackItem == true)
		{
			++attackBoostItemNum;
		}
		else
		{
			++hitpointBoostItemNum;
		}
	}

	// 読み込みテスト
	bool isNewFile = false;

	private delegate string Del(string name);

	// 新規セーブデータ
	void createNewSaveData()
	{
		FileStream		f		= new FileStream(Application.dataPath + "/Resources/savedata.dat", FileMode.Create, FileAccess.Write);
		Encoding		utfEnc	= Encoding.GetEncoding("UTF-8");
		StreamWriter	writer	= new StreamWriter(f, utfEnc);
		// 1, 3行目は説明文
		writer.WriteLine(Cryption.encryption("裏モード解放,現在のモード,闘神,護神,所持金"));
		writer.WriteLine(Cryption.encryption(isReleaseBackMode.ToString() + "," + isNowMode.ToString() + "," +
			attackBoostItemNum.ToString() + "," + hitpointBoostItemNum.ToString() + "," + money.ToString()));
		writer.WriteLine(Cryption.encryption("所持スキル…4行目シロ5行目クロ"));

		writer.WriteLine(Cryption.encryption("Firedog,Water"));
		writer.WriteLine(Cryption.encryption("Raizen,BreakRock"));
		writer.Close();
	}

	// 新規データ生成時にショップの中身も初期化を掛けておく
	void createCommodityList()
	{
		// csvからnewGame用商品データを読み込む
		TextAsset		csv = (TextAsset)Resources.Load("Data/CommodityList");
		StringReader reader = new StringReader(csv.text);

		FileStream		f		= new FileStream(Application.dataPath + "/Resources/NowCommodityList.dat", FileMode.Create, FileAccess.Write);
		Encoding		utfEnc	= Encoding.GetEncoding("UTF-8");
		StreamWriter	writer	= new StreamWriter(f, utfEnc);

		// 末尾まで読み込み&書き込み
		while(reader.Peek() > -1) writer.WriteLine(Cryption.encryption(reader.ReadLine()));

		writer.Close();
		reader.Close();
	}

	// 新規装備スキルデータの生成
	void createPlayerSkillTable(string playerName)
	{
		print(playerName);
		string newSkillStr = playerName == "Shiro" ? "Firedog,Water" : "Raizen,BreakRock";

		FileStream		f		= new FileStream(Application.dataPath + "/Resources/SkillTable" + playerName + ".dat", FileMode.Create, FileAccess.Write);
		Encoding		utfEnc	= Encoding.GetEncoding("UTF-8");
		StreamWriter	writer	= new StreamWriter(f, utfEnc);

		writer.WriteLine(Cryption.encryption(newSkillStr));
		writer.Close();
	}

	// 全ての変数データをcsvにセーブする
	public void Save()
	{
		FileStream		f		= new FileStream(Application.dataPath + "/Resources/savedata.dat", FileMode.Create, FileAccess.Write);
		Encoding		utfEnc	= Encoding.GetEncoding("UTF-8");
		StreamWriter	writer	= new StreamWriter(f, utfEnc);
		// 1, 3行目は説明文
		writer.WriteLine(Cryption.encryption("裏モード解放,現在のモード,闘神,護神,所持金"));
		writer.WriteLine(Cryption.encryption(isReleaseBackMode.ToString() + "," + isNowMode.ToString() + "," +
			attackBoostItemNum.ToString() + "," + hitpointBoostItemNum.ToString() + "," + money.ToString()));
		writer.WriteLine(Cryption.encryption("所持スキル…4行目シロ5行目クロ"));

		// 所持スキル書き込み用ラムダ式
		Del del = name =>
		{
			string str = "";
			foreach (string skill in name == "shiro" ? shiroSkill : kuroSkill)
			{
				str += skill + ",";
			}
			str = str.Remove(str.Length - 1);	// 末尾についた余計なカンマを消す
			return str;
		};

		writer.WriteLine(Cryption.encryption(del("shiro")));
		writer.WriteLine(Cryption.encryption(del("kuro")));
		writer.Close();
	}

	// 基本情報の読み込み
	void readBaseData(ref StreamReader reader)
	{
		string line = reader.ReadLine();	// 2行目を使う
		// trueの場合ここで複合化を挟む
		line = Cryption.decryption(line);
		string[] value = line.Split(',');

		print(line);

		// 情報読み取り
		isReleaseBackMode		= bool.Parse(value[0]);
		isNowMode				= bool.Parse(value[1]);
		attackBoostItemNum		= int.Parse(value[2]);
		hitpointBoostItemNum	= int.Parse(value[3]);
		money					= int.Parse(value[4]);
	}

	// 所持スキルの読み込み
	List<string> readSkillData(ref StreamReader reader)
	{
		string line = reader.ReadLine();
		// trueの場合ここで複合化を挟む
		line = Cryption.decryption(line);

		print(line);

		string[] value	= line.Split(',');
		List<string> str = new List<string>();
		// 分割した文字列に空白が来るまでは格納する
		for (int i = 0; i < value.Length; ++i)
		{
			if (value[i] == "") return str;
			str.Add(value[i]);
		}
		return str;
	}

	// インスタンス生成と同時にデータがあるか確認
	// あったらそのまま読み込み、無かったら新しくデータ生成
	public void Awake()
	{
		// C#の機能を使う
		// ファイルが無かったら新規作成してから読み込み
		if (File.Exists(Application.dataPath + "/Resources/savedata.dat") == false)
		{
			isNewFile = true;
			createNewSaveData();
			createCommodityList();
			createPlayerSkillTable("Shiro");
			createPlayerSkillTable("Kuro");
		}

		StreamReader reader = new StreamReader(Application.dataPath + "/Resources/savedata.dat", Encoding.GetEncoding("UTF-8"));

		reader.ReadLine();			// 1行目はすっ飛ばす
		readBaseData(ref reader);	// 基本情報読み込み
		reader.ReadLine();			// 3行目も飛ばす
		shiroSkill = readSkillData(ref reader);	// シロの所持スキル読み込み
		kuroSkill = readSkillData(ref reader);	// クロの所持スキル読み込み

		reader.Close();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// デバッグ
	void OnGUI()
	{
		if (Application.loadedLevelName == "CSLogo")
		{
			GUI.TextArea(new Rect(5, 5, Screen.width, 50), Application.dataPath + "/Resources/savedata.dat" + (isNewFile == true ? "←new" : "読み込み"));
		}
	}
}
