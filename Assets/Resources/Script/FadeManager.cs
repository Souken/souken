﻿using UnityEngine;
using System.Collections;

// シーン遷移時のフェードイン・アウトを制御するためのクラス
// ネットに落ちてたものをコルーチン使わないでもうちょい使いやすく改善したもの
public class FadeManager : SingletonMonoBehaviour<FadeManager> {

	// 現在のフェード状態
	enum FadeState
	{
		FadeIn, Wait, FadeOut
	};
	private FadeState nowState = FadeState.FadeIn;

    [SerializeField]private Texture2D   blackTexture;
    private float       fadeAlpha   = 1.0f;
    private bool        isFading    = true;
	private float		addFadeNum	= -0.05f;

	private bool		loadSceneFlag = false;

	private string		nowSceneName = "Title";
	private string		loadSceneName="";

	public bool isWaiting() { return (nowState == FadeState.Wait) ? true : false; }
	public bool isMaxFadeIn() { return fadeAlpha <= 0.0f ? true : false; }	// 完全にフェードインしきったか
	public bool isMaxFadeOut() { return fadeAlpha >= 1.0f ? true : false; }	// 完全にフェードアウトしきったか

    public void Awake()
    {
        //if (this != Instance) { Destroy(this); return; }
        DontDestroyOnLoad(this.gameObject); // シーン切り替え時に破棄されずにそのまま引き継がれる
    }

    public void OnGUI()
    {
		if (loadSceneFlag == true)
		{
			loadSceneFlag = false;
			Application.LoadLevel(loadSceneName);
		}

        if (isFading == false) { return; }
        // 透明度を更新して黒テクスチャを描画
		fadeAlpha += addFadeNum;
		roundingFadeAlpha();
        GUI.color = new Color(0.0f, 0.0f, 0.0f, fadeAlpha);
        GUI.DrawTexture(new Rect(0.0f, 0.0f, (float)Screen.width, (float)Screen.height), blackTexture);

		// ここでシーン遷移
		if (nowState == FadeState.Wait && isMaxFadeOut() == true)
		{
			print(loadSceneName);
			nowSceneName = loadSceneName;

            Application.LoadLevel(loadSceneName);

			// ステージへの遷移ならロード画面を挟む
            //if (0 <= loadSceneName.IndexOf("Stage"))
            //{
            //    loadSceneFlag = true;
            //    Application.LoadLevel("Load");
            //}
            //else
            //{
            //    Application.LoadLevel(loadSceneName);
            //}
		}
    }
	
	// フェードイン開始
	public void startFadeIn(float intervalAddNum)
	{
		if (isFading == true) { return; }
		nowState	= FadeState.FadeIn;
		addFadeNum	= intervalAddNum * -1.0f;
		isFading	= true;
	}

    // シーン遷移(フェードアウト開始)
    public void loadLevel(string scene, float intervalAddNum)
    {
		if (isFading == true) { return; }
		nowState		= FadeState.FadeOut;
		loadSceneName	= scene;
		addFadeNum		= intervalAddNum;
		isFading		= true;
    }

	// 透過値の丸め込み
	void roundingFadeAlpha()
	{
		// フェードインしたら待機
		if (nowState == FadeState.FadeIn && isMaxFadeIn() == true)
		{
			fadeAlpha = 0.0f;
			nowState = FadeState.Wait;
			isFading = false;
		}
		// フェードアウトしきったらシーン遷移&フェードインの準備
		// (ただし次がロードシーンの場合は直ぐに明るくしておく)
		else if (nowState == FadeState.FadeOut && isMaxFadeOut() == true)
		{
			fadeAlpha = 1.0f;
			nowState = FadeState.Wait;
			isFading = false;
		}
	}
}
