﻿using UnityEngine;
using System.Collections;

public class DecalUVScroll : MonoBehaviour {

	[SerializeField, Tooltip("0の場合は、その方向はスクロールしない")]
	private Vector2 scrollSpeed = Vector2.zero;

	private Texture2D tex;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 vec = new Vector2(
			renderer.material.GetTextureOffset("_DecalTex").x + Time.deltaTime * scrollSpeed.x,
			renderer.material.GetTextureOffset("_DecalTex").y + Time.deltaTime * scrollSpeed.y);

		if (vec.x >= 10.0f) vec.x = 0.0f;
		if (vec.y >= 10.0f) vec.y = 0.0f;

		renderer.material.SetTextureOffset("_DecalTex", vec);
		
	}
}
