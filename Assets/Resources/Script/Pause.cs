﻿using UnityEngine;
using System.Collections;

public class Pause : Menu {

    bool hasDisplay;
    public bool IsDisplaying() { return hasDisplay; }
    public void TriggerMenu() { hasDisplay = true; }
    public void DisableMenu() { hasDisplay = false; }

    //ゲームがアクティブか監視
    [SerializeField] GameState gmState;

    //ポーズで「終了 or 撤退」を選んだときの移動先
    [SerializeField] string sceneToExit;

	// Use this for initialization
    void Start()
    {
        //音設定のコンポネントをOFF
        //Effective("SoundSettingSet");
    }
	
    // Update is called once per frame
    void Update()
    {
        //if (!hasDisplay)
        //    return;


        if (Input.GetButtonDown("Start"))
        {
            gmState.SwitchPauseMode();
        }

        if ( gmState.GameIsActive()  )
        {
            return;
        }

        selecter.DonatedUpdate();

        if (Input.GetButtonDown("Attack"))
        {
            Decide();
        }
    }

    void Decide()
    {
        switch (selecter.GetSelectNum())
        {
            case 0:
                //ポーズをOFF
                gmState.SwitchPauseMode();
                break;

            case 1:
                //音設定のコンポネントをON
                Effective("SoundSettingSet");
                //自分のコンポネントをOFF
                EffectiveItself(false);
                break;

            case 2:
                //シーンを変更
                //Application.Quit();
                ChangeScene();
                break;
        }

        selecter.Decide();
    }

    void ChangeScene()
    {
        gmState.ExitGame();
        FadeManager.Instance.loadLevel(sceneToExit, 1);
    }
}
