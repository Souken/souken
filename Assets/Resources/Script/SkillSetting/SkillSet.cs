﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SkillSet : MonoBehaviour{

    [SerializeField] Rect upperLeft;
    GUISetup gui;

    [SerializeField] protected int rowNum;
    [SerializeField] protected int colNum;
    int skillNum;

    [SerializeField] protected float widthInt;
    [SerializeField] protected float heightInt;

    //どちらのキャラかを判断
    public enum Character
    {
        Shiro,
        Kuro
    };

    [SerializeField] public Character chara;

    public DataMatrix<GameObject> iconMat;
    public DataMatrix<string> nameMat;

    bool Push(string str, int index)
    {
        //配列があふれていないかチェック
        if (colNum * rowNum < index - 1) 
        {
            Debug.LogError("Matrix is shorter than equipable skill!");
            return false;
        }

        //
        int seekRow = index % rowNum;
        int seekCol = index / rowNum;

        print("row :" + seekRow + " / col : " + seekCol );

        //何も設定されていなかったら追加
        if (nameMat.matrix[seekCol, seekRow] == null)
        {
            nameMat.matrix[seekCol, seekRow] = str;
            return true;
        }

        return false;
    }

    void Awake()
    {
        nameMat = new DataMatrix<string>(colNum, rowNum);
        iconMat = new DataMatrix<GameObject>(colNum, rowNum);

        //現在使用可能なスキル(今は全スキル)一覧からスキルを読み込む
        LoadSkillName();

        //名前を元にアイコンを読みこむ
        LoadSkillIcon();

        //スキルアイコンを整列する
        Alignment();    
    }

	// Use this for initialization
	void Start () 
    {

	}

    //キャラごとのスキル名を読み込む
    void LoadSkillName()
    {
		// セーブデータから読み込む
		List<string> skillData = chara == Character.Shiro ? SaveData.Instance.shiroSkill : SaveData.Instance.kuroSkill;
		for(int i = 0; i < skillData.Count; ++i)
		{
			Push(skillData[i], i);
		}
    }

    void LoadSkillIcon()
    {
        
        for (int seekCol = 0; seekCol < colNum; ++seekCol)
        {
            for (int seekRow = 0; seekRow < rowNum; ++seekRow)
            {
                iconMat.matrix[seekCol, seekRow]
                        = (GameObject)Instantiate((GameObject)Resources.Load("prefab/SkillSetting/SkillIcon"));

                //何もなければスルー
                if (nameMat.matrix[seekCol, seekRow] == null)
                    continue;
                
                //アイコンの画像を名前に合ったものに
                //iconMat.matrix[seekCol, seekRow].guiTexture.texture = (Texture)Resources.Load("UI/skillIcon/" + nameMat.matrix[seekCol, seekRow]);
				iconMat.matrix[seekCol, seekRow].guiTexture.texture = (Texture)Resources.Load("UI/Skill/" + nameMat.matrix[seekCol, seekRow]);

                print(seekCol.ToString() + " : "
                      + seekRow.ToString() + " : "
                      + nameMat.matrix[seekCol, seekRow]);

            }
        }

    }

    void Alignment()
    {

        gui = iconMat.matrix[0, 0].GetComponent<GUISetup>();

        for (int seekCol = 0; seekCol < colNum; ++seekCol)
        {
            for (int seekRow = 0; seekRow < rowNum; ++seekRow)
            {
                Rect temp = new Rect(
                                    (upperLeft.x + seekRow * widthInt) * gui.getRatio() / 100.0f,
                                    (upperLeft.y - seekCol * heightInt) * gui.getRatio() / 100.0f,
                                    iconMat.matrix[seekCol, seekRow].guiTexture.pixelInset.width,
                                    iconMat.matrix[seekCol, seekRow].guiTexture.pixelInset.height);

                iconMat.matrix[seekCol, seekRow].guiTexture.pixelInset = temp;
            }
        }    
    }

    public CursorPos Search(string elem)
    {
        CursorPos cursor = new CursorPos(0, 0);

        for (int seekCol = 0; seekCol < colNum; ++seekCol)
        {
            for (int seekRow = 0; seekRow < rowNum; ++seekRow)
            {
                if( elem == nameMat.matrix[seekCol, seekRow] )
                {
                    cursor.first = seekCol;
                    cursor.second = seekRow;
                    return cursor;
                }
            }
        }

        return null;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
