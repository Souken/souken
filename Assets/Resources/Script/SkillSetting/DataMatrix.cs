﻿using System.Collections;
using UnityEngine;

public class DataMatrix<T>
{

    int rowNum;
    int colNum;
    
    public int row
    {
        get { return rowNum; }
    }
    public int col
    {
        get { return colNum; }
    }


    public T [ , ] matrix;

    
	// 
    public DataMatrix(int col, int row)
    {
        SetSize(col, row);
    }

    //配列を用意。再度呼び出すと中身が消えるので注意
    public void SetSize(int col, int row)
    {
        rowNum = row;
        colNum = col;

        matrix = new T[colNum, rowNum];
    }

}
