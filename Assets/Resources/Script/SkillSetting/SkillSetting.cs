﻿using UnityEngine;
using System.Collections;

using System.Text;
using System.IO;
using System;

public class SkillSetting : Menu {

    [SerializeField] AudioPlayer sePlayer;

    //データ群
    SkillSet skills;
    
    //セレクタ関係
    [SerializeField]Selecter_Matrix skill1;
    [SerializeField]Selecter_Matrix skill2;
    Selecter_Matrix mdSelecter;
    DispSelecting_Depthic selecterDepth;

    //選択されたスキルに対応する画像を表示
    [SerializeField] GUITexture dispAction1;
    [SerializeField] GUITexture dispAction2;
    GUITexture mdGUI;

    string GetSelectedElem(Selecter_Matrix slt_mat
                           , DataMatrix<string> mat) 
    {
        return mat.matrix[slt_mat.selectPos.first,
                          slt_mat.selectPos.second]; 
    }

    public string GetSelectedElem(Selecter_Matrix slt_mat)
    {
        if (slt_mat == null)
            return "";

        if (slt_mat.selectPos == null)
            return "";

        if (skills == null)
            return "";

        return skills.nameMat.matrix[slt_mat.selectPos.first,
                                    slt_mat.selectPos.second];
    }

    //ごり押しスンマセン...
    //外部からこの関数でどちらのスキルを変更するか決定してから起動
    public void TriggerSelectMode(bool isSkill1)
    {
        if (isSkill1)
        {
            mdSelecter = skill1;
            mdGUI = dispAction1;
        }
        else
        {
            mdSelecter = skill2;
            mdGUI = dispAction2;
        }

        selecterDepth.Select = mdSelecter;
    }
    
	// Use this for initialization
	void Start () {

        selecterDepth = GetComponent<DispSelecting_Depthic>();
        skills = GetComponent<SkillSet>();

        skill1.matrix = skills.nameMat;
        skill2.matrix = skills.nameMat;

        LoadEquipedSkill();

        Selecter.MoveDirection temp = Selecter.MoveDirection.NONE;
        skill1.Move(temp);
        skill2.Move(temp);

        EffectiveItselfOnly(false);
    }
	
	// Update is called once per frame
	void Update () {

        mdSelecter.DonatedUpdate();
        AvoidDuplicate();

        if (Input.GetButtonDown("Attack"))
        {
            Decide();
        }
        else if (Input.GetButtonDown("Jump"))
        {
            Cancel();
        }         

	}

    void Decide()
    {        
        if (GetSelectedElem(mdSelecter, skills.nameMat) == null)
        {
            sePlayer.Play("jump_2");
            return;
        }

        skill1.Decide();
        skill2.Decide();

        Write();

        sePlayer.Play("slash3");

        //スキル設定のコンポネントをON
        EffectiveToFront(true);        
        //自分のコンポネントをOFF
        EffectiveItselfOnly(false);
    }

    void Cancel()
    {
        skill1.Cancel();
        skill2.Cancel();

        sePlayer.Play("cancel");


        //スキル設定のコンポネントをON
        EffectiveToFront(true);
        //自分のコンポネントをOFF
        EffectiveItselfOnly(false);
    }

    //スキル1とスキル2が同じだった場合、避ける
    void AvoidDuplicate()
    {
        // 動かしているセレクタと、もう一方のセレクタを変数で管理
        // → コード見やすく
        Selecter_Matrix anSelecter
            = (mdSelecter == skill1) ? skill2 : skill1;

        // 2 のところに 1 が来たら
        if ((mdSelecter.selectPos.first == anSelecter.selectPos.first)
            && (mdSelecter.selectPos.second == anSelecter.selectPos.second)
            )
        {
            // 1 のもともとあったところに 2 を持っていく
            anSelecter.selectPos = mdSelecter.bfPos;

            // 1 が戻ってきたことによって、 1 2 が同じになったら
            if ((mdSelecter.selectPos.first == anSelecter.selectPos.first)
                && (mdSelecter.selectPos.second == anSelecter.selectPos.second)
               )
            {
                // 2 は元々の位置に戻る
                anSelecter.selectPos = anSelecter.bfPos;
            }

            Selecter.MoveDirection temp = Selecter.MoveDirection.NONE;
            anSelecter.Move(temp);
        }

        // 2 のところから 1 がどいたら 
        else if (!((mdSelecter.selectPos.first == anSelecter.bfPos.first)
               && (mdSelecter.selectPos.second == anSelecter.bfPos.second))
               )
        {
            anSelecter.selectPos = anSelecter.bfPos;

            Selecter.MoveDirection temp = Selecter.MoveDirection.NONE;
            anSelecter.Move(temp);
        }
    }

    void Write()
    {
		FileStream f = new FileStream(Application.dataPath + "/Resources/SkillTable" + skills.chara + ".dat", FileMode.Create, FileAccess.Write);
		Encoding utfEnc = Encoding.GetEncoding("UTF-8");
		StreamWriter writer = new StreamWriter(f, utfEnc);

		string str = GetSelectedElem(skill1, skills.nameMat) + "," + GetSelectedElem(skill2, skills.nameMat);
		writer.WriteLine(Cryption.encryption(str));
		writer.Close();
    }

    void LoadEquipedSkill()
    {
		// 装備スキル(datファイル)を復号化してから読み込む
		StreamReader reader = new StreamReader(Application.dataPath + "/Resources/SkillTable" + skills.chara.ToString() + ".dat", Encoding.GetEncoding("UTF-8"));

        //列ごと読み込んで
        string line = reader.ReadLine();
		line = Cryption.decryption(line);
        string[] elem = line.Split(',');
       
        skill1.selectPos = skills.Search(elem[0]);
        skill2.selectPos = skills.Search(elem[1]);

        skill1.bfPos = skill1.selectPos;
        skill2.bfPos = skill2.selectPos;         
    }
}
