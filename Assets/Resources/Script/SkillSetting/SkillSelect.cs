﻿using UnityEngine;
using System.Collections;

public class SkillSelect : Menu {

    [SerializeField] AudioPlayer sePlayer;
    [SerializeField] GameObject skillMat;

    //どちらのキャラかを判断
    enum Chara
    {
        Shiro,
        Kuro
    };
    [SerializeField] Chara chara;

    [SerializeField] SkillSetting setting;
    [SerializeField] GUITexture sk1, sk2;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        selecter.DonatedUpdate();

        if (Input.GetButtonDown("Attack"))
        {
            Decide();
        }
        else if (Input.GetButtonDown("Jump"))
        {
            Cancel();
        }

	}

    void Decide()
    {
        //スキル設定のコンポネントをON
        EffectiveToNext(true);

        if (selecter.GetSelectNum() == 0)
        {
            setting.TriggerSelectMode(true);
        }
        else
        {
            setting.TriggerSelectMode(false);
        }

        sePlayer.Play("slash3");

        //自分のコンポネントをOFF
        EffectiveItselfOnly(false);       
    }

    void Cancel()
    {
        sePlayer.Play("cancel");

        //スキル設定のコンポネントをON
        EffectiveToFront(true);
        //自分のコンポネントをOFF
        EffectiveItselfOnly(false);
    }
}
