﻿using UnityEngine;
using System.Collections;

public class ActinonDisplay : MonoBehaviour {

    [SerializeField] SkillSet skills;
    [SerializeField] Selecter_Matrix selecter;

    public string GetSelectedElem(Selecter_Matrix slt_mat)
    {
        if (slt_mat == null)
            return "";

        if (slt_mat.selectPos == null)
            return "";

        if (skills == null)
            return "";

        return skills.nameMat.matrix[slt_mat.selectPos.first,
                                    slt_mat.selectPos.second];
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //gameObject.guiTexture.texture = (Texture)Resources.Load("UI/skillIcon/" + GetSelectedElem(selecter));
		gameObject.guiTexture.texture = (Texture)Resources.Load("UI/Skill/" + GetSelectedElem(selecter));

	}
}
