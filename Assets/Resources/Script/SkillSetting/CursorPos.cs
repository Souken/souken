﻿using UnityEngine;
using System.Collections;

public class CursorPos
{
    public int first;
    public int second;

    public CursorPos(int fst, int snd)    
    {
        first = fst;
        second = snd;
    }

    
    public static CursorPos operator+(CursorPos lhs, CursorPos rhs)
    {
        return new CursorPos(rhs.first + lhs.first,
                            rhs.second + lhs.second);
    }

    public static CursorPos operator -(CursorPos lhs, CursorPos rhs)
    {
        return new CursorPos(lhs.first - rhs.first,
                    lhs.second - rhs.second);
    }
    
    /*
    public static bool operator ==(CursorPos lhs, CursorPos rhs)
    {
        return ( (lhs != null && rhs != null)
                && (lhs.first == rhs.first && lhs.second == rhs.second)
                );
    }
    public static bool operator !=(CursorPos lhs, CursorPos rhs)
    {
        return ( (lhs == null && rhs == null)
                && 
                 ( (lhs.first != rhs.first) || lhs.second != rhs.second)
               );
    }
    */
}
