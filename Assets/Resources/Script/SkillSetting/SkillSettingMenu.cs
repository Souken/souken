﻿using UnityEngine;
using System.Collections;

public class SkillSettingMenu : Menu {

    [SerializeField] AudioPlayer sePlayer;

    [SerializeField] GameObject backIcon, unit1, unit2;
    Rect backIconRect, unitRect1, unitRect2;

//    [SerializeField] Rect backIconRect;

	// Use this for initialization
	void Start () {

        backIconRect = backIcon.GetComponent<GUITexture>().pixelInset;
        unitRect1 = unit1.GetComponent<GUITexture>().pixelInset;
        unitRect2 = unit2.GetComponent<GUITexture>().pixelInset;


        FadeManager.Instance.startFadeIn(0.05f);
	}
	
	// Update is called once per frame
	void Update () {

        int temp = selecter.GetSelectNum();
        selecter.DonatedUpdate();

        if (Input.GetButtonDown("Attack"))
        {
            Decide();
        }

        //カーソルが移動していたら
        //if(temp != selecter.GetSelectNum())
           ResizeCursorGUI();
	}

    void ResizeCursorGUI()
    {
        Rect temp = selecter.guiTexture.pixelInset;

        switch (selecter.GetSelectNum())
        {
            case 0:

                temp.width = backIconRect.width;
                temp.height = backIconRect.height;

                temp.y = backIconRect.top;
                temp.x = backIconRect.left;


                break;

            case 1:
                temp.width = unitRect1.width;
                temp.height = unitRect1.height;

                temp.y = unitRect1.top;
                temp.x = unitRect1.left;

                break;
            case 2:
                temp.width = unitRect2.width;
                temp.height = unitRect2.height;

                temp.y = unitRect2.top;
                temp.x = unitRect2.left;

                break;
        }

        selecter.guiTexture.pixelInset = temp;
    }

    void Decide()
    {
        switch (selecter.GetSelectNum())
        {
            case 0:
                print("(´･ω･｀)");
				FadeManager.Instance.loadLevel("StageSelect", 0.1f);
                //Application.Quit();
                break;

            case 1:
                //子のコンポネントをON
                EffectiveOnly("ShiroSkill", true);
                //自分のコンポネントをOFF
                EffectiveItselfOnly(false);
                break;

            case 2:
                //音設定のコンポネントをON
                EffectiveOnly("KuroSkill", true);
                //自分のコンポネントをOFF
                EffectiveItselfOnly(false);
                break;
        }

        selecter.Decide();
        sePlayer.Play("slash3");
    }

}
