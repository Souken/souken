﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

// 戦闘評価
public class BattleEvaluation : SingletonMonoBehaviour<BattleEvaluation> {

	enum ElementItem { EI_Time = 0, EI_Damage, EI_Special, EI_Total };	// 評価項目
	enum EvaluationIconType { EIT_Plum = 0, EIT_Bamboo, EIT_Pine };		// 梅, 竹, 松の順

	// 時間と損傷度を一括にした構造体
	struct TimeAndDamage
	{
		public float time { get; private set; }
		public int damage { get; private set; }
		public TimeAndDamage(string arg_time, string arg_damage)
		{
			time = float.Parse(arg_time);
			damage = int.Parse(arg_damage);
		}
	}

	public bool isGameEnd { get; private set; }

	private readonly int SKIP_LINE_NUM	= 4;
	private readonly int ELEMENT_NUM	= 3;

	[SerializeField, Tooltip("プレイヤー本体")]
	private PlayerStatus player;

	[SerializeField, Tooltip("項目テキスト")]
	private GUIText		elementsText;
	[SerializeField, Tooltip("評価テキスト")]
	private GUIText		evalutionText;
	[SerializeField, Tooltip("報酬金テキスト")]
	private GUIText		resultMoneyText;
	[SerializeField, Tooltip("項目の評価アイコン")]
	private GUITexture[] evaluationIcons;
	[SerializeField, Tooltip("総合評価アイコン")]
	private GUITexture	totalEvaluationIcons;
	[SerializeField, Tooltip("戦闘経過時間")]
	private float		nowTime = 0.0f;
	[SerializeField, Tooltip("評価GUIを表示するか")]
	private bool		isShowEvaluation = false;
	[SerializeField, Tooltip("特殊評価用フラグ(trueになっていれば満たされている)")]
	private bool		isSpecialEvaluation = false;

	private Dictionary<string, string> specialItemText = new Dictionary<string, string>();	// 特殊項目の文字列
	private Dictionary<EvaluationIconType, Texture2D> icons = new Dictionary<EvaluationIconType, Texture2D>();

	// 基準金額
	private int baseMoney = 0;
	// 報酬
	public int rewardMoney { get; private set; }

	// 評価レベルに応じた条件
	private Dictionary<EvaluationIconType, TimeAndDamage> evaluationData = new Dictionary<EvaluationIconType, TimeAndDamage>();
	// 最終評価
	private Dictionary<ElementItem, EvaluationIconType> finalAssessment = new Dictionary<ElementItem, EvaluationIconType>();

	// ラムダ式だと参照が使えないので関数で各評価ごとの基準読み込み
	void readDifficultyData(ref StringReader reader, EvaluationIconType type)
	{
		string line = reader.ReadLine();
		string[] values = line.Split(',');
		evaluationData.Add(type, new TimeAndDamage(values[0], values[1]));
	}

	// 評価シート読み込み
	void readEvaluationData(ref StringReader reader, bool isMode)
	{
		// 裏モードなら指定行飛ばす
		if(isMode == true)
		{
			for (int i = 0; i < SKIP_LINE_NUM; ++i) reader.ReadLine();
		}

		// 基礎報酬金額の取得
		string line = reader.ReadLine();
		string[] values = line.Split(',');
		baseMoney = int.Parse(values[0]);

		// 評価基準の読み込み
		readDifficultyData(ref reader, EvaluationIconType.EIT_Plum);
		readDifficultyData(ref reader, EvaluationIconType.EIT_Bamboo);
		readDifficultyData(ref reader, EvaluationIconType.EIT_Pine);
	}

	// 時間計測
	void calcTime()
	{
		// 戦闘終了まで加算する
		if (GameManager.Instance.isClear() == true) return;
		nowTime += Time.deltaTime;
	}

	// 各項目の評価(もう少しリファクタリングできた)
	void checkFinalAssessment()
	{
		// 時間
		EvaluationIconType type;
		if(nowTime <= evaluationData[EvaluationIconType.EIT_Pine].time)			type = EvaluationIconType.EIT_Pine;
		else if(nowTime < evaluationData[EvaluationIconType.EIT_Bamboo].time)	type = EvaluationIconType.EIT_Bamboo;
		else type = EvaluationIconType.EIT_Plum;
		finalAssessment.Add(ElementItem.EI_Time, type);

		// 損傷度
		int damage = player.getTotalDamage();
		if(damage <= evaluationData[EvaluationIconType.EIT_Pine].damage)		type = EvaluationIconType.EIT_Pine;
		else if(damage < evaluationData[EvaluationIconType.EIT_Bamboo].damage)	type = EvaluationIconType.EIT_Bamboo;
		else type = EvaluationIconType.EIT_Plum;
		finalAssessment.Add(ElementItem.EI_Damage, type);

		print(damage);
		print(type);
		print(evaluationData[EvaluationIconType.EIT_Pine].damage);
		print(evaluationData[EvaluationIconType.EIT_Bamboo].damage);
		print(evaluationData[EvaluationIconType.EIT_Plum].damage);

		// 特殊項目
		switch(Application.loadedLevelName)
		{
			case "Stage1":
				isSpecialEvaluation = PlayerStatus.isStartAttack; break;
			case "Stage2":
				isSpecialEvaluation = finalAssessment[ElementItem.EI_Time] == EvaluationIconType.EIT_Pine ? true : false; break;
			case "Stage3":
				isSpecialEvaluation = PlayerStatus.isNoDamage; break;
		}
		finalAssessment.Add(ElementItem.EI_Special, (isSpecialEvaluation == true) ? EvaluationIconType.EIT_Pine : EvaluationIconType.EIT_Plum);
	
		// 総合
		float totalNum = 0.0f;
		foreach(KeyValuePair<ElementItem, EvaluationIconType> tmp in finalAssessment)
		{
			totalNum += (float)tmp.Value;
		}
		totalNum /= 3.0f;
		totalNum = Mathf.Round(totalNum);	// 四捨五入
		finalAssessment.Add(ElementItem.EI_Total, (EvaluationIconType)totalNum);

		// 金額計算
		rewardMoney = (int)((float)baseMoney * (totalNum + 1.0f));
	}

	// 戦闘評価表示
	void drawBattleEvaluation()
	{
		// 評価計算
		checkFinalAssessment();

		// アイコン編集
		evaluationIcons[0].texture = icons[finalAssessment[ElementItem.EI_Time]];
		evaluationIcons[1].texture = icons[finalAssessment[ElementItem.EI_Damage]];
		evaluationIcons[2].texture = icons[finalAssessment[ElementItem.EI_Special]];
		totalEvaluationIcons.texture = icons[finalAssessment[ElementItem.EI_Total]];

		// テキスト編集
		elementsText.text = "時間\n損傷度\n" + specialItemText[Application.loadedLevelName];
		evalutionText.text = nowTime.ToString() + "\n" +
			player.getTotalDamage().ToString() + "\n" + (isSpecialEvaluation == true ? "達成" : "失敗");

		// 金額
		resultMoneyText.text = rewardMoney + "文";
	}

	public void Awake()
	{
		specialItemText.Add("Stage1", "先手必勝");
		specialItemText.Add("Stage2", "疾風怒濤");
		specialItemText.Add("Stage3", "金甌無欠");

		icons.Add(EvaluationIconType.EIT_Plum,		(Texture2D)Resources.Load("UI/Result/result_bai"));
		icons.Add(EvaluationIconType.EIT_Bamboo,	(Texture2D)Resources.Load("UI/Result/result_chiku"));
		icons.Add(EvaluationIconType.EIT_Pine,		(Texture2D)Resources.Load("UI/Result/result_shou"));

		// 現在のシーン名から評価データ取得
		TextAsset csv = (TextAsset)Resources.Load("Data/Difficulty/" + Application.loadedLevelName);
		StringReader reader = new StringReader(csv.text);

		reader.ReadLine();	// 1行目はいらない
		readEvaluationData(ref reader, SaveData.Instance.isNowMode);

		rewardMoney = 0;

		isGameEnd = false;
	}

	// Use this for initialization
	void Start () {
		// 子オブジェクトを隠しておく
		foreach(Transform child in transform) child.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(isShowEvaluation == false && GameManager.Instance.isClear() == true && PlayerStatus.animatorState.IsName("exitSit") == true)
		{
			print("クリア表示");

			// 子オブジェクトを表示
			foreach(Transform child in transform) child.gameObject.SetActive(true);
			isShowEvaluation = true;
			drawBattleEvaluation();
			// 所持金追加+セーブ
			SaveData.Instance.money += rewardMoney;
			SaveData.Instance.Save();
			isGameEnd = true;
		}
		
		calcTime();
	}
}
