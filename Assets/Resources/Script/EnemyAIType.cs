﻿// 敵のAIの列挙型をここで管理
// 敵の種類を増やす時はここにその敵の列挙型を追加
namespace EnemyAIType
{
	// 雪男AI
	public enum AIYeti { Wait, Tackle, Quake, Kick, Breath, Run, Damage, Fury, NUM };
	// 鬼AI
	public enum AIOrge { Wait, Run, Jump, AttackHeight, AttackWidth, AttackUnder, AttackRoll, Thunder, Damage, NUM };

}
