﻿using UnityEngine;
using System.Collections;

public class EffectParent : MonoBehaviour
{
	
	// Update is called once per frame
	void Update () {
        // 子オブジェクトがなければオブジェクト破棄
        if(this.transform.childCount == 0)
        {
            Destroy(this.gameObject);
        }
	}
}
