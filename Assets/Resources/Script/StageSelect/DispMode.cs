﻿using UnityEngine;
using System.Collections;

public class DispMode : MonoBehaviour {

    [SerializeField] Texture[] modeGUI;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (modeGUI[0] == null)
            return;

        SaveData data = GameObject.Find("SaveData").GetComponent<SaveData>();


        if (data.isNowMode)
            guiTexture.texture = modeGUI[0];
        else
            guiTexture.texture = modeGUI[1];
	}
}
