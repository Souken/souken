﻿using UnityEngine;
using System.Collections;

// ステージ選択の時に居る犬のドット絵スクリプト
// 基本的にカーソルと同じ位置に移動させる
public class StageSelectDog : MonoBehaviour {

	[SerializeField]
	private GUITexture cursor;
	[SerializeField, Tooltip("シロ…1, クロ…2")]
	private int isShiroOrKuro = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 pos;
		pos.x = cursor.pixelInset.x;
		pos.y = cursor.pixelInset.y;
		guiTexture.pixelInset = new Rect(pos.x + (guiTexture.pixelInset.width * isShiroOrKuro), pos.y, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
	}
}
