﻿using UnityEngine;
using System.Collections;

// 2D画像の連番アニメーション
public class TexAnimation : MonoBehaviour {

	[SerializeField]
	private OnStageSelect onStageSelect;
	[SerializeField, Tooltip("フォルダ名")]
	private string	folderName;
	[SerializeField, Tooltip("何種類あるか")]
	private int		typeNum = 4;
	[SerializeField, Range(0.0f, 1.0f), Tooltip("アニメーションの速度")]
	private float	animationTime = 0.1f;

	private enum NowMode { NM_RightWalk = 0, NM_RightSit, NM_LeftWalk, NM_LeftSit };
	[SerializeField, Tooltip("状態確認用")]
	private NowMode nowMode = NowMode.NM_RightSit;

	private Texture[][] textures;
	private float		nowTime = 0.0f;
	private int			nowNum = 0;

	// 連番画像読み込み
	void loadTextures(int arg_typeNum, int texNum, string fileName)
	{
		textures[arg_typeNum] = new Texture[texNum];
		print(texNum);
		for (int i = 0; i < texNum; ++i)
		{
			print(i);
			textures[arg_typeNum][i] = Resources.Load("Tex/" + folderName + "/" + folderName + fileName + (1+i).ToString()) as Texture;
			//if (textures[arg_typeNum][i] == null) print("Tex/" + folderName + "/" + folderName + fileName + (1 + i.ToString()));
		}
	}

	void Awake()
	{
		// 枚数分生成
		textures = new Texture[typeNum][];
		loadTextures(0, 10, "_runR");
		loadTextures(1, 8, "_sitR");
		loadTextures(2, 10, "_runL");
		loadTextures(3, 8, "_sitL");

		guiTexture.texture = textures[(int)nowMode][nowNum];
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// カーソルの状況に応じてテクスチャ切り替え
		checkChangeTex();
		
		nowTime += Time.deltaTime;
		if (nowTime >= animationTime)
		{
			nowTime = 0.0f;
			nowNum = nowNum == textures[(int)nowMode].Length - 1 ? 0 : nowNum + 1;
			guiTexture.texture = textures[(int)nowMode][nowNum];
		}
	}

	// カーソルの状態とアニメーションの状態に応じてテクスチャ変更
	void checkChangeTex()
	{
		// カーソルが移動中か
		if (onStageSelect.IsMoving() == true)
		{
			// 待機中のアニメーションだったら移動状態のアニメーションに
			if (nowMode == NowMode.NM_LeftWalk || nowMode == NowMode.NM_RightWalk) return;
			nowMode = onStageSelect.isCursorMoveDirect == true ? NowMode.NM_RightWalk : NowMode.NM_LeftWalk;
			nowTime = 0.0f;
			nowNum	= 0;
		}
		else
		{
			// 移動中のアニメーションだったら待機状態のアニメーションに
			if (nowMode == NowMode.NM_LeftSit || nowMode == NowMode.NM_RightSit) return;
			nowMode = nowMode == NowMode.NM_RightWalk ? NowMode.NM_RightSit : NowMode.NM_LeftSit;
			nowTime = 0.0f;
			nowNum = 0;
		}
	}

}
