﻿using UnityEngine;
using System.Collections;

public class OnStageSelect : MonoBehaviour {

    //ステージ数絡み
    [SerializeField] int maxStgNum = 1;
    [SerializeField] int minStgNum = 0;
    [SerializeField] int nowStgNum = 0;
    int bfStgNum = 0;


    // 12/13追加(鎌田)
    // Verticalにも対応できるようにフレーム追加
    [Tooltip("押しっぱなしの時にどのくらいのフレーム数間隔を空けるか決める値")]
    [SerializeField] int waitFrame = 10;
    int nowFrame = 0;


    //各ステージアイコン
    [SerializeField] iconData[] icons;

    //カーソル移動絡み
    [SerializeField] bool takeEasing;
    [SerializeField] bool isTranslating = false;
    [SerializeField] float delay;
    [SerializeField] float minSpeed;

    //メッセージ画像を表示する背景
    [SerializeField] GUITexture bgUI;

	// 移動しているかどうか
    public bool IsMoving() { return isTranslating; }

	// 3/22追加(鎌田)
	// ドット絵との連動のため、カーソルの進行方向を取得
	// false…左方向, true…右方向
	public bool isCursorMoveDirect { get; private set; }

	// Use this for initialization
	void Start () {

        //ステージ数が足りないときはクランプ
        if (icons.Length < (maxStgNum + Mathf.Abs(minStgNum) )) {
            Debug.LogError("StageNum is short in [OnStageSelect]");

            minStgNum = Mathf.Max(-icons.Length / 2, minStgNum);
            maxStgNum = Mathf.Min(icons.Length / 2, maxStgNum);
            
        }

        //初期位置も変更されたステージ数に合わせる
        nowStgNum = Mathf.Max(minStgNum, Mathf.Min(nowStgNum, maxStgNum) );
        TranslateCursor(nowStgNum);

        //0除算対策
        if (delay == 0)
            delay = 1;
        if (minSpeed < 0.1f)
            minSpeed = 0.1f;

        FadeManager.Instance.startFadeIn(0.05f);    

	}

    void Awake()
    {
//        FadeManager.Instance.startFadeIn(0.05f);    
    }

	// Update is called once per frame
	void Update () {

        TranslateCursor(nowStgNum);
        Disp();

        if (isTranslating) {
            return;
        }          

        Select();

        if (Input.GetButtonDown("Attack")){
            Decide();
        }
	}

    void Select()
    {
        //変更前のステージ
        bfStgNum = nowStgNum;

        if (Input.GetButtonDown("UP") || (nowFrame == 0 && Input.GetAxisRaw("Vertical") == 1.0f))
        {
            nowStgNum++;
        }
        else if (Input.GetButtonDown("DOWN") || (nowFrame == 0 && Input.GetAxisRaw("Vertical") == -1.0f))
        {
            nowStgNum--;
        }

        nowStgNum = Mathf.Max(minStgNum, Mathf.Min(nowStgNum, maxStgNum));

        //ステージ番号が変わったら移動状態に
        if (bfStgNum != nowStgNum) {
            isTranslating = true;
            PlaySoundWithAppoint("select_3");

			// 3/22追加(鎌田)
			// アイコンのX座標を取得、比較して方向を設定
			isCursorMoveDirect = icons[bfStgNum].GetIconPos().x < icons[nowStgNum].GetIconPos().x ? true : false;
        }


        // ここで押しっぱなし時の処理
        if (Input.GetAxisRaw("Vertical") == -1.0f || Input.GetAxisRaw("Vertical") == 1.0f)
        {
            ++nowFrame;
            if (nowFrame == waitFrame) { nowFrame = 0; }
        }
        else
        {
            nowFrame = 0;
        }

    }

    void Decide()
    {

        string objName = icons[nowStgNum].name;
 
        //今選択中のオブジェクトの名前に Stg という文字列があればステージへ進む
        if(objName.Contains("Stg")){

            // 超ゴリ押し
            // StgIco'n' x (x = ステージ数) なので、nで区切ると後ろの文字はステージ数となる
            string[] elem = objName.Split( 'n' );
            FadeManager.Instance.loadLevel("Stage" + (elem[1]), 1);

            PlaySoundWithAppoint("select_3");

            return;
        }

        if(objName.Contains("now"))
        {
            ChangePlayMode();
            return;
        }

        //ここまで抜けたらそれ以外のシーンへ飛ぶ操作
        ToOtherLocation(objName);
    }

    void ChangePlayMode()
    {
        SaveData data = GameObject.Find("SaveData").GetComponent<SaveData>();

        if (!data.getIsReleaseBackMode())
            return;

        data.isNowMode = !(data.isNowMode);
        PlaySoundWithAppoint("select_3");
    }

    void ToOtherLocation(string objName)
    {
        PlaySoundWithAppoint("select_3");

        if (objName.Contains("Skill"))
        {
            FadeManager.Instance.loadLevel("SkillSetting", 1);
            return;        
        }

        if (objName.Contains("shop"))
        {
            FadeManager.Instance.loadLevel("Shop", 1);
            return;
        }
    }







    void TranslateCursor(int num)
    {
        if (!gameObject.guiTexture) {
            Debug.LogError("Hasn't set GUI to [OnStageSelect.cs]");
            return;
        }

        Vector2 nextPos = icons[nowStgNum].GetIconPos();
        if(takeEasing)
            nextPos = Easing(nextPos);

        //最終座標移動
        gameObject.guiTexture.pixelInset = new Rect(nextPos.x, nextPos.y,
                                                   gameObject.guiTexture.pixelInset.width, gameObject.guiTexture.pixelInset.height);
    }

    Vector2 Easing(Vector2 sourcePos)
    {
        Vector2 nowPos = new Vector2(gameObject.guiTexture.pixelInset.left
                                   , gameObject.guiTexture.pixelInset.top);
        Vector2 vec = (sourcePos - nowPos);

        //最低速度以下になった最低速度に底上げ
        if (vec.magnitude < minSpeed)
        {
            vec *= (minSpeed / vec.magnitude);
            isTranslating = false;
        }

        sourcePos = nowPos + vec / delay;
        return sourcePos;
    }


    void Disp()
    {
        for ( int i=0; i < icons.Length; ++i )
        {
            //選択されていないアイコンのコンポネントをOFF
            if ( icons[i].enabled && (i != nowStgNum) )
            {
                Behaviour[] bhvs = icons[i].gameObject.GetComponents<Behaviour>();
                for (int j = 0; j < bhvs.Length; ++j)
                {
                    bhvs[j].enabled = false;
                }
            }
        }

        DispMessage();
        DispMessageBG();    
    }

    void DispMessage()
    {

        if (isTranslating)
        {
            icons[nowStgNum].VnshStgMessage();
            icons[bfStgNum].VnshStgMessage();
        }
        else
        {
            if (!icons[nowStgNum].isDisplay)
                return;

            icons[nowStgNum].DispStgMessage();
        }
    }

    void DispMessageBG()
    {

        if (isTranslating)
        {
            bgUI.enabled = false;
        }
        else
        {
            if (!icons[nowStgNum].isDisplay)
                return;

            bgUI.enabled = true;

            //メッセージ画像にメッセージウィンドウサイズを合わせる
            Vector2 messSize = icons[nowStgNum].GetGUISize();
            messSize.x += 120;
            messSize.y += 80;
            
            //選択位置の上にメッセージウィンドウが出るように調整
            Vector2 dispPos = icons[nowStgNum].GetMessagePos();
            bgUI.pixelInset = new Rect(dispPos.x - 75, dispPos.y - 40,
                                       messSize.x, messSize.y);
        }    
    }

    public void PlaySoundWithAppoint(string str)
    {

        AudioSource[] sources = gameObject.GetComponents<AudioSource>();

        str += " (UnityEngine.AudioClip)";
        int length = sources.Length;
        for (int i = 0; i < length; ++i)
        {
            if (str == sources[i].clip.ToString())
            {
                gameObject.audio.PlayOneShot(sources[i].clip);
                return;
            }
            print(str);
            print(sources[i].clip.ToString());
        }

        Debug.LogError("Your named [Clip] has not found!");
    }
}
