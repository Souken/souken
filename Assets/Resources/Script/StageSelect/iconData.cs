﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(GUITexture))]

//ステージアイコンのデータ格納用

public class iconData : MonoBehaviour {

    //アイコンの左上座標
    [SerializeField] Vector2 iconPos;
    [SerializeField] GUISetup selecterGUIParam;
    public Vector2 GetIconPos() { return iconPos; }
    public bool isDisplay = true;


    //メッセージ用画像の左上座標
    [SerializeField] Vector2 messagePos;
    public Vector2 GetMessagePos() { return messagePos; }

    //メッセージ用画像の大きさ
    //Vector2 guiSize;
    public Vector2 GetGUISize()
    {
        return new Vector2(stgMessage.pixelInset.width,
                             stgMessage.pixelInset.height);
    }


    //ステージの説明用画像
    [SerializeField] GUITexture stgMessage;
    public void DispStgMessage() { stgMessage.enabled = true; }
    public void VnshStgMessage() { stgMessage.enabled = false; }

    

	// Use this for initialization
	void Start () {
        //ステージの説明用画像が設定されていたら適用
        if (stgMessage != null)
        {
            //画面サイズの割合による誤差を適用
            messagePos.x *= (selecterGUIParam.getRatio() / 100.0f);
            messagePos.y *= (selecterGUIParam.getRatio() / 100.0f);

            //説明用画像の位置をアイコンの座標に合わせる
            stgMessage.pixelInset = new Rect(messagePos.x, messagePos.y,
                                             stgMessage.pixelInset.width,
                                             stgMessage.pixelInset.height);

            //説明画像はデフォルトで消しておく
            VnshStgMessage();
        }

        //セレクターと画面サイズの割合による誤差を適用
        if (selecterGUIParam != null)
        {
            iconPos.x *= (selecterGUIParam.getRatio() / 100.0f);
            iconPos.y *= (selecterGUIParam.getRatio() / 100.0f);
        }
	}

    void Awake()
    {

    }

	// Update is called once per frame
	void Update () {
	
	}
}
