﻿using UnityEngine;
using System.Collections;

// エフェクトの再生、停止のコントロール
public class EffectSwitch : MonoBehaviour {
    private ParticleSystem particle;
    private float rate;

	// Use this for initialization
	void Start () {
        particle = gameObject.GetComponent<ParticleSystem>();
        rate = particle.emissionRate;
        Stop();
	}
    
    // エフェクトの再生開始
    public void Play()
    {
        particle.emissionRate = rate;
    }

    // エフェクトの再生停止
    public void Stop()
    {
        particle.emissionRate = 0.0f;
    }
}
