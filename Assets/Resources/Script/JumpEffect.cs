﻿using UnityEngine;
using System.Collections;

public class JumpEffect : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    private ParticleSystem effect;
    private float lifeTime;
    private float stayTime;

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    // Use this for initialization
    void Start()
    {
        effect = gameObject.GetComponent<ParticleSystem>();
        if (effect != null)
        {
            effect.Stop();
            lifeTime = effect.startLifetime;
            stayTime = 0.0f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Playerが上昇中のみ追跡する
        if (stayTime == 0.0f && this.transform.position.y <= target.position.y)
        {
            this.transform.position = target.position;
            stayTime = 0.0f;
            effect.Play();
        }
        else
        {
            // 一度でも上昇が止まったら描画を待って消す
            stayTime += Time.deltaTime;
            if (stayTime > lifeTime)
            {
                effect.Stop();
                Destroy(this.gameObject);
            }
        }
    }
}
