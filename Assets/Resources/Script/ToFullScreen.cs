﻿using UnityEngine;
using System.Collections;

public class ToFullScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Rect temp = gameObject.guiTexture.pixelInset;

        temp.width = Display.main.systemWidth;
        temp.height = Display.main.systemHeight;
        temp.x = -temp.width / 2;
        temp.y = -temp.height / 2;

        gameObject.guiTexture.pixelInset = temp;
	}
	
	// Update is called once per frame
	void Update () {

	}
}
