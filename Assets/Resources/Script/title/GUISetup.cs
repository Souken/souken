﻿using UnityEngine;
using System.Collections;

public class GUISetup : MonoBehaviour {

    [SerializeField] float priority;

	// リサイズ用
	// 1280*720を基準とする
	private float baseWidth = 1280.0f;
	private float baseHeight = 720.0f;

	// 縦横補正用(倍率指定)
	private float corTall = 1.0f;
	private float corWide = 1.0f;

	private float imageW;
	private float imageH;
	private float imageX;
	private float imageY;

	private Rect drawRectSize;

	private float ratio = 1.0f;
	public float getRatio() { return ratio; }

	// Use this for initialization
	void Awake()
	{
		// 初期画像サイズ
		imageX = guiTexture.pixelInset.x;
		imageY = guiTexture.pixelInset.y;
		imageW = guiTexture.pixelInset.width;
		imageH = guiTexture.pixelInset.height;

		resizeImage();

        //深度の設定
        Vector3 tPos = gameObject.guiTexture.transform.position;
        tPos.z = priority;
        gameObject.guiTexture.transform.position = tPos;
	}

	void Start () {
		//Rect temp = gameObject.guiTexture.pixelInset;

		//temp.width = gameObject.guiTexture.texture.width;
		//temp.height = gameObject.guiTexture.texture.height;

		////temp.x += temp.width / 2;
		////temp.y += temp.height / 2;

		//gameObject.guiTexture.pixelInset = temp;

		////        Transform trans;
		//Vector3 tPos = gameObject.guiTexture.transform.position;
		//tPos.z = depth;
		//gameObject.guiTexture.transform.position = tPos;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// リサイズ
	void resizeImage()
	{
		// 現在のウィンドウサイズとアスペクト比を取得
		float scWidth = Screen.width;
		float scHeight = Screen.height;
		float winAspect = scWidth / scHeight;

		// 基準サイズとの比率を計算
		float wRatio = 100.0f / (baseWidth / scWidth);
		float hRatio = 100.0f / (baseHeight / scHeight);

		// 縦横時の判別
		if (scWidth < scHeight) { ratio = wRatio * corTall; }
		else { ratio = hRatio * corWide; }

		// リサイズサイズと表示位置
		int reimageeSizeW = (int)(imageW * (ratio / 100.0f));
		int reimageeSizeH = (int)(imageH * (ratio / 100.0f));
		int reimageeSizeX = (int)(imageX * (ratio / 100.0f));
		int reimageeSizeY = (int)(imageY * (ratio / 100.0f));

		guiTexture.pixelInset = new Rect(reimageeSizeX, reimageeSizeY, reimageeSizeW, reimageeSizeH);

	}

}
