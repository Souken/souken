﻿using UnityEngine;
using System.Collections;

public class Selecter_Liner : Selecter {

    //縦　か　横　かに一直線のメニュー
    public bool verticalMenu = true;

    public int headsNum;

    [SerializeField] public float slideScale = 96.0f;	// 1280*720の時の移動量
    [SerializeField] public float slideScaleX = 384.0f;	// 1280*720の時の移動量

    [SerializeField] int selectNum;
    public int GetSelectNum() { return selectNum; }

    Rect defPos;

	// Use this for initialization
	void Start()
	{
		slideScale *= (GetComponent<GUISetup>().getRatio() / 100.0f);
		slideScaleX *= (GetComponent<GUISetup>().getRatio() / 100.0f);

        defPos = gameObject.guiTexture.pixelInset;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void AffectCommand(MoveDirection moveDir)
    {
        //縦方向メニューは縦方向の操作のみ受け付け
        if (verticalMenu)
        {
            switch (moveDir)
            {
                case MoveDirection.UP:
                    selectNum--;
                    break;

                case MoveDirection.DOWN:
                    selectNum++;
                    break;

                default: break;
            }
        }

        //横方向メニューは(略)
        else
        {
            switch (moveDir)
            {
                case MoveDirection.LEFT:
                    selectNum--;
                    break;

                case MoveDirection.RIGHT:
                    selectNum++;
                    break;

                default: break;
            }
        }

    }

    public override void Move(MoveDirection moveDir)
    {
        Rect temp = defPos;
        temp.width = guiTexture.pixelInset.width;
        temp.height = guiTexture.pixelInset.height;


        if (verticalMenu)
        {
            temp.x = guiTexture.pixelInset.x;

            switch (moveDir)
            {
                case MoveDirection.UP:

                    PlaySoundWithAppoint("select_3");
                    break;

                case MoveDirection.DOWN:

                    PlaySoundWithAppoint("select_3");
                    break;

                default: break;
            }

            temp.y -= selectNum * slideScale;
        }

        else
        {
            temp.y = guiTexture.pixelInset.y;

            switch (moveDir)
            {
                case MoveDirection.LEFT:

                    PlaySoundWithAppoint("select_3");
                    break;

                case MoveDirection.RIGHT:

                    PlaySoundWithAppoint("select_3");
                    break;

                default: break;
            }

            temp.x += selectNum * slideScaleX;
        }

        gameObject.guiTexture.pixelInset = temp;                

    }

    public override void Clamp(MoveDirection moveDir)
    {
        selectNum = Mathf.Min( Mathf.Max(0, selectNum), headsNum - 1);
    }

    public override void Wrap(MoveDirection moveDir)
    {

        switch (moveDir)
        {
            case MoveDirection.LEFT:
            case MoveDirection.UP:

                if (selectNum < 0)
                {
                    selectNum = headsNum - 1;
                }

                break;

            case MoveDirection.RIGHT:
            case MoveDirection.DOWN:

                if (headsNum - 1 < selectNum)
                {
                    selectNum = 0;
                }
                break;

            default: break;
        }
    }

    public override void Skip(MoveDirection moveDir)
    { 
        
    }

    // bool command → true : decide / false : cancel
    public void ToBranch(bool command)
    {
        if (command)
        {
            Rect temp = gameObject.guiTexture.pixelInset;
            temp.x += slideScaleX;
            gameObject.guiTexture.pixelInset = temp;
        }
        else
        {
            Rect temp = gameObject.guiTexture.pixelInset;
            temp.x -= slideScaleX;
            gameObject.guiTexture.pixelInset = temp;
        }
    }

}
