﻿using UnityEngine;
using System.Collections;

public class Selecter_Matrix : Selecter {

    [SerializeField] GameObject matrixObject;
    public DataMatrix<string> matrix;
 
    
    [SerializeField] public float slideScale = 96.0f;	// 1280*720の時の移動量
    [SerializeField] public float slideScaleX = 384.0f;	// 1280*720の時の移動量



    public CursorPos selectPos = new CursorPos(0, 0);
    public CursorPos bfPos = new CursorPos(0, 0);

    Rect defPos;

	// Use this for initialization
	void Start()
	{
        defPos = gameObject.guiTexture.pixelInset;

        slideScale *= (GetComponent<GUISetup>().getRatio() / 100.0f);
        slideScaleX *= (GetComponent<GUISetup>().getRatio() / 100.0f);

        bfPos = selectPos;
    }
	
	// Update is called once per frame
	void Update () {

        Rect temp = defPos;

        temp.x += selectPos.second * slideScaleX;
        temp.y -= selectPos.first * slideScale;

        gameObject.guiTexture.pixelInset = temp;                

	}

    public override void AffectCommand(MoveDirection moveDir)
    {
        CursorPos move = new CursorPos(0, 0);
        switch (moveDir)
        {
            case MoveDirection.UP:
                move.first--;
                break;

            case MoveDirection.DOWN:
                move.first++;                
                break;

            case MoveDirection.LEFT:
                move.second--;
                break;

            case MoveDirection.RIGHT:
                move.second++;
                break;


            default: break;
        }

        selectPos += move;

    }

    public override void Move(MoveDirection moveDir)
    {
        switch (moveDir)
        {
            case MoveDirection.NONE:
                return;

            default:
                PlaySoundWithAppoint("select_3");
                break;
        }
    }

    public override void Clamp(MoveDirection moveDir)
    {
        selectPos.first = Mathf.Min(Mathf.Max(selectPos.first, 0), matrix.row - 1);
        selectPos.second = Mathf.Min(Mathf.Max(selectPos.second, 0), matrix.col - 1);
    }

    public override void Wrap(MoveDirection moveDir)
    {
        switch (moveDir)
        {
            case MoveDirection.UP:

                if (selectPos.first < 0)
                {
                    selectPos.first = matrix.col - 1;
                }
                break;

            case MoveDirection.DOWN:

                if (matrix.col - 1 < selectPos.first)
                {
                    selectPos.first = 0;
                }
                break;


            case MoveDirection.LEFT:

                if (selectPos.second < 0)
                {
                    selectPos.second = matrix.row - 1;
                }
                break;

            case MoveDirection.RIGHT:

                if (matrix.row - 1 < selectPos.second)
                {
                    selectPos.second = 0;
                }
                break;

            default: break;
        }

    }

    public override void Skip(MoveDirection moveDir)
    { 
        
    }

    public override void Decide()
    {
        PlaySoundWithAppoint("slash3");

        bfPos = selectPos;
    }

    public override void Cancel()
    {
        PlaySoundWithAppoint("cancel");

        selectPos = bfPos;
    }

}