﻿using UnityEngine;
using System.Collections;

public abstract class Selecter : MonoBehaviour {

	public bool available = true;
    [SerializeField] bool wrapMode = true;

	// 12/13追加(鎌田)
	// Verticalにも対応できるようにフレーム追加
	[Tooltip("押しっぱなしの時にどのくらいのフレーム数間隔を空けるか決める値")]
	[SerializeField] int waitFrame = 10;
	int nowFrame = 0;

    public enum MoveDirection
    {
        UP, 
        DOWN, 
        LEFT,
        RIGHT,

        NONE
    };


	void Start()
	{
//		slideScaleY *= (GetComponent<GUISetup>().getRatio() / 100.0f);
//		slideScaleX *= (GetComponent<GUISetup>().getRatio() / 100.0f);
	}	

	void Update()
	{
		// 12/17追加(鎌田)
		// 念のためフェードマネージャーと連動
        /*
		if(available 
            && FadeManager.Instance.isMaxFadeIn() == true
            ){
			Select();
		}
         * */
	}

    // 別のスクリプトから毎フレーム呼び出される用のUpdate関数
    public void DonatedUpdate()
    {
        // 12/17追加(鎌田)
        // 念のためフェードマネージャーと連動
        if (available
            && FadeManager.Instance.isMaxFadeIn() == true )
        {
            Select();
        }    
    }

    public void Select()
    {
        //変数が1つのトランザクション内で複数あると大変なので
        //一時変数を加工するように回していく
        MoveDirection move_Dir = Command();
        CountTiltingPadTime();

        //入力に応じてセレクト位置を仮変更
        AffectCommand(move_Dir);

        //スキップやクランプなどの位置変更を適用
        Skip(move_Dir);
        RevisePos(move_Dir);

        //変更後の値を元にGUIを移動
        Move(move_Dir);
    }

    //選択位置修正
    public void RevisePos(MoveDirection moveDir)
    {
        if (wrapMode)
            Wrap(moveDir);
        else
            Clamp(moveDir);
    }


    public abstract void Move(MoveDirection moveDir);
    public abstract void AffectCommand(MoveDirection moveDir);
    public abstract void Clamp(MoveDirection moveDir);
    public abstract void Wrap(MoveDirection moveDir);



    public virtual void Skip(MoveDirection moveDir)
    { }

    public virtual MoveDirection Command()
    {
        if (nowFrame != 0)
            return MoveDirection.NONE;

        if (Input.GetButtonDown("UP") || Input.GetAxisRaw("Vertical") == 1.0f)
        {
            return MoveDirection.UP;
        }
        else if (Input.GetButtonDown("DOWN") || Input.GetAxisRaw("Vertical") == -1.0f)
        {
            return MoveDirection.DOWN;
        }

        if (Input.GetButtonDown("LEFT") || Input.GetAxisRaw("Horizontal") == -1.0f)
        {
            return MoveDirection.LEFT;
        }
        else if (Input.GetButtonDown("RIGHT") || Input.GetAxisRaw("Horizontal") == 1.0f)
        {
            return MoveDirection.RIGHT;
        }

        return MoveDirection.NONE;
    }

    //パッド入力のカウント(押しっぱなし)対応
    public void CountTiltingPadTime()
    {
        if (Input.GetAxisRaw("Vertical") == -1.0f
            || Input.GetAxisRaw("Vertical") == 1.0f

            || Input.GetAxisRaw("Horizontal") == -1.0f
            || Input.GetAxisRaw("Horizontal") == 1.0f

            || Input.GetButtonDown("UP")
            || Input.GetButtonDown("DOWN")
            || Input.GetButtonDown("LEFT")
            || Input.GetButtonDown("RIGHT")
            )
        {
            ++nowFrame;
            if (nowFrame == waitFrame) { nowFrame = 0; }
        }
        else
        {
            nowFrame = 0;
        }    
    }

    public virtual void Decide()
    {
        PlaySoundWithAppoint("slash3");
    }

    public virtual void Cancel()
    {
        PlaySoundWithAppoint("cancel");
    }

    public void PlaySoundWithAppoint(string str)
    {        
        AudioSource[] sources = gameObject.GetComponents<AudioSource>();

        str += " (UnityEngine.AudioClip)";
        int length = sources.Length;
        for (int i = 0; i < length; ++i)
        {
            if (str == sources[i].clip.ToString())
            {
                gameObject.audio.PlayOneShot(sources[i].clip);
                return;
            }
            print(str);
            print(sources[i].clip.ToString());
        }

        Debug.LogError("The [Clip] that you had named has not found!");
    }
}
