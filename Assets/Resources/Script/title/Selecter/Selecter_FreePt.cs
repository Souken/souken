﻿using UnityEngine;
using System.Collections;

public class Selecter_FreePt : Selecter{
    
    Vector2 cursorPos;

    //ステージ数絡み
    int headsNum;
    [SerializeField] int maxStgNum = 1;
    [SerializeField] int minStgNum = 0;
    
    [SerializeField] int selectNum;
    public int GetSelectNum() { return selectNum; }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    public override void AffectCommand(MoveDirection moveDir)
    {
        switch (moveDir)
        {
            case MoveDirection.UP:
                selectNum--;
                break;

            case MoveDirection.DOWN:
                selectNum++;
                break;

            default: break;
        }
    }

    public override void Move(MoveDirection moveDir)
    {
        Rect temp = gameObject.guiTexture.pixelInset;

        switch (moveDir)
        {
            case MoveDirection.UP:

                PlaySoundWithAppoint("select_3");
                break;

            case MoveDirection.DOWN:

                PlaySoundWithAppoint("select_3");
                break;

            default: break;
        }

        gameObject.guiTexture.pixelInset = temp;

    }

    public override void Clamp(MoveDirection moveDir)
    {
        selectNum = Mathf.Max(Mathf.Min(selectNum), headsNum);
    }

    public override void Wrap(MoveDirection moveDir)
    {
        
    }

    public override void Skip(MoveDirection moveDir)
    {

    }
}
