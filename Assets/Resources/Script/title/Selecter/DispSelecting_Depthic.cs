﻿using UnityEngine;
using System.Collections;

public class DispSelecting_Depthic : MonoBehaviour {

    [SerializeField] float max = 0.75f;
    [SerializeField] float min = 0.3f;
    [SerializeField] float speed = 0.01f;

    [SerializeField] Selecter selecter;

    public Selecter Select
    {
        get { return selecter; }

        set
        {
            selecter = value;

            if(selecter != null)
                defColor = selecter.guiTexture.color;            
        }
    }

    public bool incrementing = false;

    Color defColor;


	// Use this for initialization
	void Start () {

        speed = Mathf.Min( Mathf.Max(speed, 0.0f), 1.0f);

        if(selecter != null)
            defColor = selecter.guiTexture.color;
	}
	
	// Update is called once per frame
	void Update () {

        if (selecter == null)
            return;

        if (!selecter.available)
            return;

        ChangeColorDepth();
	}

    void ChangeColorDepth()
    {
        Color color = selecter.guiTexture.color;

        //目的の値まで増えたら、増加モードOFF
        if (max <= color.a)
        {
            incrementing = false;        
        }
        //目的の値まで減ったら、増加モードON
        else if (color.a <= min)
        {
            incrementing = true;
        }

        if (incrementing)
            color.a += speed;
        else
            color.a -= speed;

        selecter.guiTexture.color = color;
    }

    void OnDisable() 
    {
        if (selecter == null)
            return;

        selecter.guiTexture.color = defColor;       
    }
}
