﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

    //メニュー数
    protected int menuNum;

    //1メニュー画面毎にセレクタを1つ
    [SerializeField] protected Selecter_Liner selecter;

    //メニュー間の行き来に使う。
    //次のメニューオブジェクト、手前のメニューオブジェクト
    [SerializeField] protected GameObject front, next;

    //オブジェクトのコンポネント全てを有効・無効化する
    protected void EffectiveItself(bool flag)
    {
        
        Behaviour[] bhvs = this.GetComponentsInChildren<Behaviour>();

        if (!flag)
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = false;
            }


            AudioSource[] adSources = this.GetComponentsInChildren<AudioSource>();
            int adLength = adSources.Length;
            for (int j = 0; j < adLength; ++j)
            {
                adSources[j].enabled = true;
            }
        }
        else 
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                AudioSource tAudio = bhvs[i].GetComponent<AudioSource>();
                if (tAudio != null)
                    continue;

                bhvs[i].enabled = true;
            }
        }

    }

    protected void Effective(string str)
    {
        GameObject tgo = GameObject.Find(str);
        //if (tgo == null) {
        //    return;
        //}

        Behaviour[] bhvs = tgo.GetComponentsInChildren<Behaviour>();
        
        if (bhvs[0].enabled)
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = false;
            }
        }
        else
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = true;
            }
        }
    }

    protected void EffectiveOnly(string str, bool flag)
    {
        GameObject tgo = GameObject.Find(str);
        Behaviour[] bhvs = tgo.GetComponents<Behaviour>();

        if (!flag)
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = false;
            }
        }
        else
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = true;
            }
        }
    }

    protected void EffectiveToNext(bool flag)
    {
        Behaviour[] bhvs = next.GetComponents<Behaviour>();

        if (!flag)
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = false;
            }
        }
        else
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = true;
            }
        }
    }

    protected void EffectiveToFront(bool flag)
    {
        Behaviour[] bhvs = front.GetComponents<Behaviour>();

        if (!flag)
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = false;
            }
        }
        else
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = true;
            }
        }
    }

    protected void EffectiveItselfOnly(bool flag)
    {
        Behaviour[] bhvs = GetComponents<Behaviour>();

        if (!flag)
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = false;
            }
        }
        else
        {
            int length = bhvs.Length;
            for (int i = 0; i < length; ++i)
            {
                bhvs[i].enabled = true;
            }
        }
    }
}
