﻿using UnityEngine;
using System.Collections;

public class Unlithificate : MonoBehaviour
{

    [SerializeField]
    bool hasStarted;
    [SerializeField]
    bool hasUnlithificated;
    [SerializeField]
    float changeSpeed;

    public bool HasCompleted() { return hasUnlithificated; }
    public void StartUnlith() { hasStarted = true; }


    // Use this for initialization
    void Start()
    {
        if (1.0f < changeSpeed)
            changeSpeed = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasStarted)
            return;

        if (!hasUnlithificated)
        {
            DoUnlithification();
        }
    }

    void DoUnlithification()
    {
        Color matSColor = gameObject.renderer.materials[0].color;
        Color matEColor = gameObject.renderer.materials[1].color;

        //透明度を変えて
        matSColor.a -= changeSpeed;
        matEColor.a += changeSpeed;

        gameObject.renderer.materials[0].color = matSColor;
        gameObject.renderer.materials[1].color = matEColor;

        if (matSColor.a < 0.001f)
            hasUnlithificated = true;
    }

}
