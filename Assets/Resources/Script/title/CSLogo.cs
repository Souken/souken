﻿using UnityEngine;
using System.Collections;

// 初回起動時に表示するCreativeStaffのロゴ
public class CSLogo : MonoBehaviour {

	[SerializeField, Tooltip("フェードインの速度")]
	private float fadeInSpeed = 0.01f;
	[SerializeField, Tooltip("フェードアウトの速度")]
	private float fadeOutSpeed = 0.01f;
	[SerializeField, Tooltip("自動的に遷移するまでの時間")]
	private float moveSceneTime = 2.0f;
	[SerializeField]
	private bool isStopInput = false;

	private float nowTime = 0.0f;

	void Awake()
	{
		FadeManager.Instance.startFadeIn(fadeInSpeed);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isStopInput == true) return;

		nowTime += Time.deltaTime;

		// 決定ボタンが押されたか、一定時間経ったらフェードアウトしてタイトルへ
		if (Input.GetButtonDown("Attack") == true || nowTime >= moveSceneTime)
		{
			FadeManager.Instance.loadLevel("Title", fadeOutSpeed);
			isStopInput = true;
		}

	}
}
