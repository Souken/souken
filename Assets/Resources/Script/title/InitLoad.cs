﻿using UnityEngine;
using System.Collections;

public class InitLoad : MonoBehaviour {

	[SerializeField, Tooltip("カーソルを表示するかどうか")]
	bool isShorCursor = false;
	[SerializeField, Tooltip("次に飛ばすシーン名")]
	string nextScene = "CSLogo";

	// Use this for initialization
	void Start () {
		Screen.showCursor = isShorCursor;		// カーソルを隠しておく
	}
	
	// Update is called once per frame
	void Update () {


		Application.LoadLevel(nextScene);
	}
}
