﻿using UnityEngine;
using System.Collections;

public class TestSound : MonoBehaviour {

    int interval;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (interval == 0)
        {
            return;
        }
        else
        {
            interval++;
            if (interval == 60) {
                interval = 0;            
            }
        }
	}

    public void PlayTest()
    {
        if (interval == 0)
        {
            gameObject.audio.PlayOneShot(gameObject.audio.clip);
            interval++;
        }
    }
}
