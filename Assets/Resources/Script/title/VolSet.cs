﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class VolSet : MonoBehaviour {

    [SerializeField] bool isAlwaysReference;
    [SerializeField] bool isBGM;
    [SerializeField] AudioSource source = null;

	// Use this for initialization
	void Start () {
	
	}

    void Awake()
    {
        SetVolume();
    }

	// Update is called once per frame
	void Update () {
        if (isAlwaysReference) 
        {
            SetVolume();
        }
	}

    public void SetVolume()
    {
        GameObject bgmse = GameObject.Find("BGMSEParam");
        SoundParam sdSet = bgmse.GetComponent<SoundParam>();

        if (isBGM)
        {
            source.volume = sdSet.GetBgmVolume();
        }
        else
        {
            source.volume = sdSet.GetSeVolume();
        }
    }
}
