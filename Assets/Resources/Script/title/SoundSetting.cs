﻿using UnityEngine;
using System.Collections;

public class SoundSetting : Menu
{

    public enum SETTING_MENU
    {
        BGM = 0,
        SOUND = 1,

        RETURN = 2
    };

    [SerializeField]
    SETTING_MENU menu;

    [SerializeField]
    TestSound testSE = null;
    [SerializeField]
    seeker seSeeker = null;
    [SerializeField]
    seeker bgmSeeker = null;

    bool isSetting;

    [SerializeField]
    string relayName;
    public void SetRelayName(string arg_name) { relayName = arg_name; }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        GameObject bgmse = GameObject.Find("BGMSEParam");
        SoundParam sdSet = bgmse.GetComponent<SoundParam>();

        selecter.DonatedUpdate();

        //[SPACE] で セッティングが状態変化
        if (Input.GetButtonDown("Attack"))
        {
            Decide();
        }
        else if (Input.GetButtonDown("Jump"))
        {
            Cancel();
        }

        seSeeker.SetPosition(sdSet.GetSeVolume());
        bgmSeeker.SetPosition(sdSet.GetBgmVolume());

        Setting(sdSet);
    }

    void Cancel()
    {
        //Setting中なら、Settingのキャンセル
        if (isSetting)
        {
            selecter.available = true;
            isSetting = false;
            selecter.ToBranch(false);
        }
        //Setting中でなければ、シーンを戻す
        else
        {
            //Decide();

            //音設定のコンポネントをON
            Effective(relayName);
            //自分のコンポネントをOFF
            EffectiveItself(false);
        }
        selecter.Cancel();
    }

    void Decide()
    {
        selecter.Decide();

        if (isSetting)
        {
            selecter.available = true;
            isSetting = false;

            selecter.ToBranch(false);
        }
        else
        {
            //セレクターを固定して
            selecter.available = false;
            //セッティング状態
            isSetting = true;

            selecter.ToBranch(true);
        }

    }

    void Setting(SoundParam sdParam)
    {
        if (!isSetting)
            return;

        if (selecter.GetSelectNum() == 0)
        {
            sdParam.SetBgmVolume(Input.GetAxisRaw("Horizontal"));
        }
        else if (selecter.GetSelectNum() == 1)
        {
            sdParam.SetSeVolume(Input.GetAxisRaw("Horizontal"));
            testSE.PlayTest();
        }
        else if (selecter.GetSelectNum() == 2)
        {
            Decide();

            //音設定のコンポネントをON
            Effective(relayName);
            //自分のコンポネントをOFF
            EffectiveItself(false);
        }
    }
}
