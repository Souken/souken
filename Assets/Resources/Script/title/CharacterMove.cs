﻿using UnityEngine;
using System.Collections;

public class CharacterMove : MonoBehaviour
{

    //    [SerializeField] bool hasUnlithificated;
    //    [SerializeField] float changeSpeed;
    [SerializeField] Unlithificate unlith;


    bool hasJumped;
    [SerializeField] float jumpForceY;
    [SerializeField] float moveDelay;
    float moveSpeed;

    [SerializeField] bool hasFinished;
    public bool hasComplete() { return hasFinished; }
    public GameObject aim;

    //このフラグは2つのキャラクターで共有
    [SerializeField] bool hasStartMoving;
    public void StartMoving() { hasStartMoving = true; unlith.StartUnlith(); }

    //このフラグは2つのキャラクターで共有
    [SerializeField] GameObject charaObj;

    // 12/13(鎌田)：モーション制御追加
    Animator animator;

    bool isGround()
    {
        return Physics.Raycast(transform.position, new Vector3(0, -1, 0), 1.0f);
    }

    // Use this for initialization
    void Start()
    {
        // 自分の子にシロがいるのでそこからAnimatorを取得
        animator = gameObject.transform.FindChild("Shiro").GetComponent<Animator>();

        //if (1.0f < changeSpeed)
        //    changeSpeed = 1.0f;

        if (aim == null)
            Debug.LogError(" [Aim Object] isn't setting :  locate :: Character");

        if (moveDelay != 0.0f)
            moveSpeed = (aim.transform.position - gameObject.transform.position).magnitude / moveDelay;
    }

    // Update is called once per frame
    void Update()
    {

        if (hasFinished || !hasStartMoving)
            return;

        if (!unlith.HasCompleted())
        {
            //if (!hasUnlithificated) {
            //    Unlithificate();
            return;
        }

        if (!hasJumped)
        {
            Jump();
            return;
        }

        MoveToAim();
    }

    void Jump()
    {
        gameObject.rigidbody.AddForce(new Vector3(0, jumpForceY, 0));
        hasJumped = true;

        animator.SetTrigger("jumpTrigger");	// ジャンプモーションに移行
        CreateJumpEffect();
        CreateTutiEffect();

    }

    void MoveToAim()
    {
        //一応、線形移動で
        if (aim.transform.position.x < gameObject.transform.position.x - moveSpeed)
        {
            Vector3 temp = gameObject.transform.position;
            temp.x -= moveSpeed;
            gameObject.transform.position = temp;
        }
        else if (gameObject.transform.position.x + moveSpeed < aim.transform.position.x)
        {
            Vector3 temp = gameObject.transform.position;
            temp.x += moveSpeed;
            gameObject.transform.position = temp;
        }

        if (aim.transform.position.z < gameObject.transform.position.z - moveSpeed)
        {
            Vector3 temp = gameObject.transform.position;
            temp.z -= moveSpeed;
            gameObject.transform.position = temp;
        }
        else if (gameObject.transform.position.z + moveSpeed < aim.transform.position.z)
        {
            Vector3 temp = gameObject.transform.position;
            temp.z += moveSpeed;
            gameObject.transform.position = temp;
        }

        //目標地点に近づいて
        if (Mathf.Abs(aim.transform.position.x - gameObject.transform.position.x) <= moveSpeed
            && Mathf.Abs(aim.transform.position.z - gameObject.transform.position.z) <= moveSpeed
            && isGround())
        {
            //接地していたら終了

            animator.SetTrigger("waitTrigger");	// 着地
            hasFinished = true;
        }
    }

    /*
    void Unlithificate()
    {
        Color matSColor = charaObj.renderer.materials[0].color;
        Color matEColor = charaObj.renderer.materials[1].color;

        //透明度を変えて
        matSColor.a -= changeSpeed;
        matEColor.a += changeSpeed;

        charaObj.renderer.materials[0].color = matSColor;
        charaObj.renderer.materials[1].color = matEColor;

        if(matSColor.a < 0.001f)
            hasUnlithificated = true;
    }
    */

    public void CreateJumpEffect()
    {
        // ジャンプエフェクトの生成
        GameObject jump = (GameObject)Resources.Load("prefab/jumpEffect");
        //jump.GetComponent<JumpEffect>().SetTarget(this.transform);
        jump.GetComponent<JumpEffect>().SetTarget(this.transform);
        Instantiate(jump, transform.position, transform.rotation);
    }

    public void CreateTutiEffect()
    {
        // 土エフェクトの生成
        GameObject tuti = (GameObject)Resources.Load("prefab/tuti");
        Instantiate(tuti, this.transform.position, transform.rotation);
    }
}
