﻿using UnityEngine;
using System.Collections;

public class seeker : MonoBehaviour {


    [SerializeField] GUITexture seekbar;
    //シークバーの大きさ
    Rect seekbarScale;
    //シークバーの左端
    float barLeft;

	// Use this for initialization
	void Start () {
        seekbarScale = seekbar.pixelInset;
        barLeft = seekbarScale.left;
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void SetPosition(float param)
    {
        Rect temp = gameObject.guiTexture.pixelInset;
        // x = シークバーの左端 + (数値 * シークバーの横幅)
        temp.x = barLeft + (param * seekbarScale.width);
        gameObject.guiTexture.pixelInset = temp;        
    }
}
