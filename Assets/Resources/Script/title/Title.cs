﻿using UnityEngine;
using System.Collections;

public class Title : Menu {

    //[SerializeField] SoundSetting sd = null;
    [SerializeField] Sunset sunset = null;
    [SerializeField] CharacterMove charaL = null;
	[SerializeField] CharacterMove charaR = null;

    bool hasDecide;

	// Use this for initialization
	void Start () {
		FadeManager.Instance.startFadeIn(0.05f);

        //音設定のコンポネントをOFF
        Effective("SoundSettingSet");	
	}
	
	// Update is called once per frame
	void Update () {

        //print(Input.GetAxisRaw("Horizontal"));

		// 12/17追加(鎌田)
		// 念のためフェードマネージャーと連動
		if (hasDecide && FadeManager.Instance.isMaxFadeIn() == true)
        {
            ChangeScene();
        }
        else
        {
            selecter.DonatedUpdate();

            if (Input.GetButtonDown("Attack"))
            {
                Decide();
            }
        }

    }

    void Decide()
    {
        switch (selecter.GetSelectNum())
        {
            case 0:
                charaL.StartMoving();
				charaR.StartMoving();
                sunset.StartFading();

                hasDecide = true;
                break;
                
            case 1:
                //音設定のコンポネントをON
                Effective("SoundSettingSet");
                //自分のコンポネントをOFF
                EffectiveItself(false);
                break;

            case 2:
                print("(´･ω･｀)");
                Application.Quit();
                break;
        }

        selecter.Decide();
    }

    void ChangeScene()
    {
		if (sunset.hasGone() && charaL.hasComplete() && charaR.hasComplete())
		{
			FadeManager.Instance.loadLevel("StageSelect", 0.05f);
            //FadeManager.Instance.loadLevel("StageSelect", 0.05f);
			//Application.LoadLevel("Stage1");
        }
    }

}
