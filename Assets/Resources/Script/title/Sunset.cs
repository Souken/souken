﻿using UnityEngine;
using System.Collections;

public class Sunset : MonoBehaviour {

    [SerializeField] Color nightColor;
    [SerializeField] float changeDelay = 1.0f;

    [SerializeField] bool hasStartFading;
    public void StartFading() { hasStartFading = true; }

    bool hasGrownNight = false;
    public bool hasGone() { return hasGrownNight; }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (!hasStartFading || hasGrownNight)
            return;

        Color color = gameObject.light.color;

        color.r += (nightColor.r - color.r) / changeDelay;
        color.g += (nightColor.g - color.g) / changeDelay;
        color.b += (nightColor.b - color.b) / changeDelay;

        gameObject.light.color = color;

        //閾値が一定値以下になったら夜更け
        if ( Mathf.Abs(nightColor.r - color.r) < 0.01f )
             hasGrownNight = true;
	}
}
