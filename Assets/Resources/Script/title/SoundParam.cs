﻿using UnityEngine;
using System.Collections;

public class SoundParam : MonoBehaviour 
{
    public float changeDelay;

    [SerializeField] float seVolume;
    public float GetSeVolume() { return seVolume; }

    [SerializeField] float bgmVolume;
    public float GetBgmVolume() { return bgmVolume; }


	// Use this for initialization
	void Start () {
        if (changeDelay == 0) 
        {
            changeDelay = 100;
        }

        //Inspectorの入力値に対する丸め込み修正
        if (seVolume < 0.0)
        {
            seVolume = 0.0f;
        }
        else if (1.0f < seVolume)
        {
            seVolume = 1.0f;
        }

        if (bgmVolume< 0.0)
        {
            bgmVolume = 0.0f;
        }
        else if (1.0f < bgmVolume)
        {
            bgmVolume = 1.0f;
        }
	}	

	// Update is called once per frame
	void Update () {
	
	}


    public void SetSeVolume(float axisParam)
    {
        //変動速度に応じた変動量
        float delta = axisParam / changeDelay;

        seVolume += delta;
        
        //丸め込み修正
        if (seVolume < 0.0)
        {
            seVolume = 0.0f;
        }
        else if (1.0f < seVolume )
        {
            seVolume = 1.0f;
        }
    }

    public void SetBgmVolume(float axisParam)
    {
        //変動速度に応じた変動量
        float delta = axisParam / changeDelay;

        bgmVolume += delta;

        //丸め込み修正
        if (bgmVolume  < 0.0)
        {
            bgmVolume = 0.0f;
        }
        else if (1.0f < bgmVolume )
        {
            bgmVolume = 1.0f;
        }
    }
}
