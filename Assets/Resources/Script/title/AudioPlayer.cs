﻿using UnityEngine;
using System.Collections;

/* 
 * 効果音を単一に鳴らす汎用的なスクリプト。
 * 
 * どうやらUnityha音源が鳴り終わる前にスクリプトを無効にすると、
 * 音が正常に再生されない。
 * 
 * そこで別オブジェクトにこのスクリプトをアタッチして
 * 呼び出すことで再生する。
 * 同一音源を複数のオブジェクトにロードする問題も解消できる
 * 
*/

public class AudioPlayer : MonoBehaviour {

    [SerializeField] AudioClip[] clips;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Play(int sourceNum)
    {
        audio.clip = clips[sourceNum]; 
        audio.Play();
    }

    public void Play(string sourceName)
    {
        for (int i = 0; i < clips.Length; ++i)
        {
            if (clips[i].name == sourceName)
            {
                audio.clip = clips[i];
                break;
            }
        }
        audio.Play();
    }

    public void Stop()
    {
        audio.Stop();
    }
}
