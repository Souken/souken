﻿using UnityEngine;
using System.Collections;

public class DrawMenuMemo : MonoBehaviour {

	[SerializeField]
	private CommodityList commodityList;
	[SerializeField]
	private ShopCursor cursor;

	private string[] data;
	int preSelectNum = 0;

	// Use this for initialization
	void Start () {
		data = commodityList.getCommodityData(CommodityList.CommodityType.CT_Description);
		print(data);
		// スラッシュを改行に置き換える
		for (int i = 0; i < data.Length; ++i)
		{
			data[i] = data[i].Replace("/", "\n");
		}
		guiText.text = data[cursor.nowSelectNum];
		preSelectNum = cursor.nowSelectNum;
	}
	
	// Update is called once per frame
	void Update () {
		if (preSelectNum != cursor.nowSelectNum)
		{
			guiText.text = data[cursor.nowSelectNum];
			preSelectNum = cursor.nowSelectNum;
		}
	}
}
