﻿using UnityEngine;
using System.Collections;

public class GUITextSetUp : MonoBehaviour {

	// リサイズ用
	// 1280*720を基準とする
	private float baseWidth = 1280.0f;
	private float baseHeight = 720.0f;

	// 縦横補正用(倍率指定)
	private float corTall = 1.0f;
	private float corWide = 1.0f;

	private int fixFontSize;
	private float pixX;
	private float pixY;

	private float ratio = 1.0f;
	public float getRatio() { return ratio; }

	void Awake()
	{
		// 初期フォントサイズ
		fixFontSize = guiText.fontSize;
		pixX = guiText.pixelOffset.x;
		pixY = guiText.pixelOffset.y;
		resizeFont();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// リサイズ
	void resizeFont()
	{
		// 現在のウィンドウサイズとアスペクト比を取得
		float scWidth = Screen.width;
		float scHeight = Screen.height;
		float winAspect = scWidth / scHeight;

		// 基準サイズとの比率を計算
		float wRatio = 100.0f / (baseWidth / scWidth);
		float hRatio = 100.0f / (baseHeight / scHeight);

		// 縦横時の判別
		if (scWidth < scHeight) { ratio = wRatio * corTall; }
		else					{ ratio = hRatio * corWide; }

		// リサイズサイズと表示位置
		int reFontSize		= (int)(fixFontSize * (ratio / 100.0f));
		int rePixOffsetX	= (int)(pixX * (ratio / 100.0f));
		int rePixOffsetY	= (int)(pixY * (ratio / 100.0f));

		// フォントサイズ変更
		guiText.fontSize = reFontSize;
		guiText.pixelOffset.Set(rePixOffsetX, rePixOffsetY);
	}
}
