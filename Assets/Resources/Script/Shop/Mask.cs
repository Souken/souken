﻿using UnityEngine;
using System.Collections;

public class Mask : MonoBehaviour {
	// 左から、らっしゃい, まいど, また来いよ, 馬鹿野郎, 買わんのかい, もう売るもんがない
	enum VoiceName { VN_Come = 0, VN_Thx, VN_Bye, VN_Fool, VN_Nobuy, VN_SoldOut, VN_NUM };

	[SerializeField]
	private float moveSpeed = 1.0f;
	[SerializeField, Range(0.01f, 0.1f)]
	private float moveRange = 1.0f;
	[SerializeField]
	private Camera camera;
	[SerializeField]
	private ShopCursor cursor;
	[SerializeField, Tooltip("商品の個数データ")]
	private DrawCommodityElement commodityNum;
	[SerializeField, Tooltip("金額表示")]
	private LoadNowMoney loadNowMoney;
	[SerializeField, Tooltip("何かしら買ったかどうか")]
	private bool isBuy = false;
	[SerializeField, Tooltip("急かすボイスを発生させるまでの秒数")]
	private float hurryTime = 30.0f;
	[SerializeField]
	private AudioClip[] voice = new AudioClip[(int)VoiceName.VN_NUM];

	private CommodityList commodity;

	private float nowPower = 0.0f;
	private float nowTime = 0.0f;

	// Use this for initialization
	void Start () {
		// 常にカメラ目線
		gameObject.transform.LookAt(camera.transform);
		// 入店時にボイス発生
		audio.PlayOneShot(voice[(int)VoiceName.VN_Come]);

		commodity = GetComponent<CommodityList>();
	}
	
	// Update is called once per frame
	void Update () {
		// 常に上下に動く
		calcMove();
		calcHurry();
		
		// 決定キーが押されたら購入処理
		if (Input.GetButtonDown("Attack") == true) { calcBuy(); }

		// キャンセルキー(スペース)が押されたら抜ける
		if(Input.GetButtonDown("Jump") == true)
		{
			// 何も変われてないなら怒られて終わり
			if (isBuy == false) audio.PlayOneShot(voice[(int)VoiceName.VN_Nobuy]);
			// 買っていたら商品リストとデータのセーブを行う
			else
			{
				commodity.saveList();
				SaveData.Instance.Save();
				audio.PlayOneShot(voice[(int)VoiceName.VN_Bye]);
			}
			FadeManager.Instance.loadLevel("StageSelect", 0.01f);
		}

	}

	// 購入処理
	void calcBuy()
	{
		// 個数が0なら買えない
		if (commodity.getItemData(cursor.nowSelectNum, CommodityList.CommodityType.CT_Num) == "0")
		{
			audio.PlayOneShot(voice[(int)VoiceName.VN_SoldOut]); return;
		}
		// 金額が足りない時も買えない
		if (int.Parse(commodity.getItemData(cursor.nowSelectNum, CommodityList.CommodityType.CT_Price)) > SaveData.Instance.money)
		{
			audio.PlayOneShot(voice[(int)VoiceName.VN_Fool]); return;
		}

		// ここまで来たら購入処理
		audio.PlayOneShot(voice[(int)VoiceName.VN_Thx]);
		// 金額分引く
		SaveData.Instance.money -= int.Parse(commodity.getItemData(cursor.nowSelectNum, CommodityList.CommodityType.CT_Price));
		// 金額表示しなおす
		loadNowMoney.calcBuyUpdate(SaveData.Instance.money);
		// セーブデータの追加と個数の減少
		commodity.calcBuy(cursor.nowSelectNum);
		// 個数のテキスト更新
		commodityNum.updateText();

		isBuy = true;
	}

	// 上下運動
	void calcMove()
	{
		nowPower += moveSpeed;
		if (nowPower >= 360.0f) nowPower = 0.0f;

		Vector3 spdVec = Vector3.zero;
		spdVec.y = moveRange * Mathf.Sin(nowPower * (Mathf.PI / 180.0f));
		transform.transform.Translate(spdVec);
	}

	// 急かす
	void calcHurry()
	{
		// 一定時間何も買わなかったら急かす
		nowTime += Time.deltaTime;
		if (nowTime >= hurryTime)
		{
			nowTime = 0.0f;
			audio.PlayOneShot(voice[(int)VoiceName.VN_Fool]);
		}
	}

}
