﻿using UnityEngine;
using System.Collections;

// イントロ部分とループ部分に分ける
public class ShopBGM : MonoBehaviour {

	[SerializeField]
	private AudioSource intro;
	[SerializeField]
	private AudioSource loop;
	[SerializeField]
	bool isExitIntro = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isExitIntro == false)
		{
			if (intro.isPlaying == false)
			{
				isExitIntro = true;
				loop.Play();
			}
		}
	}
}
