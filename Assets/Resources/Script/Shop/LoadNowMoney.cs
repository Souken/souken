﻿using UnityEngine;
using System.Collections;
using System.IO;

// 所持金読み込み
public class LoadNowMoney : MonoBehaviour {

	SaveData saveData;

	// Use this for initialization
	void Start () {
		saveData = GameObject.Find("SaveData").GetComponent<SaveData>();
		guiText.text = "所持金…" + saveData.money.ToString() + "文";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void calcBuyUpdate(int money)
	{
		guiText.text = "所持金…" + money.ToString() + "文";
	}

}