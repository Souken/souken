﻿using UnityEngine;
using System.Collections;

public class ShopCursor : MonoBehaviour {

	[SerializeField, Tooltip("デフォルト時のカーソル移動量")]
	private float moveNum = 45.0f;
	[SerializeField, Tooltip("商品数")]
	private int commodityNum = 5;
	[SerializeField]
	private Vector2 defaultPixelInset;
	[SerializeField]
	private GUITexture cursorBack;

	public int nowSelectNum { get; private set; }

	int preSelectNum = 0;	// 前フレームでの選択されていた番号
	Vector2 nowPos = Vector2.zero;

	Color cursorBackColor;

	// 12/13追加(鎌田)
	// Verticalにも対応できるようにフレーム追加
	[Tooltip("押しっぱなしの時にどのくらいのフレーム数間隔を空けるか決める値")]
	[SerializeField]
	int waitFrame = 10;
	int nowFrame = 0;

	// 効果音一覧
	[SerializeField]
	private AudioClip selectSE;

	void Awake()
	{
		commodityNum = CommodityList.COMMODITY_NUM;
		nowSelectNum = 0;
		cursorBackColor = cursorBack.color;
	}

	// Use this for initialization
	void Start () {
		// 現在の解像度にあわせて移動量調整
		moveNum *= (GetComponent<GUISetup>().getRatio() / 100.0f);
		print(GetComponent<GUISetup>().getRatio() / 100.0f);
	}
	
	// Update is called once per frame
	void Update () {
		calcCursorBackAlpha();
		calcMove();
		playMove();
	}

	// 背景カーソルのアニメーション
	void calcCursorBackAlpha()
	{
		cursorBackColor.a = Mathf.PingPong(Time.time, 0.5f);
		cursorBack.color = cursorBackColor;
	}

	// カーソルの移動計算
	void calcMove()
	{
		preSelectNum = nowSelectNum;

		// 上下移動テスト
		if (Input.GetButtonDown("UP") || (nowFrame == 0 && Input.GetAxisRaw("Vertical") == 1.0f))
		{
			nowSelectNum = nowSelectNum == 0 ? commodityNum-1 : nowSelectNum - 1;	
		}
		else if (Input.GetButtonDown("DOWN") || (nowFrame == 0 && Input.GetAxisRaw("Vertical") == -1.0f))
		{
			nowSelectNum = nowSelectNum == commodityNum-1 ? 0 : nowSelectNum + 1;
		}

		// ここで押しっぱなし時の処理
		if (Input.GetAxisRaw("Vertical") == -1.0f || Input.GetAxisRaw("Vertical") == 1.0f)
		{
			++nowFrame;
			if (nowFrame == waitFrame) { nowFrame = 0; }
		}
		else
		{
			nowFrame = 0;
		}
	}

	// 実際に移動させる
	void playMove()
	{
		if (nowSelectNum != preSelectNum)
		{
			audio.PlayOneShot(selectSE);
			nowPos.y = -moveNum * (float)nowSelectNum;
			guiTexture.pixelInset = new Rect(nowPos.x, nowPos.y, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
			cursorBack.pixelInset = new Rect(nowPos.x, nowPos.y, cursorBack.pixelInset.width, cursorBack.pixelInset.height);
		}
	}

}
