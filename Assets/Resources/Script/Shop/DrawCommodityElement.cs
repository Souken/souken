﻿using UnityEngine;
using System.Collections;

// 商品の各要素の描画スクリプト
// データも持たせておく
public class DrawCommodityElement : MonoBehaviour {

	[SerializeField]
	private CommodityList commodityList;
	[SerializeField]
	private CommodityList.CommodityType commodityType;

	private string[] data;

	// Use this for initialization
	void Start () {
		guiText.text = "";
		// 金か個数の場合最後に文字を付け足す
		string tmp = "";
		if (commodityType == CommodityList.CommodityType.CT_Price) { tmp = "文"; }
		else if (commodityType == CommodityList.CommodityType.CT_Num) { tmp = "個"; }

		// データ取得
		data = commodityList.getCommodityData(commodityType);
		foreach (string str in data)
		{
			guiText.text += str + tmp + "\n";
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// 購入された時の更新処理
	public void updateText()
	{
		guiText.text = "";
		// 金か個数の場合最後に文字を付け足す
		string tmp = "";
		if (commodityType == CommodityList.CommodityType.CT_Price) { tmp = "文"; }
		else if (commodityType == CommodityList.CommodityType.CT_Num) { tmp = "個"; }

		// データ取得
		data = commodityList.getCommodityData(commodityType);
		foreach (string str in data)
		{
			guiText.text += str + tmp + "\n";
		}
	}

}
