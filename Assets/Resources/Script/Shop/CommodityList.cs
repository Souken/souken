﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System;

// 商品リストのデータを管理するクラス
public class CommodityList : MonoBehaviour {

	// 各要素の名前
	public enum CommodityType { CT_ItemType = 0, CT_Name, CT_Price, CT_Num, CT_Description };
	// アイテムの種類
	public enum ItemType { IT_ShiroSkill, IT_KuroSkill, IT_DoubleSkill, IT_AttackItem, IT_HitpointItem };

	// 商品数
	public static int COMMODITY_NUM = 5;

	private string[][] commodityDatas = new string[COMMODITY_NUM][];

	// 引数に応じた要素の配列を渡す
	public string[] getCommodityData(CommodityType type)
	{
		string[] str = new string[COMMODITY_NUM];
		for (int i = 0; i < COMMODITY_NUM; ++i) { str[i] = commodityDatas[i][(int)type]; }
		return str;
	}

	// 引数の商品の情報を渡す
	public string getItemData(int num, CommodityType type)
	{
		return commodityDatas[num][(int)type];
	}

	void Awake()
	{
		// datから現在残っている商品を読み込む
		StreamReader reader = new StreamReader(Application.dataPath + "/Resources/NowCommodityList.dat", Encoding.GetEncoding("UTF-8"));

		// 1行目はすっ飛ばす
		reader.ReadLine();
		int		lineNum = 0;
		// 末尾まで読み込む
		while (reader.Peek() > -1)
		{
			// カンマごとに要素わけ
			string line = reader.ReadLine();
			line = Cryption.decryption(line);
			string[] values = line.Split(',');
			// 1行ぶんのデータを要素分けしたものを入れる
			commodityDatas[lineNum] = line.Split(',');
			print(line);
			++lineNum;
		}
	}

	// Use this for initialization
	void Start () {
		// フェードイン開始
		FadeManager.Instance.startFadeIn(0.05f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// 商品リストのセーブ
	public void saveList()
	{
		FileStream		f		= new FileStream(Application.dataPath + "/Resources/NowCommodityList.dat", FileMode.Create, FileAccess.Write);
		Encoding		utfEnc	= Encoding.GetEncoding("UTF-8");
		StreamWriter	writer	= new StreamWriter(f, utfEnc);

		// 1行目は説明
		writer.WriteLine(Cryption.encryption("商品タイプ,商品名,値段,個数,説明"));

		// 商品の種類分回す
		for(int i = 0; i < COMMODITY_NUM; ++i)
		{
			// 要素ごとにカンマを入れる
			string str = "";
			foreach(string split in commodityDatas[i])
			{
				str += split + ",";
			}
			str = str.Remove(str.Length - 1);	// 末尾についた余計なカンマを消す
			print(str);
			writer.WriteLine(Cryption.encryption(str));
		}
		writer.Close();
	}

	// 購入処理
	public void calcBuy(int lineNum)
	{
		ItemType type = (ItemType)Enum.Parse(typeof(ItemType), commodityDatas[lineNum][0], false);
		// タイプによって処理を変える
		// 今回アイテムの種類がかなり少ないためごり押しで
		// 本当はアイテムクラス作ったほうが良かった
		switch(type)
		{
			case ItemType.IT_ShiroSkill:
				SaveData.Instance.addSkill(0, "Flower"); break;
			case ItemType.IT_KuroSkill:
				SaveData.Instance.addSkill(1, "Zetsu"); break;
			case ItemType.IT_DoubleSkill:
				SaveData.Instance.addSkill(2, "Sou"); break;
			case ItemType.IT_AttackItem:
				SaveData.Instance.addItem(true); break;
			case ItemType.IT_HitpointItem:
				SaveData.Instance.addItem(false); break;
		}
		// 個数減らす
		int num	 = int.Parse(commodityDatas[lineNum][(int)CommodityType.CT_Num]);
		--num;
		commodityDatas[lineNum][(int)CommodityType.CT_Num] = num.ToString();
	}

}
