﻿using UnityEngine;
using System.Collections;

public class PlayerIcon : MonoBehaviour {

	// リサイズ用
	// 1280*720を基準とする
	private float baseWidth = 1280.0f;
	private float baseHeight = 720.0f;

	// 縦横補正用(倍率指定)
	private float corTall = 1.0f;
	private float corWide = 1.0f;

	private float imageW;
	private float imageH;
	private float imageX;
	private float imageY;

	// テスト
	[SerializeField] private PlayerStatus status;
	[SerializeField] private Texture2D shiroImage;
	[SerializeField] private Texture2D kuroImage;
	[SerializeField] private Texture2D gaugeImage;

	bool shiroFlag;	// シロならtrue

	private Texture2D playerImage;
	private Rect drawRectSize;

	void OnGUI()
	{
		GUI.DrawTexture(drawRectSize, playerImage);
		GUI.DrawTexture(drawRectSize, gaugeImage);
	}

	// Use this for initialization
	void Start()
	{
		shiroFlag = status.isShiroType();
		playerImage = shiroFlag == true ? shiroImage : kuroImage;

		// 初期画像サイズ
		imageX = guiTexture.pixelInset.x;
		imageY = guiTexture.pixelInset.y;
		imageW = guiTexture.pixelInset.width;
		imageH = guiTexture.pixelInset.height;

		resizeImage();
	}

	// Update is called once per frame
	void Update()
	{
		// シロからクロへのチェンジ
		if(status.isShiroType() == false && shiroFlag == true)
		{
			shiroFlag = false;
			playerImage = kuroImage;
		}
		// クロからシロ
		if (status.isShiroType() == true && shiroFlag == false)
		{
			shiroFlag = true;
			playerImage = shiroImage;
		}
	}

	// リサイズ
	void resizeImage()
	{
		// 現在のウィンドウサイズとアスペクト比を取得
		float scWidth = Screen.width;
		float scHeight = Screen.height;
		float winAspect = scWidth / scHeight;

		// 基準サイズとの比率を計算
		float wRatio = 100.0f / (baseWidth / scWidth);
		float hRatio = 100.0f / (baseHeight / scHeight);

		// 縦横時の判別
		float ratio;
		if (scWidth < scHeight) { ratio = wRatio * corTall; }
		else { ratio = hRatio * corWide; }

		// リサイズサイズと表示位置
		int reimageeSizeW = (int)(imageW * (ratio / 100.0f));
		int reimageeSizeH = (int)(imageH * (ratio / 100.0f));
		int reimageeSizeX = (int)(imageX * (ratio / 100.0f));
		int reimageeSizeY = (int)(imageY * (ratio / 100.0f));

		drawRectSize = new Rect(reimageeSizeX, reimageeSizeY, reimageeSizeW, reimageeSizeH);
		guiTexture.pixelInset = drawRectSize;

		// GUI.DrawTexture用に修正
		drawRectSize = new Rect(reimageeSizeX, Screen.height - reimageeSizeH, reimageeSizeW, reimageeSizeH);
	}
}
