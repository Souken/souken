﻿using UnityEngine;
using System.Collections;

// スキルアイコン
public class SkillIcon : MonoBehaviour {

    [SerializeField] private Vector2 backLeftUpPos = new Vector2(1020, 470);

    // 面倒だから構造体でまとめた
    struct Icon
    {
        public Texture2D image;
        public Rect drawingArea;

        // 画像が読み込まれている時に使用可能
        public bool setDrawingArea(Vector2 leftUpPos)
        {
            if(image == null){ return false; }
            drawingArea.Set(leftUpPos.x, leftUpPos.y, image.width, image.height);
            return true;
        }
        public bool setDrawingArea(float left, float top)
        {
            if (image == null) { return false; }
            drawingArea.Set(left, top, image.width, image.height);
            return true;
        }

        // GUI描画
        public void draw()
        {
            GUI.DrawTexture(drawingArea, image);
        }
    };
    private Icon[] icons;

	// Use this for initialization
	void Start () {
        icons = new Icon[3];    // 背景+スキル2つ
        icons[0].image = (Texture2D)Resources.Load("ui/Skillback_0.5");
        icons[0].setDrawingArea(backLeftUpPos);

        // ここはcsvからキャラの情報引っ張ってきて設定できるようにしたい(今は直打ち)
        icons[1].image = (Texture2D)Resources.Load("ui/skill1_0.7");
        icons[1].setDrawingArea(backLeftUpPos.x + 140.0f, backLeftUpPos.y + 50.0f);
        icons[2].image = (Texture2D)Resources.Load("ui/skill2_0.7");
        icons[2].setDrawingArea(backLeftUpPos.x + 60.0f, backLeftUpPos.y + 135.0f);
    }

    void OnGUI()
    {
        // 改善版
        foreach(Icon i in icons){ i.draw(); }
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
}
