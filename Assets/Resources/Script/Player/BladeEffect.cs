﻿using UnityEngine;
using System.Collections;

public class BladeEffect : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particle1;
    [SerializeField]
    private ParticleSystem particle2;
    [SerializeField]
    private ParticleSystem particle3;
    [SerializeField]
    private ParticleSystem particle4;
    [SerializeField]
    private float rate = 30.0f;

    void Start()
    {
        Stop();
    }

    public void Play()
    {
        particle1.emissionRate = rate;
        particle2.emissionRate = rate;
        particle3.emissionRate = rate;
        particle4.emissionRate = rate;
    }

    public void Stop()
    {
        particle1.emissionRate = 0.0f;
        particle2.emissionRate = 0.0f;
        particle3.emissionRate = 0.0f;
        particle4.emissionRate = 0.0f;
    }

}
