﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// 剣の攻撃の時に多重判定を防ぐためのスクリプト
// 通常攻撃時に生成するオブジェクトに付ける
public class BladeScript : MonoBehaviour {

    private List<int>	hitEnemyList = new List<int>(); // 現在のモーション中にヒットした敵のIDを格納しておく配列
	private int			myAttackID;
	[SerializeField, Tooltip("攻撃倍率")]
	private float		attackPercent = 1.0f;
	[SerializeField]
	private int			attackNum;

	// ダメージエフェクト追加
	private GameObject shock;

	// ステータスから攻撃力を引っ張り、一発分の攻撃力を設定
	public void setParam(int attackPoint)
	{
		attackNum = (int)((float)attackPoint * attackPercent);
	}

	public void deleteMyObject()
	{
		Destroy(gameObject);
	}

	// Use this for initialization
	void Start () {
		shock = (GameObject)Resources.Load("Prefab/shock2");
        gameObject.layer = 9;
	}
	
	// Update is called once per frame
	void Update () {

	}

    // 攻撃判定
    void OnTriggerStay(Collider other)
    {
        // 攻撃以外もしくは敵以外の場合抜ける
        if (other.gameObject.tag != "enemy") { return; }
		if (other.GetComponent<EnemyBase>().isInvincible() == true) return;

        // 既に判定済みなら抜ける
        EnemyBase enemy = other.gameObject.GetComponent<EnemyBase>();
        foreach (int id in hitEnemyList) { if (id == enemy.getID()) { return; } }

        // ここまできたら判定する
        hitEnemyList.Add(enemy.getID());
        enemy.calcDamage(attackNum);   // ここはステータスからattackの値を引っ張ってくる
		Instantiate(shock, gameObject.transform.position, Quaternion.identity);	// エフェクト生成

        print(enemy.getHitPoint().ToString());  // デバッグ
    }

}
