﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

[RequireComponent(typeof(SkillFactory))]

// スキル発動用スクリプト
public class PlayerSkill : MonoBehaviour {

	// 二人分のスキルをそれぞれ用意する
	// 2次元配列に変更
	private SkillBase[][] skill = new SkillBase[2][];
	private Texture[][] skillIcon = new Texture[2][];

	private PlayerStatus status;

	[SerializeField, Tooltip("デバッグモード、datからではなくcsvから読み込む")]
	bool isDebugMode = false;

	public Texture getSkillIcon(int type, int num) { return skillIcon[type][num]; }

	// スキルとアイコンの設定
	void setSkillandIcon(ref SkillFactory factory, string skillName, int type, int num)
	{
		if (skillName == "") return;

		factory.setSkill(skillName, out skill[type][num]);
		skillIcon[type][num] = Resources.Load("UI/Skill/" + skillName) as Texture;
		skill[type][num].useSkillButtonName = "Skill" + (num+1).ToString();

		skill[type][num].setAttackPoint(status.getAttackPoint());

		// デバッグ
		if (skill[type][num] != null) { print("読み込み完了 : " + skill[type][num].getSkillName()); }
		if (skillIcon[type][num] == null) { Debug.LogError("UI/Skill/" + skillName); }
	}

	// スキルの読み込み
	void loadSkillData(ref SkillFactory factory, string filePath, int type)
	{
		// 装備スキル(datファイル)を復号化してから読み込む
		StreamReader reader = new StreamReader(Application.dataPath + filePath, Encoding.GetEncoding("UTF-8"));

		string line = reader.ReadLine();
		if(isDebugMode == false) line = Cryption.decryption(line);
		string[] values = line.Split(',');
		for (int i = 0; i < 2; ++i) setSkillandIcon(ref factory, values[i], type, i);
	}

	void Awake()
	{
		status = GetComponent<PlayerStatus>();

		// シロとクロそれぞれにスキル枠を設ける
		skill[0] = new SkillBase[2];
		skill[1] = new SkillBase[2];
		skillIcon[0] = new Texture[2];
		skillIcon[1] = new Texture[2];

		SkillFactory factory = GetComponent<SkillFactory>();
		loadSkillData(ref factory, isDebugMode == false ? "/Resources/SkillTableShiro.dat" : "/Resources/Data/SkillTableShiro.csv", 0);
		loadSkillData(ref factory, isDebugMode == false ? "/Resources/SkillTableKuro.dat" : "/Resources/Data/SkillTableKuro.csv", 1);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// クリアしていたらとめる
		if (GameManager.Instance.isClear() == true) return;

		if (GetComponent<PlayerStatus>().isMoveStatus() == false || SkillBase.isSkillPlayingNow() == true) { return; }

		// スキルの発動処理をここに書く
		// animatorはいつもどおりstatusから引っ張る(予定)
		if (Input.GetButtonDown("Skill1") == true && skill[status.getPlayerType()][0].SkillStart(ref status) == true)
		{
			print("正常発動：" + skill[status.getPlayerType()][0].getSkillName());
		}
		else if (Input.GetButtonDown("Skill2") == true && skill[status.getPlayerType()][1].SkillStart(ref status) == true)
		{
			print("正常発動：" + skill[status.getPlayerType()][1].getSkillName());
		}

	}
}
