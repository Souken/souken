﻿using UnityEngine;
using System.Collections;

// 移動処理
public class PlayerMove : MonoBehaviour
{

    [SerializeField]
    private float rotatePower = 1.0f;
    [SerializeField]
    private float moveSpeed = 0.1f;
    [SerializeField]
    private float attackingMoveSpeed = 0.01f;
    [SerializeField]
    private float jumpPower = 0.2f;
    [SerializeField]
    private float gravityRevision = 0.001f;
    [SerializeField]
    private GameObject cameraObj;

    private int waitFrame = 0;
    private bool playRunSEFlag = false;

    //private GameObject	cameraObj;
    private Vector3 spdVec = Vector3.zero;
    private Vector3 boxSize;

    // 吹っ飛ばす方向
    [SerializeField]
    private Vector3 knockDirect = Vector3.zero;
    [SerializeField]
    private float addKnockForce = 0.0f;

    // エフェクト, SE関連
    private GameObject wind;	// 風
    private GameObject soil;	// 土(ただし雪ステージなら変える)
    private AudioClip jumpSE;
    private AudioClip runSE;

    // 地面と接地しているか
    public bool isGrounded()
    {
        // 鉛直下向きにオブジェクトがあるかどうかを判定、接触していたらtrueを返す
        //return Physics.Raycast(transform.position, new Vector3(0.0f, -1.0f, 0.0f), boxSize.y / 2.0f);

        //上むきに進んでいるなら、設置していない(はず)
        if (0.0f < spdVec.y)
        {
            return false;
        }

        return Physics.Raycast(transform.position, new Vector3(0.0f, -1.0f, 0.0f), Mathf.Abs(spdVec.y) + Mathf.Abs(Physics.gravity.y * gravityRevision));
    }

    // 自分の底辺
    Vector3 GetBottom()
    {
        Vector3 bottom = transform.position;
        bottom.y = transform.position.y - transform.localScale.y / 2;
        return bottom;
    }

	// カメラの座標を返す(外部で移動処理する時に使用)
	public Vector3 getCameraPos() { return cameraObj.transform.position; }

	// スキルなど外部の方で移動させる場合に使用する関数
	public void playMovePlayer(Vector3 arg_spdVec)
	{
		// 移動する位置の算出
		Vector3 nextPos = gameObject.transform.position + arg_spdVec;
		gameObject.rigidbody.MovePosition(nextPos);
	}

    // Use this for initialization
    void Start()
    {
        //カメラのオブジェクト情報を取得
        //cameraObj = GameObject.Find("Main Camera"); 
        boxSize = gameObject.GetComponent<BoxCollider>().size;

        wind = (GameObject)Resources.Load("Prefab/jumpEffect");
        wind.GetComponent<JumpEffect>().SetTarget(transform);
        soil = (GameObject)Resources.Load("Prefab/tuti");
        jumpSE = (AudioClip)Resources.Load("se/jump_2");
        runSE = (AudioClip)Resources.Load("se/dash2");

        audio.clip = runSE;
    }

    // Update is called once per frame
    void Update()
    {
        // フェードインしきっていないなら動かない
        if (FadeManager.Instance.isMaxFadeIn() == false) { return; }

        gameObject.rigidbody.WakeUp();

        // スキル使用中なら強制的に移動量0に
        if (SkillBase.isSkillPlayingNow() == true) { spdVec = Vector3.zero; return; }

        calcMoveVec();
        calcJump();

        // 移動する位置の算出
        Vector3 nextPos = gameObject.transform.position + spdVec;
        gameObject.rigidbody.MovePosition(nextPos);
    }

    void LateUpdate()
    {
        // 移動する位置の算出
        //Vector3 nextPos = gameObject.transform.position + spdVec;
        //gameObject.rigidbody.MovePosition(nextPos);

        PlayerStatus.animator.SetFloat("speed", spdVec.magnitude);
        // 走ってたら走行音鳴らす
        if (playRunSEFlag == false && PlayerStatus.animatorState.IsName("run") == true)
        {
            playRunSEFlag = true;
            audio.Play();
        }
        else if (playRunSEFlag == true && PlayerStatus.animatorState.IsName("run") == false)
        {
            playRunSEFlag = false;
            audio.Stop();
        }

        // 待機状態かどうか
        if (spdVec.magnitude == 0.0f && waitFrame < 180)
        {
            ++waitFrame;
        }
        else if (spdVec.magnitude != 0.0f)
        {
            waitFrame = 0;
        }
        PlayerStatus.animator.SetInteger("waitFrame", waitFrame);

        // スリープモードの確認
        //if (gameObject.rigidbody.IsSleeping() == true)
        //{
        //	print("(´=ω=｀)");
        //}
        //else
        //{
        //	print("(｀・ω・´)");
        //}
    }

    // 毎フレーム余計な力を無くしておく(ダウン以外)
    void FixedUpdate()
    {
        //if (GetComponent<PlayerStatus>().damageDownFlag == false)
        //{
        gameObject.rigidbody.velocity = Vector3.zero;
        gameObject.rigidbody.angularVelocity = Vector3.zero;
        //}
    }

    // 移動ベクトル算出
    void calcMoveVec()
    {
		// クリアしていたらとめる
		if (GameManager.Instance.isClear() == true)
		{
			spdVec = Vector3.zero;
			return;
		}

        //カメラと自機の向きを抽出
        Vector3 temp = (gameObject.transform.position - cameraObj.transform.position);
        temp.y = 0.0f;
        temp.Normalize();
        temp.y = gameObject.transform.forward.y;

        // 基本移動ベクトル算出
        float tempSpdY = spdVec.y;
		// 攻撃中は減速させる
        float nowMoveSpeed = PlayerStatus.animator.GetBool("attack") == false ? moveSpeed : attackingMoveSpeed;

        // ダメージ状態、もしくは地上攻撃の時は落下のみ
        if (GetComponent<PlayerStatus>().isMoveStatus() == true)
        {
            spdVec = new Vector3(
            temp.z * nowMoveSpeed * Input.GetAxisRaw("Horizontal") + temp.x * nowMoveSpeed * Input.GetAxisRaw("Vertical"),
            tempSpdY,
             -temp.x * nowMoveSpeed * Input.GetAxisRaw("Horizontal") + temp.z * nowMoveSpeed * Input.GetAxisRaw("Vertical"));
        }
        else
        {
            // 吹っ飛ばしかひるみか
            if (PlayerStatus.animator.GetBool("damageDown") == true)
            {
                spdVec = knockDirect * addKnockForce;
                spdVec.y = tempSpdY;
            }
            else
            {
                spdVec = new Vector3(0.0f, tempSpdY, 0.0f);
            }
        }

        spdVec.Normalize();
        spdVec *= nowMoveSpeed;
        spdVec.y = tempSpdY;

        // 移動入力があったときのみ向きを変える
        // ダメージ受けてる時は無しにしてる
        if (Input.GetAxisRaw("Horizontal") != 0.0f || Input.GetAxisRaw("Vertical") != 0.0f)
        {
            gameObject.transform.forward = new Vector3(spdVec.x, 0.0f, spdVec.z);
        }
    }

    // ジャンプ計算
    void calcJump()
    {

        // 空中に浮いてたら重力加算してすぐ抜ける
        if (isGrounded() == false) { spdVec.y += Physics.gravity.y * gravityRevision; return; }

		// クリアしていたらとめる
		if (GameManager.Instance.isClear() == true)
		{
			return;
		}

        if (PlayerStatus.animator.GetBool("jumping") == true)
        {

            //ここから地面に接地する前フレームの処理---------------------
            //Rayの当り判定チェック
            //自機と衝突物とのむきに対して
            Vector3 stPt = transform.position;
            Ray ray = new Ray(stPt, Vector3.down);
            RaycastHit hit;

            //落下先までRayを飛ばして
            if (Physics.Raycast(ray, out hit, 10, 8 | 9))
            {
                //衝突点と自機のベクトルを得る
                Vector3 hitPoint = ray.GetPoint(hit.distance - boxSize.y / 2.0f);
                print("(*‘ω‘ *)");

                // 接地状態          
                spdVec.y = -(hit.distance) + 0.000001f;
                PlayerStatus.animator.SetBool("jumping", false);
                print(hit.distance);

                return;
            }
            //ここまで -----------------------------

        }

        // 地面についていたら落下ベクトルを無くす
        spdVec.y = 0.0f;
        PlayerStatus.animator.SetBool("jumping", false);

        // ジャンプ
        if (GetComponent<PlayerStatus>().isMoveStatus() == true && Input.GetButtonDown("Jump") == true)
        {
            PlayerStatus.animator.SetBool("jumping", true);
            spdVec.y += jumpPower;
            audio.PlayOneShot(jumpSE);
            Instantiate(wind, transform.position, transform.rotation);
            Instantiate(soil, GetBottom(), transform.rotation);
        }
    }

    // 吹っ飛び時の方向と力をセット
    public void setKnockDownStatus(Vector3 direct, float force)
    {
        knockDirect = direct;
        addKnockForce = force;
        PlayerStatus.animator.SetBool("damageDown", true);
    }

    // 吹っ飛ばし終了
    public void exitKnockDown()
    {
        knockDirect = Vector3.zero;
        addKnockForce = 0.0f;
        PlayerStatus.animator.SetBool("damageDown", false);
    }

}
