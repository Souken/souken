﻿using UnityEngine;
using System.Collections;

// 通常攻撃
// 今はクロの処理を書いておく
// シロとクロで種類が違うので基底クラス作って派生させる？
public class PlayerNormalAttack : MonoBehaviour {

	// 斬撃関連
	[SerializeField, Tooltip("回転以外の斬撃の生成距離")]
	private float attackDistance = 0.8f;
	[SerializeField]
	private BladeEffect		bladeEffect;
	private GameObject[]	bladeHitObject = new GameObject[2];	// 通常攻撃

	[SerializeField, Tooltip("空中攻撃の判定オブジェクト")]
	private GameObject skyRollAttackObject;

	private const int MAX_NORMAL_ATTACK_NUM = 3;    // 連続してできる通常攻撃の回数
	private int attackID = 0;

	private AudioClip ripperWindSE;
	private AudioClip swingSE;
	private AudioClip slashSE;
	
	// 魔法弾関連
	private GameObject normalMagic;

	// Use this for initialization
	void Start () {
		bladeHitObject[0] = (GameObject)Resources.Load("prefab/Blade/BladeHitObject_1");
		bladeHitObject[1] = (GameObject)Resources.Load("prefab/Blade/BladeHitObject_2");
		foreach (GameObject obj in bladeHitObject) { obj.GetComponent<BladeScript>().setParam(GetComponent<PlayerStatus>().getAttackPoint()); }

		ripperWindSE	=	(AudioClip)Resources.Load("se/ripperWind_5");
		swingSE			=	(AudioClip)Resources.Load("se/swing3");
		slashSE			=	(AudioClip)Resources.Load("se/slash2");

		// 通常弾取得
		normalMagic = (GameObject)Resources.Load("Prefab/NormalMagic");
		normalMagic.GetComponent<NormalMagic>().setAttackNum(GetComponent<PlayerStatus>().getAttackPoint());
	}
	
	// Update is called once per frame
	void Update () {
		// クリアしていたらとめる
		if (GameManager.Instance.isClear() == true) return;


		if (GetComponent<PlayerStatus>().isMoveStatus() == false || SkillBase.isSkillPlayingNow() == true) 
		{
			if (PlayerStatus.animator.GetBool("attack") == true) { exitAttack(); }
			return; 
		}

		if (PlayerStatus.animator.GetInteger("playerID") == 0)
		{
			calcShiro();
		}
		else
		{
			calcKuro();
		}
	}

	// シロの処理
	void calcShiro()
	{
		// 前方に魔法弾発射
		if (Input.GetButtonDown("Attack") == true && PlayerStatus.animator.GetBool("attack") == false)
		{
			PlayerStatus.animator.SetBool("attack", true);

			audio.PlayOneShot(swingSE);
			audio.PlayOneShot(slashSE);

			Vector3 pos = gameObject.transform.position;
			pos.y += 0.2f;
			GameObject tmp = Instantiate(normalMagic, pos, Quaternion.identity) as GameObject;
			tmp.transform.forward = transform.forward;
			tmp.transform.parent = GameObject.Find("Game Element").transform;
		}
		// 攻撃状態ではないなら
		else if (PlayerStatus.animatorState.IsTag("attack") != true)
		{
			// 前回の状態が攻撃で、攻撃が終了していたら
			if (PlayerStatus.oldAnimatorState.IsTag("attack") == true && PlayerStatus.animator.GetBool("attack") == true)
			{
				PlayerStatus.animator.SetBool("attack", false);
			}
		}
	}

	// クロの処理
	void calcKuro()
	{
		// 初回の通常攻撃
		if (PlayerStatus.animator.GetBool("attack") == false && Input.GetButtonDown("Attack") == true)
		{
			PlayerStatus.animator.SetBool("attack", true);
			createBladeHitObject();	
			return;
		}

		// 攻撃状態ではないなら
		if (PlayerStatus.animatorState.IsTag("attack") != true)
		{
			// 前回の状態が攻撃で、攻撃が終了していたら
			if (PlayerStatus.oldAnimatorState.IsTag("attack") == true && PlayerStatus.animator.GetBool("attack") == true)
			{
				exitAttack();
			}
		}
		// 攻撃状態なら
		else
		{
			// 地上攻撃専用
			// 最大連続攻撃回数までいっていなかったら攻撃可能
			if (PlayerStatus.animator.GetBool("jumping") == false && attackID < MAX_NORMAL_ATTACK_NUM && Input.GetButtonDown("Attack") == true)
			{
				createBladeHitObject();
			}
		}
	}


	// 斬撃攻撃の当たり判定オブジェクト生成
	void createBladeHitObject()
	{
		++attackID;
		PlayerStatus.animator.SetInteger("attackID", attackID);

		// 地上か空中か
		if (PlayerStatus.animator.GetBool("jumping") == true)
		{
			Vector3 pos = transform.position + new Vector3(0.0f, 0.5f, 0.2f);
			GameObject obj = Instantiate(skyRollAttackObject, pos, transform.rotation) as GameObject;
			obj.transform.parent = transform;

			// 面倒だから一気に再生
			audio.PlayOneShot(ripperWindSE);
			audio.PlayOneShot(swingSE);
			audio.PlayOneShot(slashSE);
			return;
		}

		// 攻撃オブジェクトの子を探して削除
		foreach (Transform child in transform)
		{
			if (child.tag == "attack") { child.GetComponent<BladeScript>().deleteMyObject(); }
		}

		// 攻撃オブジェクト生成
		// 最終攻撃は回転、それ以外は正面
		GameObject tmp;
		if (attackID == MAX_NORMAL_ATTACK_NUM)
		{
			tmp = Instantiate(bladeHitObject[1], gameObject.transform.position, Quaternion.identity) as GameObject;
		}
		else
		{
			Vector3 pos = gameObject.transform.position;
			pos += (attackDistance * gameObject.transform.forward);
			tmp = Instantiate(bladeHitObject[0], pos, Quaternion.identity) as GameObject;
		}
		tmp.transform.parent = gameObject.transform;	// 親子関係を結んでおく

		bladeEffect.Play();

		// 面倒だから一気に再生
		audio.PlayOneShot(ripperWindSE);
		audio.PlayOneShot(swingSE);
		audio.PlayOneShot(slashSE);
	}

	// 攻撃終了処理
	public void exitAttack()
	{
		// 攻撃オブジェクトの子を探して削除
		foreach (Transform child in transform)
		{
			if (child.tag == "attack") { child.GetComponent<BladeScript>().deleteMyObject(); }
		}

		attackID = 0;
		PlayerStatus.animator.SetInteger("attackID", attackID);
		PlayerStatus.animator.SetBool("attack", false);
		bladeEffect.Stop();
	}

}
