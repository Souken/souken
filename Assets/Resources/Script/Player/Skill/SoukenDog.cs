﻿using UnityEngine;
using System.Collections;

// 双狗乱舞で使用する犬モデルのスクリプト
public class SoukenDog : MonoBehaviour {

	[SerializeField, Tooltip("持続時間")]
	private float duration = 0.2f;
	[SerializeField, Tooltip("消滅時間")]
	private float deleteTime = 0.05f;

	[SerializeField, Tooltip("移動スピード")]
	private float speed = 0.01f;

	float nowTime = 0.0f;
	[SerializeField]
	bool isDelete = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(0.0f, 0.0f, speed);

		calcTime();
	}

	void calcTime()
	{
		nowTime += Time.deltaTime;
		if (isDelete == true)
		{
			if (nowTime >= deleteTime) Destroy(gameObject);
		}
		else
		{
			if (nowTime >= duration)
			{
				nowTime = 0.0f;
				isDelete = true;
			}
		}
	}
}
