﻿using UnityEngine;
using System.Collections;

// 雷蝉
// 通常攻撃の判定を纏って突進する
public class SkillRaizen : SkillBase {

	private GameObject	hitObject;	// 通常回転攻撃の判定を流用
	private GameObject	thunderWind;// 雷のブラーエフェクト
	private PlayerMove	playerMove;
	private float moveSpeed = 0.2f;
	Vector3 spdVec;

	public override void setAttackPoint(int attackPoint)
	{
		base.setAttackPoint(attackPoint);
		hitObject.GetComponent<BladeScript>().setParam(attackPoint);
	}

	public override bool SkillStart(ref PlayerStatus status)
	{
		if (base.SkillStart(ref status) == false) return false;

		// 発動と同時に判定オブジェクトを自分に纏わせる
		GameObject tmp = Instantiate(hitObject, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
		tmp.transform.parent = gameObject.transform;	// 親子関係を結んでおく
		GameObject thunder = Instantiate(thunderWind, transform.position, gameObject.transform.rotation) as GameObject;
		thunder.transform.parent = gameObject.transform;
		return true;
	}

	protected override void SubAwake()
	{
		base.SubAwake();
		setState(SkillType.ST_Raizen, "雷蝉", "raizenRun", 10.0f);
		startType = StartType.NotJumping;

		hitObject	= (GameObject)Resources.Load("Prefab/Blade/BladeHitObject_4");
		thunderWind = (GameObject)Resources.Load("Prefab/ThunderWind"); 
		playerMove	= GetComponent<PlayerMove>();
	}

	protected override void SubUpdate()
	{
		base.SubUpdate();
		if (enabledFlag == true) { return; }	// 使用中以外なら抜ける

		spdVec = transform.forward;
		spdVec.y = 0.0f;
		spdVec.Normalize();
		playerMove.playMovePlayer(spdVec * moveSpeed);
	}

	protected override void skillExit()
	{
		base.skillExit();
		// オブジェクトの子を探して削除
		foreach (Transform child in transform)
		{
			if (0 <= child.name.IndexOf("BladeHit")) { child.GetComponent<BladeScript>().deleteMyObject(); }
			if (0 <= child.name.IndexOf("ThunderWind")) { Destroy(child.gameObject); }
		}
	}

}
