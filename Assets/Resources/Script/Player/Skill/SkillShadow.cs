﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// 絶影
public class SkillShadow : SkillBase {

	private GameObject	shadow;			// 生成する影
	private GameObject	shadowSmoke;	// 影の生成時に出る煙
	private GameObject	hitObject;		// ダメージ判定を行うオブジェクト
	private AudioClip	startSlashSE;	

	private List<GameObject> shadows = new List<GameObject>();	// 攻撃指示をするために影を格納する配列

	private float	createHitObjectDistance = 1.0f;
	private Vector3 hitObjectPos;

	// 影の生成
	private float	distance		= 1.5f;
	private int		createNum		= 5;
	private float	createRotate	= 30.0f;

	private bool isStartSlash = false;

	// オブジェクト生成時に使うラムダ式
	private delegate void Del(float f);
	
	// 影の生成
	void createShadows()
	{
		Transform element = GameObject.Find("Game Element").transform;

		Vector3 direct = transform.forward;
		direct.y = 0.0f;
		hitObjectPos = transform.position + direct * createHitObjectDistance;
		hitObjectPos.y = 0.0f;

		// 方向指定して影を生成するラムダ式
		Del del = f =>
		{
			Quaternion qvec = transform.rotation * Quaternion.AngleAxis(f, new Vector3(0, 1, 0));
			shadows.Add(Instantiate(shadow, hitObjectPos, qvec) as GameObject);
			shadows[shadows.Count-1].transform.Translate(0.0f, 0.0f, distance);
			shadows[shadows.Count-1].transform.LookAt(hitObjectPos);
			shadows[shadows.Count-1].transform.parent = transform; // 削除するために親子関係を結んでおく

			GameObject smoke = Instantiate(shadowSmoke, shadows[shadows.Count - 1].transform.position, Quaternion.identity) as GameObject;
			smoke.transform.parent = element;
		};

		// 正の最大値を出してそこから一定間隔ごと引いて生成
		float num = (float)(createNum - (createNum - 1) / 2) * createRotate;
		for (int i = 1; i <= createNum; ++i)
		{
			// 調整で180度足す
			del(180.0f + num - (float)i * createRotate);
		}
	}

	// 判定の生成
	void createHitObject()
	{
		// 判定オブジェクト生成
		Vector3 direct = transform.forward;
		direct.y = 0.0f;
		GameObject hit = Instantiate(hitObject, hitObjectPos, Quaternion.identity) as GameObject;
		hit.transform.forward = direct;
		hit.transform.parent = transform;
	}

	// 判定や影の削除
	void deleteElements()
	{
		for (int i = 0; i < shadows.Count; ++i) Destroy(shadows[i]);
		shadows.Clear();
		// 攻撃オブジェクトの子を探して削除
		foreach (Transform child in transform)
		{
			if (0 <= child.name.IndexOf("MoreSlashingObject")) { Destroy(child.gameObject); }
		}
	}

	public override void setAttackPoint(int attackPoint)
	{
		base.setAttackPoint(attackPoint);
		// 倍率はここで決めておく
		attackPercent = 1.2f;
		hitObject.GetComponent<MoreSlashing>().setParam((int)((float)attackPoint * attackPercent));
	}

	public override bool SkillStart(ref PlayerStatus status)
	{
		// 判断とコスト処理、animatorは基底クラスに任せる
		if (base.SkillStart(ref status) == false) return false;

		createShadows();
		
		return true;
	}

	protected override void SubAwake()
	{
		base.SubAwake();
		setState(SkillType.ST_Shadow, "絶影", "callShadow", 50.0f);
		startType = StartType.NotJumping;	// 地面発動可能

		shadow		= (GameObject)Resources.Load("Prefab/PlayerShadow");
		shadowSmoke = (GameObject)Resources.Load("Prefab/shadowSmoke");
		hitObject	= (GameObject)Resources.Load("Prefab/Blade/MoreSlashingObject");
		startSlashSE = (AudioClip)Resources.Load("SE/slash5");
	}

	protected override void SubUpdate()
	{
		base.SubUpdate();
		if (enabledFlag == true) { return; }	// 使用中以外なら抜ける
		
		// 回転に移行したら切りかかる
		if (state.IsName("callShadow") == true && state.normalizedTime > 0.75 && isStartSlash == false)
		{
			isStartSlash = true;
			audio.PlayOneShot(startSlashSE);
			foreach (GameObject tmp in shadows) tmp.GetComponent<PlayerShadow>().isSlush = true;
			createHitObject();
		}
	}

	protected override void skillExit()
	{
		base.skillExit();
		deleteElements();
		isStartSlash = false;
	}
}
