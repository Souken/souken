﻿using UnityEngine;
using System.Collections;

// 絶影で使用する影オブジェクト
public class PlayerShadow : MonoBehaviour {

	[SerializeField]
	private Animator animator;
	
	[SerializeField, Tooltip("犬のオブジェクト")]
	private GameObject dogObj;
	[SerializeField, Tooltip("刀のオブジェクト")]
	private GameObject swordObj;
	[SerializeField, Tooltip("現在の色")]
	private Color nowColor;
	[SerializeField, Tooltip("透過の減少度")]
	private float sumAlpha = 0.01f;

	[SerializeField, Tooltip("移動速度")]
	private float moveSpeed = 0.1f;

	[SerializeField, Tooltip("trueになったら切りつける")]
	public bool isSlush = false;
	[SerializeField, Tooltip("消え始める")]
	private bool isStartDestroy = false;

	private AnimatorStateInfo state;

	void Awake()
	{
		// 色を常に更新
		dogObj.renderer.material.color = nowColor;
		foreach (Material mat in swordObj.renderer.materials) mat.color = nowColor;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isStartDestroy == true) { calcDestroy(); return; }

		state = animator.GetCurrentAnimatorStateInfo(0);
		checkSlush();
		checkDestroy();
	}

	// 切り始めのチェック
	void checkSlush()
	{
		// もう切っていたら抜ける
		if (state.IsName("ramp") == false) return;

		if (state.IsName("ramp") == true && isSlush == true)
		{
			animator.SetTrigger("AttackTrigger");
			animator.SetInteger("AttackNum", Random.Range(0, 2));
			isSlush = false;
		}
	}

	// 消滅始めのチェック
	void checkDestroy()
	{
		if (state.IsName("ramp") == true) return;
		// 消え始めるまで移動
		transform.Translate(0.0f, 0.0f, moveSpeed);
		if(state.normalizedTime >= 1.0f)	isStartDestroy = true;
	}

	// 消滅処理
	void calcDestroy()
	{
		nowColor.a -= sumAlpha;
		dogObj.renderer.material.color = nowColor;
		foreach (Material mat in swordObj.renderer.materials) mat.color = nowColor;
		if (nowColor.a <= 0.0f) Destroy(gameObject);
	}

}
