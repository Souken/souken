﻿using UnityEngine;
using System.Collections;

// 花火
public class SkillFlower : SkillBase {

	private AudioClip	fireSE;
	private GameObject	fireWork;		// 花火
	private GameObject	magicCircle;	// 魔方陣

	private float rollTime = 3.0f;				// 回転時間
	private float createIntervalTime = 0.05f;	// 花火生成の間隔
	private float playIntervalTime = 0.1f;		// 効果音の再生間隔
	
	private float nowIntervalTime = 0.0f;
	private float seTime = 0.0f;
	private float nowTime = 0.0f;

	public override void setAttackPoint(int attackPoint)
	{
		base.setAttackPoint(attackPoint);
		// 倍率はここで決めておく
		attackPercent = 1.2f;
		fireWork.GetComponent<Firework>().setAttack((int)((float)attackPoint * attackPercent));
	}

	public override bool SkillStart(ref PlayerStatus status)
	{
		// 判断とコスト処理、animatorは基底クラスに任せる
		if (base.SkillStart(ref status) == false) return false;
		Instantiate(magicCircle, gameObject.transform.position, transform.rotation);
		return true;
	}

	protected override void SubAwake()
	{
		base.SubAwake();
		setState(SkillType.ST_Flower, "花火", "flowerJump", 50.0f);
		startType = StartType.Anytime;	// 地面と空中両方発動可能

		fireSE		= (AudioClip)Resources.Load("se/damageBurn2");
		fireWork	= (GameObject)Resources.Load("Prefab/fireWork");
		magicCircle = (GameObject)Resources.Load("Prefab/MagicCircle");
	}

	protected override void SubUpdate()
	{
		base.SubUpdate();
		if (enabledFlag == true) { return; }	// 使用中以外なら抜ける

		// ローリング中
		if (state.IsName("roll") == true && nowTime < rollTime)
		{
			// 一定時間経ったら終了
			nowTime += Time.deltaTime;
			if (nowTime >= rollTime) animator.SetTrigger("exitFlowerRoll");

			// 一定間隔で生成
			nowIntervalTime += Time.deltaTime;
			seTime += Time.deltaTime;
			if(seTime >= playIntervalTime)
			{
				audio.PlayOneShot(fireSE);
				seTime = 0.0f;
			}
			if (nowIntervalTime >= createIntervalTime)
			{
				createFirework();
				nowIntervalTime = 0.0f;
			}
		}
	}

	protected override void skillExit()
	{
		base.skillExit();
		nowTime = 0.0f;
		seTime = 0.0f;
		nowIntervalTime = 0.0f;
	}

	// 火弾生成
	void createFirework()
	{
		// 方向ランダム
		GameObject tmp = Instantiate(fireWork, transform.position, Random.rotation) as GameObject;
		tmp.transform.parent = GameObject.Find("Game Element").transform;
	}

}
