﻿using UnityEngine;
using System.Collections;

// 岩通し
// ボタン押しっぱなしで三段階まで溜めが可能
// 1段階目…2倍, 2段階目…4倍, 3段階目…6倍
public class SkillBreakRock : SkillBase {

	// 溜めの段階数
	private const int	CHARGE_STEP = 3;
	private const float CHARGE_TIME	= 2.0f;
	private int			nowStep		= 0;
	private float		nowTime		= 0.0f;

	private BreakRockData effectData;
	
	private GameObject chargeEffect;
	private GameObject chargeLineEffect;
	private GameObject hitObject;
	private GameObject bladeObject;

	private AudioClip chargeSE;
	private AudioClip stepUpSE;
	private AudioClip slashSE;

	// 攻撃に移ったか
	bool isAttack = false;

	public override bool SkillStart(ref PlayerStatus status)
	{
		// 判断とコスト処理、animatorは基底クラスに任せる
		if (base.SkillStart(ref status) == false) return false;
		
		nowStep = 1;
		nowTime = 0.0f;
		isAttack = false;

		chargeLineEffect.particleSystem.emissionRate = 200.0f;
		stepUpEffect();

		audio.clip = chargeSE;
		audio.Play();

		return true;
	}

	protected override void SubAwake()
	{
		base.SubAwake();
		setState(SkillType.ST_BreakRock, "岩通し", "breakCharge", 30.0f);
		startType = StartType.NotJumping;

		// エフェクト情報読み込み
		effectData	= Resources.Load("Script/Player/Skill/BreakRockData") as BreakRockData;

		hitObject	= (GameObject)Resources.Load("Prefab/Blade/breakRockHit");
		bladeObject = transform.FindChild("Hips/Spine1/Spine/Neck/Head1/kuroBlade").gameObject;	// ごり押し過ぎる

		chargeEffect		= Instantiate((GameObject)Resources.Load("Prefab/Blade/bladeCharge"), bladeObject.transform.position, Quaternion.identity) as GameObject;
		chargeLineEffect	= Instantiate((GameObject)Resources.Load("Prefab/Blade/bladeLineCharge"), bladeObject.transform.position, Quaternion.identity) as GameObject;
		
		// 効果音読み込み
		chargeSE = (AudioClip)Resources.Load("SE/rockCharge");
		stepUpSE = (AudioClip)Resources.Load("SE/stepUp");
		slashSE = (AudioClip)Resources.Load("SE/breakRockSlash");

		// 剣と親子関係を結ぶ
		chargeEffect.transform.forward		= transform.forward;
		chargeEffect.transform.parent		= bladeObject.transform;
		chargeEffect.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), -90.0f);
		chargeLineEffect.transform.forward	= transform.forward;
		chargeLineEffect.transform.parent	= bladeObject.transform;
		chargeLineEffect.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), -90.0f);

		// 消しておく
		chargeEffect.particleSystem.emissionRate		= 0.0f;
		chargeLineEffect.particleSystem.emissionRate	= 0.0f;
	}

	protected override void SubUpdate()
	{
		base.SubUpdate();
		if (enabledFlag == true) { return; }	// 使用中以外なら抜ける

		// 攻撃判定生成
		if (isAttack == true)
		{
			if (state.IsName("attack1") == true && state.normalizedTime >= 0.2)
			{
				audio.Stop();
				audio.clip = null;
				audio.PlayOneShot(slashSE);

				GameObject tmp = Instantiate(hitObject, transform.position, Quaternion.identity) as GameObject;
				tmp.transform.forward = transform.forward;
				tmp.transform.parent = transform;
				tmp.GetComponent<BreakRockHit>().setStatus(nowStep - 1, effectData.attackDamage[nowStep - 1] * GetComponent<PlayerStatus>().getAttackPoint());
				isAttack = false;
			}
			return;
		}

		// ボタンを押しっぱなしかどうか
		if (state.IsName("breakCharge") == true && Input.GetButton(useSkillButtonName) == true)
		{
			if (nowStep == CHARGE_STEP) return;

			nowTime += Time.deltaTime;
			if (nowTime >= CHARGE_TIME)
			{
				audio.PlayOneShot(stepUpSE);

				nowTime = 0.0f;
				++nowStep;
				stepUpEffect();
			}
		}
		// ボタンが離されたら当たり判定オブジェクト生成&ダメージ設定
		else if (state.IsName("breakCharge") == true && Input.GetButton(useSkillButtonName) == false)
		{
			animator.SetTrigger("breakRockTrigger");
			isAttack = true;
		}
	}

	// 終了の際にエフェクトを消す
	protected override void skillExit()
	{
		base.skillExit();
		chargeEffect.particleSystem.emissionRate = 0.0f;
		chargeLineEffect.particleSystem.emissionRate = 0.0f;

		nowTime = 0.0f;
		nowStep = 0;
		isAttack = false;

		audio.Stop();
		audio.clip = null;

		// 攻撃オブジェクトの子を探して削除
		foreach (Transform child in transform)
		{
			if (child.tag == "attack") { Destroy(child.gameObject); }
		}
	}

	// エフェクト更新
	void stepUpEffect()
	{
		chargeEffect.particleSystem.startLifetime	= effectData.lifeTime[nowStep - 1];
		chargeEffect.particleSystem.startSpeed		= effectData.speed[nowStep - 1];
		chargeEffect.particleSystem.startSize		= effectData.size[nowStep - 1];
		chargeEffect.particleSystem.emissionRate	= effectData.emmisionRate[nowStep - 1];

		chargeLineEffect.particleSystem.startLifetime = 0.3f * (float)nowStep;
	}

}
