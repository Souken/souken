﻿using UnityEngine;
using System.Collections;

public class BreakRockData : ScriptableObject {
	
	public float[]	lifeTime		= new float[3];
	public float[]	speed			= new float[3];
	public float[]	size			= new float[3];
	public float[]	emmisionRate	= new float[3];
	public int[] attackDamage = new int[3];

}
