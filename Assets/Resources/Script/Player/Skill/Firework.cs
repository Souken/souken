﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Firework : MonoBehaviour {

    private List<int> hitEnemyList = new List<int>(); // ヒットした敵のIDを格納しておく配列

    [SerializeField, Tooltip("生成されてから消えるまでの時間(秒)")]
    private float deleteTime = 2.0f;
    [SerializeField]
    private int damage = 10;
    [SerializeField]
    private float speed = 0.2f;

    // 炎の色の範囲
    [SerializeField, Tooltip("FireColor(min)")]
    private Color min = new Color(0.5f, 0.5f, 0.5f, 1.0f);
    [SerializeField, Tooltip("FireColor(max)")]
    private Color max = new Color(1.0f, 1.0f, 1.0f, 1.0f);

    private float nowTime = 0.0f;
    private bool hitFlag = false;

    ParticleSystem fire = null, hitFire = null;

    public void setAttack(int attackPoint)
    {
        damage = attackPoint;
    }

    void Start()
    {
        fire = gameObject.transform.FindChild("fire").GetComponent<ParticleSystem>();
        hitFire = gameObject.transform.FindChild("hitFire").GetComponent<ParticleSystem>();

        // 色を同じにする
        fire.startColor = RandomColor();
        hitFire.startColor = fire.startColor;
    }

    void Update()
    {
        nowTime += Time.deltaTime;
        if (nowTime >= deleteTime) { Destroy(gameObject); }

        if (hitFlag == false)
        {
            // 移動はここに書く
            transform.Translate(new Vector3(0.0f, 0.0f, speed));
        }
    }

    // 攻撃判定
    void OnTriggerStay(Collider other)
    {
        // 既に当たっているか、攻撃以外もしくは敵以外の場合抜ける
        if (hitFlag || other.gameObject.tag != "enemy") { return; }

        // ここまできたら判定する
        EnemyBase enemy = other.gameObject.GetComponent<EnemyBase>();
        enemy.calcDamage(damage);   // ここはステータスからattackの値を引っ張ってくる
        hitFlag = true;
        print(enemy.getHitPoint().ToString());  // デバッグ
        
        // fireを止めてhitFireの再生を始める
        fire.emissionRate = 0.0f;
        hitFire.Play();
        // nowTimeをhitFireの再生終了と同時に消えるように調整
        nowTime = deleteTime - hitFire.startLifetime;
    }

    private Color RandomColor()
    {
        return new Color(Random.Range(min.r, max.r), Random.Range(min.g, max.g), Random.Range(min.b, max.b), Random.Range(min.a, max.a));
    }
}
