﻿using UnityEngine;
using System.Collections;

// 水蓮
public class SkillWater : SkillBase {

	[SerializeField] private int	createNum = 8;
	[SerializeField, Tooltip("自分からどれくらい水柱を離すか")]
	private float distanceFromCenter = 0.7f;

	private AudioClip waterSE;
	private GameObject water;

	// 水柱生成時に使うラムダ式
	private delegate void Del(float f);

	public override void setAttackPoint(int attackPoint)
	{
		base.setAttackPoint(attackPoint);
		attackPercent = 1.5f;
		water.GetComponent<Water>().setAttack((int)((float)attackPoint * attackPercent));
	}

	public override bool SkillStart(ref PlayerStatus status)
	{
		if (base.SkillStart(ref status) == false) return false;
		return true;
	}

	protected override void SubAwake()
	{
		base.SubAwake();
		setState(SkillType.ST_Water, "水蓮", "water", 20.0f);
		startType = StartType.NotJumping;

		waterSE = (AudioClip)Resources.Load("SE/suirenSE");
		water = (GameObject)Resources.Load("Prefab/SuirenWater");
	}

	protected override void SubUpdate()
	{
		base.SubUpdate();
		if (enabledFlag == true) { return; }	// 使用中以外なら抜ける
	}

	// アニメーションの挙動に付随して水柱生成
	public void createWater()
	{
		audio.PlayOneShot(waterSE);

		// 生成
		Vector3 pos = transform.position;
		pos.y = 0.5f;

		// 方向指定して水柱を生成するラムダ式
		// 生成した後指定距離話す
		Del del = f =>
		{
			Quaternion qvec = transform.rotation * Quaternion.AngleAxis(f, new Vector3(0, 1, 0));
			GameObject tmp = Instantiate(water, pos, qvec) as GameObject;
			tmp.transform.Translate(0.0f, 0.0f, distanceFromCenter);
			//tmp.transform.parent = transform;
			tmp.transform.parent = GameObject.Find("Game Element").transform;
		};

		float rotateNum = 360.0f / (float)createNum;	// 一個ごとの間隔
		// 個数を自分の周りに生成
		for(int i = 0; i < createNum; ++i)
		{
			del(rotateNum * (float)i);
		}
	}

}
