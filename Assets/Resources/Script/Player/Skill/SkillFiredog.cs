﻿using UnityEngine;
using System.Collections;

// 火炎犬
public class SkillFiredog : SkillBase {

	private const float SHOT_FIREDOG_TIME = 0.8f;	// モーションから発射までの秒数
	private float		nowSeconds = 0.0f;

	private AudioClip	fireSE;
	private GameObject	fireDog;	// 火炎犬
	private GameObject	magicCircle;// 魔方陣

	public override void setAttackPoint(int attackPoint)
	{
		base.setAttackPoint(attackPoint);
		// 倍率はここで決めておく
		attackPercent = 1.8f;
		fireDog.GetComponent<Firedog>().setAttack((int)((float)attackPoint * attackPercent));
	}

	public override bool SkillStart(ref PlayerStatus status)
	{
		// 判断とコスト処理、animatorは基底クラスに任せる
		if(base.SkillStart(ref status) == false) return false;
		nowSeconds = 0.0f;
		Instantiate(magicCircle, gameObject.transform.position, transform.rotation);
		return true;
	}

	protected override void SubAwake()
	{
		base.SubAwake();
		setState(SkillType.ST_FireDog, "火炎犬", "fireSit", 10.0f);
		startType = StartType.NotJumping;

		fireSE		= (AudioClip)Resources.Load("se/damageBurn2");
		fireDog		= (GameObject)Resources.Load("Prefab/firedog");
		magicCircle = (GameObject)Resources.Load("Prefab/MagicCircle");
	}

	protected override void SubUpdate()
	{
		base.SubUpdate();
		if (enabledFlag == true) { return; }	// 使用中以外なら抜ける

		nowSeconds += Time.deltaTime;
		if (nowSeconds >= SHOT_FIREDOG_TIME)
		{
			audio.PlayOneShot(fireSE);
			GameObject tmp = Instantiate(fireDog, gameObject.transform.position, Quaternion.identity) as GameObject;
			tmp.transform.parent = GameObject.Find("Game Element").transform;
			nowSeconds = -100.0f;
		}
	}
}
