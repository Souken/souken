﻿using UnityEngine;
using System.Collections;

// 双狗乱舞
// 使用中は無敵
public class SkillSouken : SkillBase {

	private GameObject shiroObj;
	private GameObject kuroObj;
	private GameObject smoke;
	private GameObject hitObject;

	private AudioClip initSE;
	private AudioClip createSE;

	[SerializeField, Tooltip("犬の生成時の誤差範囲")]
	private float createDogsDistance = 2.0f;

	[SerializeField]
	private bool isStart = false;
	[SerializeField]
	private bool isHighSpeed = false;
	// 今は仮で5秒間無敵
	[SerializeField, Tooltip("終了")]
	private bool isExitAttack = false;

	private const float WAIT_TIME			= 0.5f;
	private const float ATTACK_TIME			= 5.0f;
	private const float START_HIGH_SPEED	= 3.0f;

	private const float NORMAL_CREATE_DOG_INTERVAL	= 0.1f;
	private const float HIGH_CREATE_DOG_INTERVAL	= 0.05f;

	private const float NORMAL_SE_INTERVAL	= 0.2f;
	private const float HIGH_SE_INTERVAL	= 0.1f;

	private float nowTime = 0.0f;
	private float createTime = 0.0f;
	private float seTime = 0.0f;

	private float createHitObjectDistance = 1.0f;
	private Vector3 hitObjectPos;

	// 当たり判定そのものを消すため、発動時の場所を覚えておく
	[SerializeField]
	private Vector3 initPos;
	Vector3 direct = Vector3.zero;

	// 犬の生成で使う
	private GameObject element;
	private float hitObjectRadius = 0.0f;
	private delegate void Create(string name);

	private delegate void CalcPattern();
	CalcPattern pattern;

	// 判定の生成
	void createHitObject()
	{
		// 判定オブジェクト生成
		direct = transform.forward;
		direct.y = 0.0f;
		hitObjectPos = initPos + direct * createHitObjectDistance;
		hitObjectPos.y = 0.0f;
		GameObject hit = Instantiate(hitObject, hitObjectPos, Quaternion.identity) as GameObject;
		hit.transform.forward = direct;
		hit.transform.parent = transform;
	}

	// 待機
	void calcWait()
	{
		if (nowTime >= WAIT_TIME)
		{
			nowTime = 0.0f;
			pattern = calcAttack;
			createHitObject();
		}
	}
	// 攻撃
	void calcAttack()
	{
		calcCreateDogs();
		calcSE();
		if (isExitAttack == false && nowTime >= ATTACK_TIME)
		{
			isExitAttack = true;
			animator.SetTrigger("exitSouken");
			Instantiate(smoke, transform.position, transform.rotation);
			foreach (Transform child in transform)
			{
				if (0 <= child.name.IndexOf("SoukenSlashingObject")) { Destroy(child.gameObject); break; }
			}
		}
		if (isHighSpeed == false && nowTime >= START_HIGH_SPEED)
		{
			isHighSpeed = true;
			foreach (Transform child in transform)
			{
				if (0 <= child.name.IndexOf("SoukenSlashingObject")) { child.GetComponent<SoukenSlashing>().highSpeed(); return; }
			}
		}
	}

	// 犬生成
	void calcCreateDogs()
	{
		createTime += Time.deltaTime;
		if (createTime >= (isHighSpeed == true ? HIGH_CREATE_DOG_INTERVAL : NORMAL_CREATE_DOG_INTERVAL))
		{
			createTime = 0.0f;
			// 生成を行うラムダ式
			Create create = name =>
			{
				// Yがマイナス方向を向かない範囲でランダムに方向を決める
				Vector3 direct = new Vector3(Mathf.Sin(Random.Range(0.0f, 2.0f)) * Mathf.PI, Mathf.Sin(Random.Range(0.0f, 1.0f)) * Mathf.PI, Mathf.Cos(Random.Range(0.0f, 2.0f)) * Mathf.PI);
				// 若干場所をずらす
				Vector3 createPos = new Vector3(
					Random.Range(hitObjectPos.x - createDogsDistance, hitObjectPos.x + createDogsDistance), hitObjectPos.y,
					Random.Range(hitObjectPos.z - createDogsDistance, hitObjectPos.z + createDogsDistance));
				GameObject dog = Instantiate(name == "shiro" ? shiroObj : kuroObj, createPos, Quaternion.identity) as GameObject;
				dog.transform.forward = direct;
				dog.transform.Translate(0.0f, 0.0f, hitObjectRadius * 3.0f);
				dog.transform.forward = direct * -1.0f;
				dog.transform.parent = element.transform;
			};

			create("shiro");
			create("kuro");
		}

	}

	// 効果音
	void calcSE()
	{
		seTime += Time.deltaTime;
		if (seTime >= (isHighSpeed == true ? HIGH_SE_INTERVAL : NORMAL_SE_INTERVAL))
		{
			seTime = 0.0f;
			audio.PlayOneShot(createSE);
		}
	}

	public override void setAttackPoint(int attackPoint)
	{
		base.setAttackPoint(attackPoint);
		// 倍率はここで決めておく
		attackPercent = 1.0f;
		hitObject.GetComponent<SoukenSlashing>().setParam((int)((float)attackPoint * attackPercent));
	}

	public override bool SkillStart(ref PlayerStatus status)
	{
		// 判断とコスト処理、animatorは基底クラスに任せる
		if (base.SkillStart(ref status) == false) return false;

		initPos = transform.position;
		status.startInvincible();
		Instantiate(smoke, transform.position, transform.rotation);
		audio.PlayOneShot(initSE);
		return true;
	}

	protected override void SubAwake()
	{
		base.SubAwake();
		setState(SkillType.ST_Souken, "双狗乱舞", "slashSouken", 100.0f);
		startType = StartType.NotJumping;	// 地面発動可能

		shiroObj	= (GameObject)Resources.Load("Prefab/SoukenShiro");
		kuroObj		= (GameObject)Resources.Load("Prefab/SoukenKuro");
		smoke		= (GameObject)Resources.Load("Prefab/shadowSmoke");
		hitObject	= (GameObject)Resources.Load("Prefab/Blade/SoukenSlashingObject");

		initSE = (AudioClip)Resources.Load("SE/tackle");
		createSE = (AudioClip)Resources.Load("SE/teleport");

		element = GameObject.Find("Game Element");
		hitObjectRadius = hitObject.GetComponent<SoukenSlashing>().getRadius() * 3.0f;

		pattern = calcWait;
	}

	// 更新
	protected override void SubUpdate()
	{
		base.SubUpdate();
		if (enabledFlag == true) { return; }	// 使用中以外なら抜ける
		transform.position.Set(initPos.x, initPos.y, initPos.z);

		if (isExitAttack == true) return;
		nowTime += Time.deltaTime;
		pattern();
	}

	protected override void skillExit()
	{
		base.skillExit();
		foreach (Transform child in transform)
		{
			if (0 <= child.name.IndexOf("SoukenSlashingObject")) { Destroy(child.gameObject); break; }
		}
		GetComponent<PlayerStatus>().endInvincible();

		nowTime		= 0.0f;
		createTime	= 0.0f;
		seTime		= 0.0f;

		isStart = false;
		isExitAttack = false;
		isHighSpeed = false;

		pattern = calcWait;
	}
}
