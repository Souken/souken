﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// 水蓮発生時に出る水柱
public class Water : MonoBehaviour {

	private List<int> hitEnemyList = new List<int>(); // ヒットした敵のIDを格納しておく配列

	// ダメージエフェクト追加
	private static GameObject shock;

	[SerializeField, Tooltip("生成されてから消えるまでの時間(秒)")]
	private float deleteTime = 2.0f;
	[SerializeField] private int damage = 10;

	private float nowTime = 0.0f;

	public void setAttack(int attackPoint)
	{
		damage = attackPoint;
	}

	// Use this for initialization
	void Start () {
		if (shock == null) shock = (GameObject)Resources.Load("Prefab/shock2");
		gameObject.layer = 9;
	}
	
	// Update is called once per frame
	void Update () {
		nowTime += Time.deltaTime;
		if (nowTime >= deleteTime) { Destroy(gameObject); }
	}

	// 攻撃判定
	void OnTriggerStay(Collider other)
	{
		// 攻撃以外もしくは敵以外の場合抜ける
		if (other.gameObject.tag != "enemy") { return; }
		if (other.GetComponent<EnemyBase>().isInvincible() == true) return;

		// 既に判定済みなら抜ける
		EnemyBase enemy = other.gameObject.GetComponent<EnemyBase>();
		foreach (int id in hitEnemyList) { if (id == enemy.getID()) { return; } }

		// ここまできたら判定する
		hitEnemyList.Add(enemy.getID());
		enemy.calcDamage(damage);   // ここはステータスからattackの値を引っ張ってくる
		Instantiate(shock, enemy.transform.position, Quaternion.identity);	// エフェクト生成
		
		print(enemy.getHitPoint().ToString());  // デバッグ
	}

}
