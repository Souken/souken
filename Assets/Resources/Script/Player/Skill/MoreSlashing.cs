﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// 絶影で生成する細かいダメージを与え続けるオブジェクト
public class MoreSlashing : MonoBehaviour {

	[SerializeField, Tooltip("判定が行われてから判定処理を解除するまでの時間")]
	private float intervalTime = 0.1f;
	[SerializeField, Tooltip("確認用")]
	private int attackNum = 0;
	
	// ヒットした敵のIDを格納しておく配列
	// 判定が行われたらIDをキーとして、中身に一定値を入れる
	// 中身の値が0になったらリストから除外して判定を行えるようにする
	private Dictionary<int, float> hitEnemyList = new Dictionary<int,float>();
	
	// ダメージエフェクト追加
	private GameObject	shock;
	private AudioClip slashSE;

	// スキル側で設定した値をもってくる
	public void setParam(int attackPoint)
	{
		attackNum = attackPoint;
	}

	// Use this for initialization
	void Start () {
		shock = (GameObject)Resources.Load("Prefab/shock2");
		slashSE = (AudioClip)Resources.Load("SE/slash2");
		gameObject.layer = 9;
	}
	
	// Update is called once per frame
	void Update () {
		// 要素が何もないなら抜ける
		if (hitEnemyList.Count == 0) return;
		
		// リストの要素の時間を毎回減少させ、0になったらその要素は削除
		foreach (int i in new List<int>(hitEnemyList.Keys))
		{
			float num = hitEnemyList[i];
			num -= Time.deltaTime;
			if (num <= 0.0f) hitEnemyList.Remove(i);
			else hitEnemyList[i] = num;
		}
	}

	// 攻撃判定
	void OnTriggerStay(Collider other)
	{
		// 攻撃以外もしくは敵以外の場合抜ける
		if (other.gameObject.tag != "enemy") { return; }
		if (other.GetComponent<EnemyBase>().isInvincible() == true) return;

		// 既に判定済みなら抜ける
		EnemyBase enemy = other.gameObject.GetComponent<EnemyBase>();
		foreach (KeyValuePair<int, float> id in hitEnemyList) { if (id.Key == enemy.getID()) { return; } }

		// ここまできたら判定する
		hitEnemyList.Add(enemy.getID(), intervalTime);
		enemy.calcDamage(attackNum);   // ここはステータスからattackの値を引っ張ってくる
		Instantiate(shock, gameObject.transform.position, Quaternion.identity);	// エフェクト生成
		audio.PlayOneShot(slashSE);

		print(enemy.getHitPoint().ToString());  // デバッグ
	}

}
