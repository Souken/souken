﻿using UnityEngine;
using System.Collections;

// スキルの基底クラス
public class SkillBase : MonoBehaviour {

	protected enum SkillType
	{
		ST_FireDog = 1, ST_Raizen, ST_Water, ST_BreakRock, ST_Flower, ST_Shadow, ST_Souken
	};
	protected SkillType type;

	protected Animator			animator;
	protected AnimatorStateInfo state, oldState;
	protected string			skillName	= "";
	protected string			startSkillStateName = "";
	protected float				costPoint	= 0.0f;	// スキル使用ポイント
	protected int				skillID		= 0;	// 1か2のどちらかが入る

	[SerializeField, Tooltip("攻撃倍率")]
	protected float				attackPercent = 1.0f;

	protected bool enabledFlag = true;				// 使用可能かどうか(trueの場合使える)
	static protected bool commonEnabledFlag = true;// スキル共通の使用フラグ
	static public bool isSkillPlayingNow() { return commonEnabledFlag == true ? false : true; }

	// どのタイミングで使えるか(ジャンプ中はダメとか)
	// 列挙型で制御してみる
	protected enum StartType { NotJumping, Anytime };
	protected StartType startType;

	// スキル割り当てられているボタン
	// 押しっぱなしなどの判断に使用
	public string useSkillButtonName { protected get; set; }

	public void		setSkillID(int id)	{ skillID = id; }

	public string	getSkillName()		{ return skillName; }
	public float	getCost()			{ return costPoint; }

	// 攻撃力の設定
	public virtual void setAttackPoint(int attackPoint) { }

	// 発動時の処理
	public virtual bool SkillStart(ref PlayerStatus status)
	{
		// スキル使用不可ならすぐ抜ける
		if (enabledFlag == false || checkStartSkill() == false || status.getSkillPoint() < costPoint) { return false; }

		// 通常攻撃の途中だったらフラグを折る
		PlayerNormalAttack normalAttack = GetComponent<PlayerNormalAttack>();
		normalAttack.exitAttack();
		animator.Play(startSkillStateName);

		// 使用可能なら発動
		enabledFlag = false; 
		commonEnabledFlag = false;
		status.invocationSkill(costPoint);

		animator.SetBool("skill", true);
		animator.SetInteger("skillID", (int)type);

		return true;
	}

	protected virtual void SubAwake()
	{ 
		animator	= GetComponent<Animator>();
		state		= animator.GetCurrentAnimatorStateInfo(0);
	}
	protected virtual void SubUpdate()
	{
		oldState = state;
		state = animator.GetCurrentAnimatorStateInfo(0);

		// 吹っ飛び中ならスキル中断
		if (animator.GetBool("damageDown") == true || animator.GetBool("damageShrink") == true && enabledFlag == false)
		{
			skillExit(); 
			return;
		}

		// ループ防止
		if (animator.GetBool("skillExit") == false && enabledFlag == false && state.IsTag("attack") == true) { animator.SetBool("skillExit", true); }

		// 終了してたらフラグをオフにして抜ける
		if (enabledFlag == false && state.IsTag("attack") != true && oldState.IsTag("attack") == true)
		{
			print("正常終了 :" + skillName);
			skillExit();	
		}
	}

	protected void setState(SkillType arg_type, string arg_name, string startStateName, float arg_cost)
	{
		type		= arg_type;
		skillName	= arg_name;
		startSkillStateName = startStateName;
		costPoint	= arg_cost;
	}

	protected virtual void skillExit()
	{
		animator.SetBool("skill", false);
		animator.SetBool("skillExit", false);
		animator.SetInteger("skillID", 0);
		enabledFlag = true;
		commonEnabledFlag = true;
	}

	// Use this for initialization
	void Awake () {
		SubAwake();
	}
	
	// Update is called once per frame
	void Update () {
		SubUpdate();
	}

	// スキルを発動できる状態かどうかのチェック
	bool checkStartSkill()
	{
		switch (startType)
		{
			case StartType.NotJumping:
				return animator.GetBool("jumping") == false ? true : false;
			case StartType.Anytime:
				return true;
		}

		return false;
	}

}
