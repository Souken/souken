﻿using UnityEngine;
using System.Collections;

// 火炎犬本体
public class Firedog : MonoBehaviour {

    [SerializeField] private int	DeleteFrame = 180; // デフォルトは3秒で消える
    [SerializeField] private float	speed = 0.2f;
	[SerializeField] private int	attack = 30;
	private int nowFrame = 0;
    private int BreakObjectNum = 9;

	public void setAttack(int attackPoint)
	{
		attack = attackPoint;
	}

	// Use this for initialization
	void Start () {
        // プレイヤーの向いている方向に向かせる
        gameObject.transform.forward = GameObject.Find("Player").transform.forward;
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.Translate(new Vector3(0.0f, 0.0f, speed));
        if (++nowFrame >= DeleteFrame) { Destroy(gameObject); }
	}

    // 仮当たり判定
    void OnTriggerEnter(Collider collider)
    {
        // 敵or壊せるものに当たったら自分消滅&ダメージ
        if (collider.gameObject.tag != "enemy") { return; }
		if (collider.GetComponent<EnemyBase>().isInvincible() == true) return;

		collider.gameObject.GetComponent<EnemyBase>().calcDamage(attack);
		Instantiate((GameObject)Resources.Load("Prefab/fire"), collider.gameObject.transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

}
