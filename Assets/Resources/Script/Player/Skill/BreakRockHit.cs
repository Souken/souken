﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BreakRockHit : MonoBehaviour {

	private List<int> hitEnemyList = new List<int>(); // 現在のモーション中にヒットした敵のIDを格納しておく配列

	// ダメージエフェクト追加
	[SerializeField]
	private GameObject shock;

	[SerializeField]
	private Vector3[] createPos	= new Vector3[3];
	[SerializeField]
	private Vector3[] scale		= new Vector3[3];

	private int damage;

	public void setStatus(int step, int arg_damage)
	{
		transform.Translate(createPos[step]);
		transform.localScale = scale[step];
		damage = arg_damage;
	}

	// Use this for initialization
	void Start () {
		//shock = (GameObject)Resources.Load("Prefab/shock2");
		gameObject.layer = 9;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// 攻撃判定
	void OnTriggerStay(Collider other)
	{
		// 攻撃以外もしくは敵以外の場合抜ける
		if (other.gameObject.tag != "enemy") { return; }
		if (other.GetComponent<EnemyBase>().isInvincible() == true) return;

		// 既に判定済みなら抜ける
		EnemyBase enemy = other.gameObject.GetComponent<EnemyBase>();
		foreach (int id in hitEnemyList) { if (id == enemy.getID()) { return; } }

		// ここまできたら判定する
		hitEnemyList.Add(enemy.getID());
		enemy.calcDamage(damage);
		Instantiate(shock, gameObject.transform.position, Quaternion.identity);	// エフェクト生成

		print(enemy.getHitPoint().ToString());  // デバッグ
	}
}
