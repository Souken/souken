﻿using UnityEngine;
using System.Collections;

// スキルの設定をする時に使用
// 正直微妙だがプレイヤーオブジェクトに貼り付けて使用すること前提
public class SkillFactory : MonoBehaviour {

	void Start()
	{

	}

	void Update()
	{

	}

	// 自身にコンポーネントを追加した後
	// 引数の基底クラスに入れてやる
	public void setSkill(string skillName, out SkillBase skill)
	{
		switch (skillName)
		{
			case "Firedog":
				gameObject.AddComponent<SkillFiredog>();
				skill = GetComponent<SkillFiredog>();	return;

			case "Water":
				gameObject.AddComponent<SkillWater>();
				skill = GetComponent<SkillWater>();		return;

			case "Raizen":
				gameObject.AddComponent<SkillRaizen>();
				skill = GetComponent<SkillRaizen>();	return;

			case "BreakRock":
				gameObject.AddComponent<SkillBreakRock>();
				skill = GetComponent<SkillBreakRock>();	return;

			case "Flower":
				gameObject.AddComponent<SkillFlower>();
				skill = GetComponent<SkillFlower>(); return;

			case "Zetsu":
				gameObject.AddComponent<SkillShadow>();
				skill = GetComponent<SkillShadow>(); return;
			case "Sou":
				gameObject.AddComponent<SkillSouken>();
				skill = GetComponent<SkillSouken>(); return;

			default: 
				Debug.LogError("スキルが見つかりません"); skill = null; return;
		}
	}

}
