﻿using UnityEngine;
using System.Collections;

public class NormalMagic : MonoBehaviour {

	[SerializeField]
	private GameObject hitEffect;
	[SerializeField, Tooltip("持続時間")]
	private float 	duration = 1.0f;
	[SerializeField]
	private float	speed = 0.2f;
	[SerializeField, Tooltip("攻撃倍率")]
	private float attackPercent = 0.3f;
	[SerializeField, Range(0.0f, 0.5f), Tooltip("追尾力")]
	private float ROTATE_POWER = 0.01f;

	private GameObject target;
	private int attack = 5;
	private float nowTime = 0.0f;

	public void setAttackNum(int attackPoint)
	{
		attack = (int)((float)attackPoint * attackPercent);
	}

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectsWithTag("enemy")[0];	// 敵の一番若い番号の奴を狙う
	}
	
	// Update is called once per frame
	void Update () {
		nowTime += Time.deltaTime;
		if (nowTime >= duration)
		{
			Destroy(gameObject);
			return;
		}

		// 自分の座標と自分の座標から相手への方向ベクトル算出
		Vector3 myPos = transform.position;
		Vector3 tarPos = target.transform.position;
		Vector3 targetDirect = (tarPos - myPos).normalized;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);

		transform.Translate(new Vector3(0.0f, 0.0f, speed));
	}

	// 当たり判定
	void OnTriggerEnter(Collider collider)
	{
		// 敵に当たったら自分消滅&ダメージ
		if (collider.gameObject.tag != "enemy") { return; }
		if (collider.GetComponent<EnemyBase>().isInvincible() == true) return;

		collider.gameObject.GetComponent<EnemyBase>().calcDamage(attack);
		Instantiate(hitEffect, collider.gameObject.transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
