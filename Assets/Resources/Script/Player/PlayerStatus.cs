﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerNormalAttack))]
[RequireComponent(typeof(PlayerMove))]
[RequireComponent(typeof(PlayerSkill))]
[RequireComponent(typeof(VolSet))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]

// プレイヤーの状態(HP, SPなど)
public class PlayerStatus : CharacterBase {

    private const float             MAX_SKILLPOINT      = 100.0f;
    private float                   skillPoint          = MAX_SKILLPOINT;
    [SerializeField] private float  recoverySkillPoint  = 0.1f;

	[SerializeField] private GameObject shiroBlade;
	[SerializeField] private GameObject kuroBlade;
	private GameObject changeRing;

	private AudioClip damageBlow;
	public bool damageDownFlag = false;	// ふっとばされてる時用

	static public Animator animator { get; private set; }
	static public AnimatorStateInfo animatorState { get; private set; }
	static public AnimatorStateInfo oldAnimatorState { get; private set; } 

	// 動かせる状態か
	public bool isMoveStatus() 
	{
		if (animator.GetBool("damageShrink") == true || animator.GetBool("damageDown") == true) return false;
		return nowState == CharacterBase.State.Alive ? true : false; 
	}

	// 切り替え
	enum PlayerType { SHIRO = 0, KURO }
	PlayerType type = PlayerType.SHIRO;
	GameObject	matObject;
	Material	shiroMat;
	Material	kuroMat;
	PlayerSkill playerSkill;
	[SerializeField, Tooltip("スキルアイコン1")]
	private GUITexture skillIcon1;
	[SerializeField, Tooltip("スキルアイコン2")]
	private GUITexture skillIcon2;

	// シロかクロか
	public bool isShiroType() { return type == PlayerType.SHIRO ? true : false; }
	public int getPlayerType() { return (int)type; }

    // ステータス情報を渡す
    public float getMaxSkillPoint() { return MAX_SKILLPOINT; }
    public float getSkillPoint()    { return skillPoint; }

	// 評価用変数(特殊評価で必要になる ゴリ押し)
	static public bool	isStartAttack { get; private set; }			// 先手を取れたか
	static public bool	isNoDamage { get; private set; }			// ダメージを受けてないか
	public int getTotalDamage() { return MAX_HITPOINT - hitPoint; }	// ダメージの総数

	// 先手が取れたかどうかの確認
	static public void calcStartAttack()
	{
		// もう先手を取ったか、ダメージを既に受けていたら抜ける
		if (isStartAttack == true || isNoDamage == false) return;
		isStartAttack = true;
	}

    // スキル発動時の関数
    public bool invocationSkill(float useSkillPoint)
    {
        if (skillPoint - useSkillPoint < 0.0f) { return false; }
        skillPoint -= useSkillPoint;
        return true;
    }

	// 双狗乱舞用(無敵の開始、終了)
	public void startInvincible() 
	{ 
		nowState = State.Invincible;
		GetComponent<BoxCollider>().isTrigger = true;
		matObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
		// 剣の透過
		if (type == PlayerType.SHIRO) shiroBlade.SetActive(false);
		else kuroBlade.SetActive(false);
	}
	public void endInvincible() 
	{ 
		nowState = State.Alive;
		GetComponent<BoxCollider>().isTrigger = false;
		matObject.GetComponent<SkinnedMeshRenderer>().enabled = true;
		// 剣の透過
		if (type == PlayerType.SHIRO) shiroBlade.SetActive(true);
		else kuroBlade.SetActive(true);
	}

    protected override void Awake()
    {
        //base.Awake();

		animator = GetComponent<Animator>();
		animator.SetInteger("playerID", (int)type);

		oldAnimatorState = animator.GetCurrentAnimatorStateInfo(0);

		changeRing = (GameObject)Resources.Load("Prefab/ChangeRing");

		matObject = transform.FindChild("shiro").gameObject;

		shiroMat = (Material)Resources.Load("Model/shiro/Materials/shiro_tecture_01");
		kuroMat = (Material)Resources.Load("Model/shiro/Materials/kuro_tecture_01");
		matObject.renderer.material = type == PlayerType.SHIRO ? shiroMat : kuroMat;
		playerSkill = GetComponent<PlayerSkill>();

		// セーブデータから勾玉の個数を取得して、ステータスに加算
		SaveData data = GameObject.Find("SaveData").GetComponent<SaveData>();
		attackPoint += data.getAddAttack();
		MAX_HITPOINT += data.getAddHitpoint();
		if (hitPoint == 0) hitPoint = MAX_HITPOINT;

		// 評価用変数初期化
		isStartAttack = false;
		isNoDamage = true;
    }

	// Use this for initialization
	void Start () {
		damageBlow = (AudioClip)Resources.Load("se/damageBlow2");
		// 初期はシロなのでクロの剣を隠しておく
		kuroBlade.SetActive(false);
		
		// スキルアイコン設定
		skillIcon1.texture = playerSkill.getSkillIcon(getPlayerType(), 0);
		skillIcon2.texture = playerSkill.getSkillIcon(getPlayerType(), 1);
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.Instance.isClear() == true) return;

		// クリアしていたらとめる
		if (GameManager.Instance.isPreClear() == true && GetComponent<PlayerMove>().isGrounded() == true)
		{
			if (animatorState.IsName("swordoff") == false && animatorState.IsName("exitSit") == false)
			{
				print("けんしまい");
				clearFlags();
				GetComponent<PlayerNormalAttack>().exitAttack();
				animator.Play("swordoff");
				GameManager.Instance.gameClear();
			}
			return;
		}

		// 死んでいたら死亡処理に投げる
		if (isAlive() == false) { calcDead(); return; }

		// スキル使用中はキャラチェンジや回復はオフに
		if (SkillBase.isSkillPlayingNow() == true) return;

		// スキルポイント計算
        if (skillPoint < MAX_SKILLPOINT)
        {
            // 最大値になるまで加算
            skillPoint = skillPoint + recoverySkillPoint >= MAX_SKILLPOINT ? MAX_SKILLPOINT : skillPoint + recoverySkillPoint;
        }

		// キャラチェンジ
		calcChangeCharacter();
	}

	void LateUpdate()
	{
		oldAnimatorState = animatorState;
		animatorState = animator.GetCurrentAnimatorStateInfo(0);

		// ダメージ受けてる時の後処理はこっちで
		if (oldAnimatorState.IsTag("damage") == true && animatorState.IsTag("damage") == false)
		{
			animator.SetBool("damageDown", false);
			animator.SetBool("damageShrink", false);
			nowState = State.Alive;
		}
	}

	// キャラクターチェンジ
	void calcChangeCharacter()
	{
		if (Input.GetButtonDown("Change") == true)
		{
			// タイプ変更
			type = type == PlayerType.SHIRO ? PlayerType.KURO : PlayerType.SHIRO;
			matObject.renderer.material = type == PlayerType.SHIRO ? shiroMat : kuroMat;
			animator.SetInteger("playerID", (int)type);

			// スキルアイコン変更
			skillIcon1.texture = playerSkill.getSkillIcon(getPlayerType(), 0);
			skillIcon2.texture = playerSkill.getSkillIcon(getPlayerType(), 1);

			// エフェクト生成
			Instantiate(changeRing, transform.position, Quaternion.identity);

			// 剣の変更
			if (type == PlayerType.SHIRO)
			{
				shiroBlade.SetActive(true);
				kuroBlade.SetActive(false);
			}
			else
			{
				shiroBlade.SetActive(false);
				kuroBlade.SetActive(true);
			}
		}
	}

	// animatorのフラグを全部折る関数
	void clearFlags()
	{
		animator.SetFloat("speed", 0.0f);
		animator.SetInteger("waitFrame", 0);
		animator.SetInteger("skillID", 0);
		animator.SetBool("skill", false);
		animator.SetBool("skillExit", false);
		animator.SetBool("jumping", false);
	}

	// ダメージ計算
	public override void calcDamage(int damage)
	{
		// 死亡かダメージ中、無敵以外なら処理
		if (isAlive() == false || nowState == State.Damage || nowState == State.Invincible) { return; }
		
		hitPoint -= damage;
		if (hitPoint <= 0)
		{
			hitPoint = 0;
			nowState = State.Dead;
		}

		isNoDamage = false;
	}

	// ダメージモーション(ひるみ)
	public void calcShrink()
	{
		if (nowState == State.Damage || nowState == State.Dead || nowState == State.Invincible) return;

		clearFlags();
		GetComponent<PlayerNormalAttack>().exitAttack();
		nowState = State.Damage;
		if (animator.GetBool("damageDown") == false)
		{
			animator.SetBool("damageShrink", true);
		}
		audio.PlayOneShot(damageBlow);
	}

	// ふっとばされる処理(リジットボディ必須、AddForceではなくMoveVecで飛ばす)
	public void calcKnocked(Vector3 otherPos, float addForce)
	{
		if (nowState == State.Damage || nowState == State.Dead || nowState == State.Invincible) return;
		
		clearFlags();
		GetComponent<PlayerNormalAttack>().exitAttack();
		nowState = State.Damage;
		
		// 吹っ飛ばす方向をPlayerMoveに渡す
		Vector3 directVec = (gameObject.transform.position - otherPos).normalized;
		directVec.y = 0.0f;	// yはなし(重力に任せる)
		GetComponent<PlayerMove>().setKnockDownStatus(directVec, addForce);

		animator.SetBool("damageShrink", false);
		animator.SetBool("damageDown", true);

		audio.PlayOneShot(damageBlow);
	}

	protected override void calcDead()
	{
		base.calcDead();
		
		if (GameManager.Instance.isGameOver() == true) { return; }
		// モーションが終わったらゲームオーバーにする
		GameManager.Instance.gameOver();	// ゲームオーバー
		animator.Play("dead");
		print("ｵｳﾌ(死亡)");
	}


    // デバッグ用(ステータスとUIの関連チェック)
    // 常に減少
    void debugChangeStatus()
    {
        if (--hitPoint <= 0) { hitPoint = MAX_HITPOINT; }
        if (--skillPoint <= 0) { skillPoint = MAX_SKILLPOINT; }
    }

	public void swordOff()
	{
		shiroBlade.SetActive(false);
		kuroBlade.SetActive(false);
	}

}
