﻿using UnityEngine;
using System.Collections;

// 全敵AI共通のもの
public class AIBase : MonoBehaviour {

	protected delegate void AI();
	protected AI aiUpdate;

	protected bool exitFlag = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// 毎フレーム更新
	public bool update()
	{
		aiUpdate();
		return isExit();
	}

	// 派生クラスごとの初期化処理
	public virtual void setInit()
	{

	}

	// 派生クラスごとに終了処理を変えておく
	public virtual bool isExit()
	{
		// もし終わっていたらここでフラグオフにしてから派生クラスの終了処理へ移行させる
		if (exitFlag == false) { return false; }
		else
		{
			exitFlag = false;
			return true;
		}
	}

}
