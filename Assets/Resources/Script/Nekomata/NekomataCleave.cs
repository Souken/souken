﻿using UnityEngine;
using System.Collections;

// 切り裂くAI
public class NekomataCleave : MonoBehaviour {

	// デリゲートで関数オブジェクトに関数を入れ替える感じでクラス内遷移をさせてみる
	private delegate void MethodObj();
	MethodObj methodObj;

	[SerializeField] private Vector3	warpPos			= Vector3.zero;
	[SerializeField] private float		chargeSeconds	= 3.0f;
	[SerializeField] private float		jumpHeight		= 5.0f;
	[SerializeField] private float		jumpSpeed		= 0.1f;
	[SerializeField] private float		tackleSpeed		= 1.0f;
	[SerializeField] private float		addSizeNum		= 1.002f;
	[SerializeField] private float		waitSeconds		= 2.0f;		// 攻撃後の待機時間

	[SerializeField] private EffectSwitch effectSwitch;

	private float	nowSeconds	= 0.0f;
	private bool	exitFlag	= false;
	private Vector3 movePos		= Vector3.zero;	// 移動する位置
	private Vector3 moveStartPos= Vector3.zero;	// 移動する前の位置

	private Animator	animator;

	[SerializeField] private GameObject catHand;
	Vector3 defaultSize;

	// Y軸の回転はせず、X, Z軸回転を使って指定のオブジェクトの方向を向かせる関数
	void lookAtOtherObject(Vector3 otherObjectPos)
	{	
		otherObjectPos.y = 0.0f;
		gameObject.transform.LookAt(otherObjectPos.normalized);
	}

	// Use this for initialization
	void Start () {
		animator	= gameObject.GetComponent<Animator>();
		
		methodObj	= calcWarp;
		defaultSize = catHand.GetComponent<BoxCollider>().size;

		// 裏モードの場合高速チャージ
		if (SaveData.Instance.isBackMode() == true || GetComponent<Nekomata>().isEnemyBackMode() == true) chargeSeconds = 1.0f;

	}
	
	public bool updateAI()
	{
		methodObj();

		if (exitFlag == true)
		{
			exitFlag = false;
			return true;
		}
		return false;
	}

	// ワープ
	void calcWarp()
	{
		// 効果音をここで発生させる
		audio.PlayOneShot(GetComponent<Nekomata>().tackleSE);

		gameObject.transform.position = warpPos;
		lookAtOtherObject(GameObject.FindGameObjectWithTag("Player").transform.position);
		animator.SetBool("threatFlag", true);
		nowSeconds = chargeSeconds;
		methodObj = calcCharge;

		effectSwitch.Play();
		// 効果音をここで発生させる
		audio.PlayOneShot(GetComponent<Nekomata>().threatSE);
	}

	// チャージ
	void calcCharge()
	{
		// 試し
		// 手を徐々に大きくしてみる(当たり判定は設定よりすこし大きくする)
		catHand.transform.localScale *= addSizeNum;
		catHand.GetComponent<BoxCollider>().size *= (addSizeNum + 0.003f);

		nowSeconds -= Time.deltaTime;
		// チャージ完了したら移動関数へ遷移
		if (nowSeconds <= 0.0)
		{
			animator.SetBool("threatFlag", false);
			nowSeconds	= waitSeconds;			// ここで攻撃後の待機時間を入れておく
			methodObj	= calcJump;

			effectSwitch.Stop();

			// ジャンプ到着位置の設定
			moveStartPos = gameObject.transform.position;
			movePos = moveStartPos;
			movePos.y += jumpHeight;
		}
	}

	// ジャンプ(一定位置まで飛ぶ)
	void calcJump()
	{
		// 移動
		gameObject.transform.Translate(0.0f, jumpSpeed, 0.0f);
		// 地点を通り過ぎてたりしたら補正移動して攻撃に移る
		float baseDistance = Vector3.Distance(movePos, moveStartPos);
		float nowDistance = Vector3.Distance(gameObject.transform.position, moveStartPos);
		if(nowDistance >= baseDistance)
		{
			gameObject.transform.position = movePos;
			moveStartPos	= gameObject.transform.position;
			movePos			= GameObject.Find("Player").transform.position;
			gameObject.transform.LookAt(movePos);

			// 効果音をここで発生させる
			audio.PlayOneShot(GetComponent<Nekomata>().tackleSE);

			// モーションを攻撃に変えて、手の判定をONにする
			animator.SetTrigger("attackTrigger");
			catHand.GetComponent<CatHand>().attackFlag = true;
			catHand.GetComponent<CatHand>().setAttack(GetComponent<Nekomata>().getAttackPoint());
			methodObj		= calcAttack;
		}
	}

	// 攻撃
	void calcAttack()
	{
		// 移動
		gameObject.transform.Translate(0.0f, 0.0f, tackleSpeed);
		// 地点を通り過ぎてたりしたら補正移動して攻撃に移る
		float baseDistance = Vector3.Distance(movePos, moveStartPos);
		float nowDistance = Vector3.Distance(gameObject.transform.position, moveStartPos);
		if (nowDistance >= baseDistance)
		{
			// 位置の微調整(地面に埋まらないように若干高くする)
			Vector3 tmp = movePos;
			movePos.y = 0.1f;
			gameObject.transform.position = tmp;

			// 向き調整
			lookAtOtherObject(GameObject.FindGameObjectWithTag("Player").transform.position);
			methodObj = calcWaitAttacked;
		}
	}

	// 攻撃後の待機
	void calcWaitAttacked()
	{
		nowSeconds -= Time.deltaTime;

		// 指定秒数の1/4が過ぎたら攻撃判定をオフにする
		if (catHand.GetComponent<CatHand>().attackFlag == true && nowSeconds <= (waitSeconds / 4) * 3)
		{
			// 試し(手を元の大きさに)
			catHand.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
			catHand.GetComponent<BoxCollider>().size = defaultSize;
			catHand.GetComponent<CatHand>().attackFlag = false;
		}

		// チャージ完了したら移動関数へ遷移
		if (nowSeconds <= 0.0)
		{
			nowSeconds	= chargeSeconds;
			methodObj	= calcWarp;
			exitFlag	= true;
		}
	}


}
