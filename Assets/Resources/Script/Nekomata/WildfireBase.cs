﻿using UnityEngine;
using System.Collections;

// 鬼火の制御スクリプト
public class WildfireBase : MonoBehaviour {

    [SerializeField] protected int		DELETE_FRAME = 180; // デフォルトは3秒で消える
    [SerializeField] protected float	speed	= 0.1f;
	[SerializeField] protected float	damagePercent = 1.0f;	// ダメージの割合
	[SerializeField] protected int		damage	= 0;
	private int							nowFrame = 0;

    // コンストラクタ代わり
    public static WildfireBase Instantiate(WildfireBase prefab, Vector3 forwardVec)
    {
        WildfireBase obj = Instantiate(prefab) as WildfireBase;
        obj.gameObject.transform.forward = forwardVec;
        return obj;
    }

	public void setDamage(int attackPoint)
	{
		damage = (int)((float)attackPoint * damagePercent);
	}

    public void setForward(Vector3 forwardVec)
    {
        gameObject.transform.forward = forwardVec;
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// 秒数計算は基底クラスの方で行う
	protected virtual void Update()
	{
		if (++nowFrame >= DELETE_FRAME) { Destroy(gameObject); }
	}

    // 当たり判定(取り敢えず何かに当たったら消す、プレイヤーの場合ダメージを)
    void OnTriggerEnter(Collider collider)
    {
		if (collider.gameObject.tag == "Player")
		{
			PlayerStatus player = collider.gameObject.GetComponent<PlayerStatus>();
			player.calcDamage(damage);
			player.calcShrink();
			Instantiate((GameObject)Resources.Load("Prefab/fire"), collider.gameObject.transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
    }
}
