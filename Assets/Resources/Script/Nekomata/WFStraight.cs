﻿using UnityEngine;

// 直線
public class WFStraight : WildfireBase {

	void Awake()
	{
		damagePercent = 1.2f;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();
		gameObject.transform.Translate(new Vector3(0.0f, 0.0f, speed));
	}
	
}
