﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;   //List用

public class Nekomata : EnemyBase {
    private Animator animator;
    private AnimatorStateInfo state, oldState;

	// AI(どうにかジェネリックとデリゲートですっきりさせたい)
	// 取り敢えずデリゲートにAIのupdate関数を突っ込む方針
	private delegate bool AI();
	AI aiUpdate;

	private NekomataCleave		aiCleave;
	private NekomataWildfire	aiWildfire;

	public AudioClip threatSE;
	public AudioClip tackleSE;

	[SerializeField]
	private ParticleSystem aura;

	private AudioClip[] damageVoices = new AudioClip[5];

	// 超ごり押し(デバッグ目的)
	public string aiName = "Wildfire";

	public int nowHitPoint_D = 0;

    protected override void Awake()
    {
        base.Awake();
		animator = gameObject.GetComponent<Animator>();
		oldState = animator.GetCurrentAnimatorStateInfo(0);

		aiWildfire = GetComponent<NekomataWildfire>();
		aiCleave = GetComponent<NekomataCleave>();

		// タックル音
		tackleSE = (AudioClip)Resources.Load("SE/tackle");
		// 威嚇効果音
		threatSE = (AudioClip)Resources.Load("se/threat_5");
		// ダメージボイス取得
		for (int i = 0; i < 5; ++i) { damageVoices[i] = (AudioClip)Resources.Load("se/nyaa" + (i + 1).ToString()); }

		// 最初のAIを設定
		aiUpdate = aiWildfire.updateAI;

    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// フェードインしきっていないなら動かない
		if (FadeManager.Instance.isMaxFadeIn() == false) { return; }

        // 現在の状態を持ってくる
        oldState    = state;
        state       = animator.GetCurrentAnimatorStateInfo(0);

		// 死んでいたら死亡処理に投げる
		if (isAlive() == false) 
		{
			if (GameManager.Instance.isPreClear() == false && GameManager.Instance.isClear() == false && 
				state.IsName("dead") == true && state.normalizedTime >= 1.0f)
			{
				GameManager.Instance.gamePreClear();	// ゲームクリア
			}
			else if (state.IsName("dead") == false)
			{
				calcDead();
			} 
			return; 
		}

		// AI更新(trueなら変更)
		if(aiUpdate() == true)
		{
			print("jo");
			if (aiName == "Wildfire")
			{
				aiName = "Cleave";
				aiUpdate = aiCleave.updateAI;
			}
			else
			{
				aiName = "Wildfire";
				aiUpdate = aiWildfire.updateAI;
			}
		}
		// デバッグ(現在のHP公開)
		nowHitPoint_D = hitPoint;
	}

	public override void calcDamage(int damage)
	{
		base.calcDamage(damage);
		PlayerStatus.calcStartAttack();	// 先手の確認
		// ランダムでボイス再生
		audio.PlayOneShot(damageVoices[Random.Range(0, 5)]);
	}

	// 死亡処理
	protected override void calcDead()
	{
		base.calcDead();
		if (GameManager.Instance.isClear() == true) { return; }
		aura.Stop();
		animator.Play("dead");
		print("ｵｳﾌ(死亡)");
	}

}
