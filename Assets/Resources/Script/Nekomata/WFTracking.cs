﻿using UnityEngine;

// 指定フレーム間は追尾する鬼火
public class WFTracking : WildfireBase {

	[SerializeField, Tooltip("追尾するフレーム数")] 
	private int	stopTrackingFrame = 60;
	[Tooltip("プレイヤーを追尾する時の回転力、高いほど追尾性能が上がる")]
	[SerializeField, Range(0.0f, 0.5f)]
	private float	ROTATE_POWER = 0.05f;

	private GameObject target;
	private int		nowMyFrame	= 0;
	[SerializeField]
	private float	mySpeed		= 0.2f;

	void Awake()
	{
		damagePercent = 0.8f;
	}

	// Use this for initialization
	void Start () {
		Color[] colors = new Color[5];
		for (int i = 4; i >= 0; --i)
		{
			float num = i;
			num = num / 10.0f + 0.1f;
			colors[i] = new Color(1.0f, num, 0.0f, 0.5f - (num - 0.1f));
		}

		ParticleAnimator particleAnimator = GetComponent<ParticleAnimator>();
		Color[] modifiedColors = particleAnimator.colorAnimation;
		particleAnimator.colorAnimation = colors;

		target = GameObject.Find("Player");
		transform.LookAt(target.transform);
		DELETE_FRAME = 120;
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();

		// プレイヤーの座標と自分の座標からプレイヤーへの方向ベクトル算出
		Vector3 myPos = transform.position;
		Vector3 tarPos = target.transform.position;
		Vector3 targetDirect = (tarPos - myPos).normalized;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), ROTATE_POWER);

		// 指定フレームまで追尾
		if(nowMyFrame < stopTrackingFrame)
		{
			++nowMyFrame;
		}
		gameObject.transform.Translate(new Vector3(0.0f, 0.0f, mySpeed));
	}

}
