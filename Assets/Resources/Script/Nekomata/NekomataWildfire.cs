﻿using UnityEngine;
using System.Collections;

// 鬼火使用時のAI
public class NekomataWildfire : MonoBehaviour {

    // デリゲートで関数オブジェクトに関数を入れ替える感じでクラス内遷移をさせてみる
    private delegate void MethodObj();
    MethodObj methodObj;

	// 乱数使うのでRandom用意
	System.Random rand = new System.Random();

    [SerializeField] private int	shotNum				= 3;		// 撃つ回数
    [SerializeField] private int	shotBulletNum		= 6;		// 一回の間隔ごとに撃つ弾数
    [SerializeField] private float	shotIntervalSeconds = 2.0f;		// 間隔(秒)
	[SerializeField] private float	moveSpeed			= 0.05f;

	private int		nowShotNum	= 0;	// 現在撃った回数

    private float	nowSeconds	= 0.0f;
    private float	bulletInterval;
    private float	addAngle	= Mathf.PI / 36.0f;			// 間隔を重ねるごとに角度を変化

	private Vector3[]	basePos			= new Vector3[4];	// 移動基準地点(シーンに置いておいたからStart内で設定)
	private int			nextPosNum;							// ランダムで設定
	private Vector3		moveStartPos	= Vector3.zero;		// 移動開始地点(移動補正に使用)

	private bool exitFlag = false;

	private int nekomataAtatckPoint = 0;

	private Animator		animator;
	private AudioClip		wildfireSE;
    private GameObject		wildfire;

	// Use this for initialization
	void Start () {
        nowSeconds = shotIntervalSeconds;
        bulletInterval = (2.0f * Mathf.PI) / shotBulletNum; // 間隔を算出

		nekomataAtatckPoint = GetComponent<Nekomata>().getAttackPoint();

		if(GetComponent<Nekomata>().isEnemyBackMode() || SaveData.Instance.isBackMode())
		{
			shotBulletNum = 12;
		}

		// 基準点を格納
		GameObject[] tmp = GameObject.FindGameObjectsWithTag("AI");
		for (int i = 0; i < 4; ++i)
		{
			basePos[i] = tmp[i].transform.position;
			print(basePos[i]);
		}
		// 最初に向かう基準点を算出
		nextPosNum = rand.Next(0, 4);

        // 鬼火取得
        wildfire = (GameObject)Resources.Load("Prefab/WildFire");
		// 鬼火音
		wildfireSE = (AudioClip)Resources.Load("se/wildFire_1");

		animator = gameObject.GetComponent<Animator>();

        // 初回の更新処理の関数を格納
        methodObj = calcMove;
	}

	// 全体の更新処理
	public bool updateAI()
	{
        // ここの更新をどうにか上手く切り替えたい
        methodObj();
		if (exitFlag == true)
		{
			exitFlag = false;
			return true;
		}
		return false;
	}

    // 鬼火の更新処理
    void calcWildfires()
    {
		// プレイヤーの方向を常に向かせる(タグ検索で見つける)
		gameObject.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);

        nowSeconds -= Time.deltaTime;
        // 一定のタイミングで鬼火発射
        if (nowSeconds <= 0.0)
        {
            audio.PlayOneShot(wildfireSE);
            createWildfires();
			// 一定間隔置く
            bulletInterval	+= addAngle;
			nowSeconds		= shotIntervalSeconds;
			++nowShotNum;

			// 撃ったらしばらく待機
			methodObj = calcWait;
        }
    }

    // 鬼火生成
    void createWildfires()
    {
        Vector3 initPos = gameObject.transform.position;
        initPos.y += 0.2f;

		int trackingNum = GetComponent<Nekomata>().isEnemyBackMode() || SaveData.Instance.isBackMode() ? 3 : 1;
		if (trackingNum == 3 && GetComponent<Nekomata>().getHitPoint() < 200) trackingNum = 5;

        for (int i = 0; i < shotBulletNum; ++i)
        {
            Vector3 vec = new Vector3(Mathf.Sin((float)i * bulletInterval), 0.0f, Mathf.Cos((float)i * bulletInterval));
            GameObject tmp = Instantiate(wildfire, initPos, Quaternion.identity) as GameObject;

            //自分の親オブジェクトにクローンを作って
            //親がクローンのコンポネントを管理しやすくする            
            tmp.transform.parent = this.transform.parent.transform;
	
			// ここで状況に応じて追加クラスを変える
			// 今の所一個だけ追尾弾
			if (i >= shotBulletNum - trackingNum)
			{
				tmp.AddComponent<WFTracking>().setForward(vec);

			}
			else
			{
				tmp.AddComponent<WFStraight>().setForward(vec);
			}
			tmp.GetComponent<WildfireBase>().setDamage(nekomataAtatckPoint);
		}
    }

    // 待機処理(関数テスト)
    void calcWait()
    {
		nowSeconds -= Time.deltaTime;
		// 待機完了したら
		if (nowSeconds <= 0.0)
		{
			// 指定数撃っていなかったら鬼火処理へ
			if (nowShotNum != shotNum) { methodObj = calcWildfires; return; }

			// 指定数撃ってたら設定の初期化を行った後に移動関数へ
			nowShotNum = 0;
			nowSeconds = shotIntervalSeconds;
			// 移動先を決め、指定方向に向かせておく(同じ値ならもう一度やり直し)
			// 簡易再帰関数ですね…
			while (true)
			{
				int tmp = rand.Next(0, 4);
				if (nextPosNum != tmp) { nextPosNum = tmp; break; }
			}
			gameObject.transform.LookAt(basePos[nextPosNum]);
			moveStartPos	= gameObject.transform.position;
			methodObj		= calcMove;

			exitFlag = true;
		}
    }

	// 指定地点まで移動
	void calcMove()
	{
		// アニメーション設定
		if (animator.GetBool("moveFlag") == false) { animator.SetBool("moveFlag", true); }
		// 指定方向を向いていなかったら向かせる
		if (gameObject.transform.forward != basePos[nextPosNum]) { gameObject.transform.LookAt(basePos[nextPosNum]); }

		// 取り敢えず即移動
		gameObject.transform.Translate(0.0f, 0.0f, moveSpeed);
		// 地点を通り過ぎてたりしたら補正移動して攻撃に移る
		float baseDistance	= Vector3.Distance(basePos[nextPosNum], moveStartPos);
		float nowDistance	= Vector3.Distance(gameObject.transform.position, moveStartPos);
		if (nowDistance >= baseDistance)
		{
			gameObject.transform.position = basePos[nextPosNum];
			animator.SetBool("moveFlag", false);
			methodObj = calcWildfires;

			// 移動が終わったら効果音鳴らす
			audio.PlayOneShot(GetComponent<Nekomata>().threatSE);
		}
	}


}
