﻿using UnityEngine;
using System.Collections;

public class AutoDestroyEffect : MonoBehaviour
{

    private ParticleSystem effect;
    private float lifeTime; // 描画時間
    private float drawTime; // 現在の描画経過時間

    // Use this for initialization
    void Start()
    {
        effect = gameObject.GetComponent<ParticleSystem>();
        lifeTime = effect.startLifetime;
        drawTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (drawTime > lifeTime)
        {
            // 描画時間が過ぎていたら消す
            Destroy(this.gameObject);
        }
        else
        {
            // 描画中
            drawTime += Time.deltaTime;
        }
    }
}
