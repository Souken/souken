﻿using UnityEngine;
using System.Collections;

// ステージのクリア、ゲームオーバーなどを管理するクラス
public class GameManager : SingletonMonoBehaviour<GameManager> {

	// ゲームの状態(フェードイン, 進行中, ポーズ, クリア, ゲームオーバー)
	enum GameState
	{
		FadeIn, InProgress, Pause, PreClear, Clear, GameOver
	};
	private GameState	nowState		= GameState.FadeIn;

	[SerializeField]
	private float addFadeNum = 0.05f;
	[SerializeField]
	private bool changeSceneFlag = false;

	public void gamePreClear() { nowState = GameState.PreClear; }
	public void gameClear() { nowState = GameState.Clear; }
	public void gameOver()	{ nowState = GameState.GameOver; }
	public bool isGameEnd() { return nowState == GameState.Clear || nowState == GameState.GameOver ? true : false; }	// ゲームが終わってるかどうか
	public bool isPausing() { return nowState == GameState.Pause ? true : false; }
	public bool isPreClear() { return nowState == GameState.PreClear ? true : false; }
	public bool isClear()	{ return nowState == GameState.Clear ? true : false; }
	public bool isGameOver() { return nowState == GameState.GameOver ? true : false; }

	// 毎回初めは進行中にしておく
	public void Awake()
	{
		nowState = GameState.InProgress;
		FadeManager.Instance.startFadeIn(0.05f);
	}

	void Update()
	{
		print(nowState);

		// フェードインしきるまで待機
		if (nowState == GameState.FadeIn && FadeManager.Instance.isMaxFadeIn() == true) { nowState = GameState.InProgress; }

		// ゲームクリア、ゲームオーバーしていたらフェードアウト
		if (isGameEnd() == false) return;

		if (changeSceneFlag == false && isClear() == true && BattleEvaluation.Instance.isGameEnd == true)
		{
			if (Input.GetButtonDown("Attack") == true)
			{
				if (Application.loadedLevelName == "Stage3")
				{
					FadeManager.Instance.loadLevel("EndRoll", 0.005f);
				}
				else
				{
					FadeManager.Instance.loadLevel("StageSelect", 0.005f);
				}
				changeSceneFlag = true;
			}
		}
		else if (changeSceneFlag == false && isGameOver() == true)
		{
			FadeManager.Instance.loadLevel("StageSelect", 0.005f);
			changeSceneFlag = true;
		}
	}

}
