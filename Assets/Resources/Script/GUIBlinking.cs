﻿using UnityEngine;
using System.Collections;

public class GUIBlinking : MonoBehaviour {

    [SerializeField] int interval = 15;
    [SerializeField] int visibleTime = 15;

    bool isVisible;
    int count;

	// Use this for initialization
	void Start () 
    {	

	}
	
	// Update is called once per frame
	void Update () {

        UpdateState();

        if(guiTexture != null)
            guiTexture.enabled = isVisible;

	}

    void UpdateState()
    {
        count++;

        if (count < interval)
            return;

        if (interval + visibleTime < count)
        {
            count = 0;
            isVisible = false;
            return;
        }

        if (interval < count)
        {
            isVisible = true;
            return;
        }
    }
}
